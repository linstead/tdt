/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * Declares the primitive data transfer operations.
 * Originated by Cezar.
 */
#ifndef __PRIMOPS_H__
#define __PRIMOPS_H__

/* 
 * Primitive read and write functions.
 * arguments:
 *      void *val: pointer to a primitive value
 *      void *type_val: always NULL, not used, introduced for
 *      compatibility with operations on composite types
 *      void *args: additional parameters. The default implementation
 *      needs args that cast to a TDTAddress
 *      void *fctns: an array of operations. Not used. Introduced for
 *      compatibility with operations on composite types.
 * error conditions:
 *  TODO:
 */
void
read_char (void *val, void *type_val, void *args, void *fctns);
void
read_int (void *val, void *type_val, void *args, void *fctns);
void
read_float (void *val, void *type_val, void *args, void *fctns);
void
read_double (void *val, void *type_val, void *args, void *fctns);
void
read_notype (void *val, void *type_val, void *args, void *fctns);
void
read_block (void *val, int size, void *args);


void
write_char (void *val, void *type_val, void *args, void *fctns);
void
write_int (void *val, void *type_val, void *args, void *fctns);
void
write_float (void *val, void *type_val, void *args, void *fctns);
void
write_double (void *val, void *type_val, void *args, void *fctns);
void
write_notype (void *val, void *type_val, void *args, void *fctns);
void
write_block (void *val, int size, void *args);

#ifdef _MPI

void
read_char_mpi (void *val, void *type_val, void *args, void *fctns);
void
read_int_mpi (void *val, void *type_val, void *args, void *fctns);
void
read_float_mpi (void *val, void *type_val, void *args, void *fctns);
void
read_double_mpi (void *val, void *type_val, void *args, void *fctns);
void
read_block_mpi (void *val, int size, void *args);
void
write_char_mpi (void *val, void *type_val, void *args, void *fctns);
void
write_int_mpi (void *val, void *type_val, void *args, void *fctns);
void
write_float_mpi (void *val, void *type_val, void *args, void *fctns);
void
write_double_mpi (void *val, void *type_val, void *args, void *fctns);
void
write_block_mpi (void *val, int size, void *args);


#endif
#endif
