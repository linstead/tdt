/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Windows changes 29-09-2002, Dan
 * Only added TDT_ in front of the defined types
 */

#include <stdio.h>
#include <stdlib.h>
#include "typeops.h"
#include "tdt_map.h"

void
tdt_map (void *val, TypeDesc td, void *args, void *functions) {

	TypeName tn;
	TypeVal  tv;

	void     (**funarray) (void *, void *, void *, void *);

    if (td == NULL) {
        tdt_error ("td passed to tdt_map is NULL");
        exit (1);
    }

    if (type_name (td) == TDT_NOTYPE) {
        tdt_error ("type_name(td) in tdt_map is NOTYPE");
        exit (1);
    }

    tn = type_name (td);
    tv = type_value (td);

    if (functions == NULL) {
        tdt_error ("functions in tdt_map is NULL");
        exit (1);
    }
    
	funarray = (void (**)(void *, void *, void *, void *)) functions;

/*    if (args == NULL) {
        tdt_error ("args parameter is NULL in tdt_map");
        exit (1);
    }
*/
    if (val == NULL) {
        tdt_error ("val parameter is NULL in tdt_map");
        exit (1);
    }
    
	funarray[tn] (val, tv, args, functions);
}
