#include <string.h>
#include "decl.h"
type1 get_type1(char *name)
{
	if( strcmp(name,"char") == 0)
		return(zchar);
	if( strcmp(name,"int") == 0)
		return(zint);
	if( strcmp(name,"float") == 0)
		return(zfloat);
	if( strcmp(name,"double") == 0)
		return(zdouble);
	if( strcmp(name,"struct") == 0)
		return(zstruct);

	return(notype1);
}
