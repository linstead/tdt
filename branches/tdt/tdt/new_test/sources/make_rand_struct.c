#include "make_rand_struct.h"
#include "decl.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

int myrand(int r)
{
	int p;
	if(r<=0)
		return(-1);
	p=rand();
	p=(p-r*(p/r));
	return(p);
}

mydecl* add_rand_decl(char* name, int level)
{
	int size_addr, size_array, n, i;
	char *name1;
	mydecl *md;
	mytype2 *t;
    addr_desc *ad_d;
	array_desc *ar_d;
	struct_desc *sd;
	
	md = (mydecl*) calloc(1,sizeof(mydecl));
	md->t2=(mytype2*) calloc(1,sizeof(type2));
	md->t1=(mytype1*) calloc(1,sizeof(type1));
	
	/*determine the type of the right side of the declaration*/
	n=myrand(100);
	size_addr=size_array=0;
	if(n<opt.array_percentage) 
		size_array=myrand(opt.max_array_sizes)+1;
	else if(n<opt.array_percentage+opt.addr_percentage)
		size_addr=myrand(opt.max_array_sizes)+1;
	else if(n<opt.array_percentage+opt.addr_percentage+opt.addr_array_percentage) {
		size_array=myrand(opt.max_array_sizes)+1;
		size_addr=myrand(opt.max_array_sizes)+1;
	}
	
	md->t2->name=strdup(name);
	if(size_addr+size_array==0)
		md->t2->type=simple;
	t=md->t2;
	for(i=0;i<size_array;i++) {
		ar_d= (array_desc*) calloc(1,sizeof(array_desc));
		t->type_desc=ar_d;
		t->type=zarray;
		ar_d->dim=myrand(opt.max_array_dimensions)+1;
		if(i<size_array-1 || size_addr>0) {
			ar_d->t=(mytype2*) calloc(1,sizeof(type2));
			t=ar_d->t;
		}
	}
		
	for(i=0;i<size_addr;i++) {
		ad_d= (addr_desc*) calloc(1,sizeof(addr_desc));
		t->type_desc=ad_d;
		t->type=zaddr;
		ad_d->dim=myrand(opt.max_array_dimensions)+1;
		if(i<size_addr-1) {
			ad_d->t=(mytype2*) calloc(1,sizeof(type2));
			t=ad_d->t;
		}
	}	

	/*determine the left side of the declaration*/
	n=myrand(100);
	if(n>opt.struct_percentage || level==0) { 
		/*simple*/
		n=myrand(4);
		md->t1->type=(type1) n;
	}
	else { 
		/*struct*/
		md->t1->type=zstruct;
		n=myrand(opt.max_elem_struct)+1;
		sd = (struct_desc*) calloc(1,sizeof(struct_desc));
		md->t1->type_desc=sd;
		sd->num_decls=n;
		sd->decls=(mydecl**) calloc(1,n*sizeof(mydecl*));
		name1=(char*)calloc(1,strlen(name)+10);
		for(i=0;i<n;i++){
			sprintf(name1,"%s_%d",name,i);
//			printf("%s\n",name1);
			sd->decls[i]=add_rand_decl(name1,level-1);
		}
		free(name1);
	}
	return(md);
}

	
int read_opt(char *s,int *d)
{
	char *ch;
	int r;
	ch=strchr(s,'=');
	if(ch==NULL)
		return(0);
	else
	{
		ch[0]=0;
		ch++;
		r=sscanf(ch,"%d",d);
		if (r==0 || r==EOF)
			return(0);
		else
			return(1);
	}
}


model* add_rand_model(char *file)
{
	FILE *conf;
	char buffer[100];
	char op[100];
	int o;
	int i;
	char name[100];
	model *mod;
	
	srand((int) time(NULL));
	
	conf=fopen(file,"r+t");
	if (conf==NULL) {
		printf("Cannot open file %s\n",file);
		return(NULL);
	}

	/*default parameters*/
	opt.num_declarations=10;
	opt.struct_percentage=30;
	opt.max_elem_struct=10;
	opt.array_percentage=10;
	opt.addr_percentage=10;
	opt.addr_array_percentage=10;
	opt.max_array_dimensions=2;
	opt.max_array_sizes=2;
	opt.max_level=2;

		
	while(NULL!=fgets(op,99,conf))
	{
//		printf("aaa:   %s",buffer);
		if(read_opt(op,&o)) {
			if(strcmp(op,"num_declarations")==0)
				opt.num_declarations=o;
			if(strcmp(op,"struct_percentage")==0)
				opt.struct_percentage=o;
			if(strcmp(op,"max_elem_struct")==0)
				opt.max_elem_struct=o;
			if(strcmp(op,"array_percentage")==0)
				opt.array_percentage=o;
			if(strcmp(op,"addr_percentage")==0)
				opt.addr_percentage=o;
			if(strcmp(op,"addr_array_percentage")==0)
				opt.addr_array_percentage=o;
			if(strcmp(op,"max_array_dimensions")==0)
				opt.max_array_dimensions=o;
			if(strcmp(op,"max_array_sizes")==0)
				opt.max_array_sizes=o;
			if(strcmp(op,"max_level")==0)
				opt.max_level=o;
		}
	}

	mod=(model*) calloc(1,sizeof(model));
	mod->num_decls=opt.num_declarations;
	mod->decls=(mydecl**) calloc(1,sizeof(mydecl*)*opt.num_declarations);
	
	for(i=0;i<opt.num_declarations;i++) {
		sprintf(name,"z_%d",i);
		mod->decls[i]=add_rand_decl(name,opt.max_level);
	}
	return(mod);

}


