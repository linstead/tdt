/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Change history: 06/06/02
	added tdt_size_array 
	changed order of args for tdt_read and tdt_write
*/

/* Windows changes 29-09-2002, Dan
 * Only added TDT_ in front of the defined types
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "typeops.h"
#include "xmlhandlers.h"
#include "addrops.h"
#include "tdterror.h"

#include "tdt_inner.h"
#include "tdterror.h"

#ifdef _MPI
#include "mtdt.h"
#else
#include "tdt.h"
#endif

#ifdef _MPI
#include <mpi.h>
#endif

/* Returns NULL on error */
TDTState
make_empty_ts () {

	TDTState    ts;

	ts = (TDTState) malloc (sizeof (struct tdt_state));

    if (ts == NULL) {
        tdt_error ("Cannot allocate memory for TDTState in make_empty_ts");
        return NULL;
    }

	return ts;
}

/* Exits with status 1 on error */
void
set_datadesc (TDTState ts, DataDesc md) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in set_datadesc");
        exit (1);
    }

    if (md == NULL) {
        tdt_error ("DataDesc is NULL in set_datadesc");
        exit (1);
    }
    
	ts->md = md;
}

/* Exits with status 1 on error */
void
set_tdtaddr (TDTState ts, TDTAddr ta) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in set_tdtaddr");
        exit (1);
    }

    if (ta == NULL) {
        tdt_error ("TDTAddr is NULL in set_tdtaddr");
        exit (1);
    }
    
	ts->ta = ta;
}

/* Returns NULL on error */
DataDesc
get_datadesc (TDTState ts) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in get_datadesc");
        return NULL;
    }
        
	return ts->md;
}

/* Returns NULL on error */
TDTAddr
get_tdtaddr (TDTState ts) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in get_tdtaddr");
        return NULL;
    }
        
	return ts->ta;
}

/* Exits with status 1 on error */
TDTState
tdt_parse_datadesc (String xmlname) {

	TDTState    ts;
	DataDesc   md;

	md = read_datadesc (xmlname);

    if (md == NULL) {
        tdt_error ("read_datadesc returned NULL");
        exit (1);
    }
    
	ts = make_empty_ts ();

    if (ts == NULL) {
        tdt_error ("make_empty_ts returned NULL");
        exit (1);
    }
    
	set_datadesc (ts, md);

	return ts;
}

TDTState
tdt_init (TDTConfig tc, String cname) {

    TDTState ts;
    int i, found;
    String ddname;

    /* Find the datadesc associated with "cname" in "tc"*/

    found = 0;

    for (i=0; i<tc->num_channels; i++) {

         if (strcmp(tc->channels[i]->name, cname) == 0) {
             ddname = tc->channels[i]->datadesc;
             found  = 1;
         }
    }
    if (!found) {
         tdt_error("Cannot find datadesc filename: no such connection");
         return NULL;
    }

    /* Now call the "original" tdt_init(), i.e. parse the datadesc.*/
    ts = tdt_parse_datadesc (ddname);
    return ts;
}

/* Exits with status 1 on error */
TDTState
tdt_open_file (TDTState ts, String fname, Mode mode) {

	TDTAddr     ta;

	ta = mkfileaddr (fname, mode);

    if (ta == NULL) {
        tdt_error ("mkfileaddr returned NULL in tdt_open_file");
        exit (1);
    }
    
	ta = tdt_open_addr (ta);

    if (ta == NULL) {
        tdt_error ("tdt_open returned NULL in tdt_open_file");
        exit (1);
    }

	set_tdtaddr (ts, ta);

	return ts;
}

/* Exits with status 1 on error */
TDTState
tdt_open_socket (TDTState ts, String hostname, int port, Mode mode) {

	TDTAddr     ta;

	ta = mksocketaddr (hostname, port, mode);

    if (ta == NULL) {
        tdt_error ("mksocketaddr returned NULL in tdt_open_socket");
        exit (1);
    }
    
	ta = tdt_open_addr (ta);

    if (ta == NULL) {
        tdt_error ("tdt_open returned NULL in tdt_open_socket");
        exit (1);
    }

	set_tdtaddr (ts, ta);

	return ts;
}

TDTState
tdt_open (TDTConfig tc, String cname) {

    TDTState ts;
    Channel c;
    int i, found;
    Mode mode;

    ts = tdt_init (tc, cname);

    found = 0;

    for (i=0; i<tc->num_channels; i++) {
         if (strcasecmp(tc->channels[i]->name, cname) == 0) {
             c = tc->channels[i];
             found = 1;
             break;
         }
    }

    if (!found) {
        tdt_error ("tdt_open: channel name not found in TDTConfig");
        fprintf (stderr, "%s", cname);
        exit (1);
    }

    if (strcasecmp(c->mode, "in")==0) {
        mode = READ;
    } else {
        mode = WRITE;
    }

    /* open the appropriate channel */

    /* SOCKET */

    if (strcasecmp(c->type, "socket")==0) {

        ts = tdt_open_socket (ts, c->host, c->port, mode);

        if (ts == NULL) {
            tdt_error ("tdt_open_socket returned NULL in tdt_open");
            exit (1);
        }
    }

    /* FILE */

    if (strcasecmp(c->type, "file")==0) {

        ts = tdt_open_file (ts, c->filename, mode);

        if (ts == NULL) {
            tdt_error ("tdt_open_file returned NULL in tdt_open");
            exit (1);
        }
    }

    return ts;
}

/* Exits with status 1 on error */
void
tdt_write (TDTState ts, void *value, String name) {

	TypeDesc    td;
	TDTAddr     ta;
	DataDesc    md;

	md = get_datadesc (ts);

    if (md == NULL) {
        tdt_error ("get_datadesc returned NULL in tdt_write");
        exit (1);
    }
    
	td = find_type_in_datadesc (md, name);

    if (td == NULL) {
        tdt_error ("find_type_in_datadesc returned NULL in tdt_write");
        exit (1);
    }

	ta = get_tdtaddr (ts);

    if (ta == NULL) {
        tdt_error ("get_tdtaddr returned NULL in tdt_write");
        exit (1);
    }

    /*Send arrays by block*/
    ta->transfer_blocks = TRUE;

    if (tdt_send (value, td, ta) != 0 ) {
        tdt_error ("call to tdt_send failed in tdt_write");
        exit (1);
    }
    
}

/* Exits with status 1 on error */
void
tdt_read (TDTState ts, void *value, String name) {

	TypeDesc    td;
	TDTAddr     ta;
	DataDesc    md;

           
	md = get_datadesc (ts);

    if (md == NULL) {
        tdt_error ("get_datadesc returned NULL in tdt_read");
        exit (1);
    }
    
	td = find_type_in_datadesc (md, name);

    if (td == NULL) {
        tdt_error ("find_type_in_datadesc returned NULL in tdt_read");
        exit (1);
    }

	ta = get_tdtaddr (ts);

    if (ta == NULL) {
        tdt_error ("get_tdtaddr returned NULL in tdt_read");
        exit (1);
    }
           
    if (tdt_recv (value, td, ta) != 0) {
        tdt_error ("call to tdt_recv failed in tdt_write");
        exit (1);
    }

}

/* Exits with status 1 on error */
void
tdt_close (TDTState ts) {

	TDTAddr     ta;
	DataDesc   md;

	ta = get_tdtaddr (ts);

    if (ta == NULL) {
        tdt_error ("get_tdtaddr returned NULL in tdt_close");
        exit (1);
    }
    
	tdt_close_addr (ta);

	md = get_datadesc (ts);

    if (md == NULL) {
        tdt_error ("get_datadesc returned NULL in tdt_close");
        exit (1);
    }
    
	free_datadesc (md);

	free (ts);
}

void
tdt_size_array (TDTState ts, String name, int size) {
	
	TypeDesc    td;
	DataDesc   md;
	ArrayDesc	ad;

	md = get_datadesc (ts);

    if (md == NULL) {
        tdt_error ("get_datadesc returned NULL in tdt_size_array");
        exit (1);
    }
    
	td = find_type_in_datadesc (md, name);

    if (td == NULL) {
        tdt_error ("find_type_in_datadesc returned NULL in tdt_read");
        exit (1);
    }

	/* The type should be an array, if not, panic. */
	if (type_name (td) != TDT_ARRAY) {
		tdt_error ("variable not an array in tdt_size_array:");
		tdt_error (name);
		exit (1);
	}

	/* Set the size of the array. */
	ad = (ArrayDesc) type_value (td);
	ad->size = size;
}

/* from tdt_config.c*/

void
print_channel (Channel c) {
    printf ("name is %s\n", c->name);
    printf ("mode is %s\n", c->mode);
    printf ("host is %s\n", c->host);
    printf ("port is %d\n", c->port);
    printf ("type is %s\n", c->type);
    printf ("filename is %s\n", c->filename);
    printf ("datadesc is %s\n", c->datadesc);
}

void
print_config (TDTConfig tc) {

    int i;

    printf ("*************************************\n");

    printf ("tc->progname is %s\n", tc->progname);
    printf ("tc->num_channels is %d\n", tc->num_channels);

    for (i=0; i<tc->num_channels; i++) {
        printf ("\n");
        printf ("Channel %d:\n", i);
        printf ("-------------------------------------\n");
        print_channel (tc->channels[i]);
        printf ("-------------------------------------\n");
    }

    printf ("*************************************\n");

}

TDTConfig
tdt_configure (String config_filename) {

    TDTConfig tc;

    tc = read_config (config_filename);

    /* Uncomment the following of you want to examine the parsed config */
    /*print_config(tc);*/

    return tc;
}

void
tdt_end (TDTConfig tc) {

    int i;

    for (i=0; i<tc->num_channels; i++) {
        free(tc->channels[i]);
    }

    free (tc);
}


#ifdef _MPI
TDTState*
tdt_init_mpi(String xmlname, int* argc, char*** argv) {

	int np, i;
	TDTState *ts;
	TDTAddr ta;
	DataDesc md;

	md = read_datadesc (xmlname);

	if (md == NULL) {
		tdt_error ("read_datadesc returned NULL");
		exit (1);
	}

	i=MPI_Init(argc , argv);
	if (i != MPI_SUCCESS)
	{
		tdt_error ("Could not initialize MPI");
		exit(1);
	}

	MPI_Comm_size(MPI_COMM_WORLD, &np);

	ts = (TDTState*) malloc (np * sizeof (struct tdt_state));
	if (ts == NULL) {
	    tdt_error ("Cannot allocate memory for TDTState* in tdt_init_mpi");
	    return NULL;
	}

	for(i=0;i<np;i++) {

	    ts[i] = make_empty_ts ();
	    set_datadesc (ts[i], md);
	    ta = mkmpiaddr(i);
	    if (ta == NULL) {
	        tdt_error ("mkmpiaddr error in tdt_init_mpi");
	        exit(1);
	    }
	    set_tdtaddr (ts[i], ta);
	}

	return (ts);
}

TDTState*
tdt_init_mpif(String xmlname) {

	int np, i;
	TDTState *ts;
	TDTAddr ta;
	DataDesc md;

	md = read_datadesc (xmlname);

	if (md == NULL) {
		tdt_error ("read_datadesc returned NULL");
		exit (1);
	}

	MPI_Comm_size(MPI_COMM_WORLD, &np);

	ts = (TDTState*) malloc (np * sizeof (struct tdt_state));
	if (ts == NULL) {
	    tdt_error ("Cannot allocate memory for TDTState* in tdt_init_mpi");
	    return NULL;
	}

	for(i=0;i<np;i++) {

	    ts[i] = make_empty_ts ();
	    set_datadesc (ts[i], md);
	    ta = mkmpiaddr(i);
	    if (ta == NULL) {
	        tdt_error ("mkmpiaddr error in tdt_init_mpi");
	        exit(1);
	    }
	    set_tdtaddr (ts[i], ta);
	}

	return (ts);
}

void
tdt_end_mpi(TDTState* ts)
{
	int np, i;
	TDTAddr ta; 
	DataDesc md;

	MPI_Comm_size(MPI_COMM_WORLD, &np);

	for(i=0;i<np;i++) {
		if (i==0) {
			md = get_datadesc (ts[0]);
			if (md == NULL) {
				tdt_error ("get_datadesc returned NULL in tdt_end_mpi");
				exit (1);
			}
			free_datadesc (md);
		}
		ta = get_tdtaddr (ts[i]);
		if (ta == NULL) {
			tdt_error ("get_datadesc returned NULL in tdt_end_mpi");
			exit (1);
		}
		close_mpi (ta);
		free (ts[i]);
	}
	
	free (ts);
		
}

int
tdt_get_rank ()
{
	int me;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &me);
	return (me);
}

int 
tdt_get_np ()
{
	int np;
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	return (np);
}

#endif
