/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This is progc, and will be reading from proga on port 2222 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include "tdt.h"

int
main (int argc, char **argv) { 

    /* variables from proga */
    int kcoup_ac;
    int kice_ac;
    int kocnd_ac;
    int kmask_ac;
    int klsr_ac;
    int klwr_ac;
    double t0_ac;
    double sigm_ac;
    double cle_ac;
    double clm_ac;
    double rw_ac;
    double ro_ac;
    double cr_ac;
    double cav_ac;
    double cpgn_ac;
    double ra_ac;
    int ntsmx_ac;
    int ntsmxr_ac;
    int ntsr_ac;
    int nyrstamp_ac;
    int nyra_ac;
    int mona_ac;
    int njula_ac;
    double tyer_ac;
    double tst_ac;
    double tday_ac;
    int it_ac;
    int ns_ac;
    double dy_ac;
    double dxt_ac[18][14];
    double sqra_ac[18][14];
    double sqro_ac[18][14];
    double frocn_ac[18][14];
    double frlnd_ac[18][14];
    double frglc_ac[18][14];
    double st_ac[18][14];
    double sd_ac[18][14];
    double sg_ac[18][14];
    double horo_ac[18][14];
    double sigoro_ac[18][14];
    int idir_ac[18][14][18][14];
    double froffa_ac[18][14][18][14];
    double fr_ac[18][14];
    double asf_ac[18][14][43];

    /* variables from progo */
    double dtts_oc;
    int init_oc;
    double runlen_oc;
    int eqyear_oc;
    int eqmon_oc;
    int year0_oc;
    int month0_oc;
    int day0_oc;
    double dttsit_oc;
    double dttsid_oc;
    int imt_oc;
    int jmt_oc;
    double dzt_oc[13];
    double dxtdeg_oc[72];
    double dytdeg_oc[38];
    double xt_oc[72];
    double yt_oc[38];
    double dxt_oc[72];
    double dyt_oc[38];
    double cst_oc[38];
    double map_oc[72][38];
    double sbcocn_oc[72][38][10];
    /* double sbcocn_co[72][38][20]; */
    
    /* progc writes the following to proga */
    double asf_ca[18][14][43];

    /* progc write the following to progo */
    double sbcocn_co[72][38][30];
    
    TDTState ts_ac;
    TDTState ts_ca;
    TDTState ts_co;
    TDTState ts_oc;

    /* some loop counters */
    int i,j,k,l;

    ts_ac = tdt_init ("exdat_ac.xml");
    ts_ca = tdt_init ("exdat_ca.xml");
    ts_co = tdt_init ("exdat_co.xml");
    ts_oc = tdt_init ("exdat_oc.xml");

    ts_ac = tdt_open_socket (ts_ac, "localhost", 2222, READ);
    ts_ca = tdt_open_socket (ts_ca, "localhost", 2224, WRITE);
    ts_co = tdt_open_socket (ts_co, "localhost", 2226, WRITE);
    ts_oc = tdt_open_socket (ts_oc, "localhost", 2228, READ);

    /* PROGA - Deal with proga first */
    /* reads from proga */
    tdt_read (&kcoup_ac, "kcoup_ac", ts_ac);
    tdt_read (&kice_ac, "kice_ac", ts_ac);
    tdt_read (&kocnd_ac, "kocnd_ac", ts_ac);
    tdt_read (&kmask_ac, "kmask_ac", ts_ac);
    tdt_read (&klsr_ac, "klsr_ac", ts_ac);
    tdt_read (&klwr_ac, "klwr_ac", ts_ac);
    tdt_read (&t0_ac, "t0_ac", ts_ac);
    tdt_read (&sigm_ac, "sigm_ac", ts_ac);
    tdt_read (&cle_ac, "cle_ac", ts_ac);
    tdt_read (&clm_ac, "clm_ac", ts_ac);
    tdt_read (&rw_ac, "rw_ac", ts_ac);
    tdt_read (&ro_ac, "ro_ac", ts_ac);
    tdt_read (&cr_ac, "cr_ac", ts_ac);
    tdt_read (&cav_ac, "cav_ac", ts_ac);
    tdt_read (&cpgn_ac, "cpgn_ac", ts_ac);
    tdt_read (&ra_ac, "ra_ac", ts_ac);
    tdt_read (&ntsmx_ac, "ntsmx_ac", ts_ac);
    tdt_read (&ntsmxr_ac, "ntsmxr_ac", ts_ac);
    tdt_read (&ntsr_ac, "ntsr_ac", ts_ac);
    tdt_read (&nyrstamp_ac, "nyrstamp_ac", ts_ac);
    tdt_read (&nyra_ac, "nyra_ac", ts_ac);
    tdt_read (&mona_ac, "mona_ac", ts_ac);
    tdt_read (&njula_ac, "njula_ac", ts_ac);
    tdt_read (&tyer_ac, "tyer_ac", ts_ac);
    tdt_read (&tst_ac, "tst_ac", ts_ac);
    tdt_read (&tday_ac, "tday_ac", ts_ac);
    tdt_read (&it_ac, "it_ac", ts_ac);
    tdt_read (&ns_ac, "ns_ac", ts_ac);
    tdt_read (&dy_ac, "dy_ac", ts_ac);
    tdt_read (&dxt_ac, "dxt_ac", ts_ac);
    tdt_read (&sqra_ac, "sqra_ac", ts_ac);
    tdt_read (&sqro_ac, "sqro_ac", ts_ac);
    tdt_read (&frocn_ac, "frocn_ac", ts_ac);
    tdt_read (&frlnd_ac, "frlnd_ac", ts_ac);
    tdt_read (&frglc_ac, "frglc_ac", ts_ac);
    tdt_read (&st_ac, "st_ac", ts_ac);
    tdt_read (&sd_ac, "sd_ac", ts_ac);
    tdt_read (&sg_ac, "sg_ac", ts_ac);
    tdt_read (&horo_ac, "horo_ac", ts_ac);
    tdt_read (&sigoro_ac, "sigoro_ac", ts_ac);
    tdt_read (&idir_ac, "idir_ac", ts_ac);
    tdt_read (&froffa_ac, "froffa_ac", ts_ac);
    tdt_read (&fr_ac, "fr_ac", ts_ac);
    tdt_read (&asf_ac, "asf_ac", ts_ac);

    /* writes to proga */
    memmove (asf_ca, asf_ac, sizeof (double) * 18 * 14 * 43);
    tdt_write (&asf_ca, "asf_ca", ts_ca);
    /* PROGA END */

    /* PROGO - deal with progo now */
    for (i=0; i<72; i++) {
        for (j=0; j<38; j++) {
            for (k=0; k<30; k++) {
                sbcocn_co[i][j][k] = i*j*k*3.5;
            }
        }
    }
    
    tdt_write (&sbcocn_co, "sbcocn_co", ts_co);

    /* reads from progo */
    tdt_read (&dtts_oc, "dtts_oc", ts_oc);
    tdt_read (&init_oc, "init_oc", ts_oc);
    tdt_read (&runlen_oc, "runlen_oc", ts_oc);
    tdt_read (&eqyear_oc, "eqyear_oc", ts_oc);
    tdt_read (&eqmon_oc, "eqmon_oc", ts_oc);
    tdt_read (&year0_oc, "year0_oc", ts_oc);
    tdt_read (&month0_oc, "month0_oc", ts_oc);
    tdt_read (&day0_oc, "day0_oc", ts_oc);
    tdt_read (&dttsit_oc, "dttsit_oc", ts_oc);
    tdt_read (&dttsid_oc, "dttsid_oc", ts_oc);
    tdt_read (&imt_oc, "imt_oc", ts_oc);
    tdt_read (&jmt_oc, "jmt_oc", ts_oc);
    tdt_read (&dzt_oc, "dzt_oc", ts_oc);
    tdt_read (&dxtdeg_oc, "dxtdeg_oc", ts_oc);
    tdt_read (&dytdeg_oc, "dytdeg_oc", ts_oc);
    tdt_read (&xt_oc, "xt_oc", ts_oc);
    tdt_read (&yt_oc, "yt_oc", ts_oc);
    tdt_read (&dxt_oc, "dxt_oc", ts_oc);
    tdt_read (&dyt_oc, "dyt_oc", ts_oc);
    tdt_read (&cst_oc, "cst_oc", ts_oc);
    tdt_read (&map_oc, "map_oc", ts_oc);
    tdt_read (&sbcocn_oc, "sbcocn_oc", ts_oc);
//    tdt_read (&sbcocn_co, "sbcocn_co", ts_oc);

    /* END PROGO */

    /* test the output */
/*    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            printf("%f ", fr_ac[i][j]);
        }
        printf("\n");
    }
*/
    tdt_end (ts_ac);
    tdt_end (ts_ca);
    tdt_end (ts_co);
    tdt_end (ts_oc);

    return 0;

}
