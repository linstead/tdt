/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "tdt.h"

int
main (int argc, char **argv) {

    double new_double;
    
	struct {
		int         firstthing[2];
		double      secondthing;
	} astruct;

    int new_int;
	TDTState    ts;

	astruct.firstthing[0] = 10;
	astruct.firstthing[1] = 20;
	astruct.secondthing = 199.99;

    new_double = 123.45;
    new_int = 12345;
    
	ts = tdt_init ("example.xml");
	ts = tdt_open_socket (ts, "pc115", 2222, WRITE);

    tdt_write (&new_double, "new_double", ts);
	tdt_write (&astruct, "astruct", ts);
    tdt_write (&new_int, "new_int", ts);
    
	tdt_end (ts);

	return 0;
}
