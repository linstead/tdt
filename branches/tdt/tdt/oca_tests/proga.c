/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This is proga, and will be writing to progc on port 2222 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "tdt.h"

int
main (int argc, char **argv) { 

    int kcoup_ac;
    int kice_ac;
    int kocnd_ac;
    int kmask_ac;
    int klsr_ac;
    int klwr_ac;
    double t0_ac;
    double sigm_ac;
    double cle_ac;
    double clm_ac;
    double rw_ac;
    double ro_ac;
    double cr_ac;
    double cav_ac;
    double cpgn_ac;
    double ra_ac;
    int ntsmx_ac;
    int ntsmxr_ac;
    int ntsr_ac;
    int nyrstamp_ac;
    int nyra_ac;
    int mona_ac;
    int njula_ac;
    double tyer_ac;
    double tst_ac;
    double tday_ac;
    int it_ac;
    int ns_ac;
    double dy_ac;
    double dxt_ac[18][14];
    double sqra_ac[18][14];
    double sqro_ac[18][14];
    double frocn_ac[18][14];
    double frlnd_ac[18][14];
    double frglc_ac[18][14];
    double st_ac[18][14];
    double sd_ac[18][14];
    double sg_ac[18][14];
    double horo_ac[18][14];
    double sigoro_ac[18][14];
    int idir_ac[18][14][18][14];
    double froffa_ac[18][14][18][14];
    double fr_ac[18][14];
    double asf_ac[18][14][43];

    /* will read this from progc */
    double asf_ca[18][14][43];
    
    TDTState ts_ac;
    TDTState ts_ca;

    /* some loop counters */
    int i,j,k,l;

    /* initialising the variables starts here */
    kcoup_ac    = 1;
    kice_ac     = 2;
    kocnd_ac    = 3;
    kmask_ac    = 4;
    klsr_ac     = 5;
    klwr_ac     = 6;
    t0_ac       = 7.7;
    sigm_ac     = 8.8;
    cle_ac      = 9.9;
    clm_ac      = 10.1;
    rw_ac       = 11.11;
    ro_ac       = 12.12;
    cr_ac       = 13.13;
    cav_ac      = 14.14;
    cpgn_ac     = 15.15;
    ra_ac       = 16.16;
    ntsmx_ac    = 17;
    ntsmxr_ac   = 18;
    ntsr_ac     = 19;
    nyrstamp_ac = 20;
    nyra_ac     = 21;
    mona_ac     = 22;
    njula_ac    = 23;
    tyer_ac     = 24.24;
    tst_ac      = 25.25;
    tday_ac     = 26.26;
    it_ac       = 27;
    ns_ac       = 28;
    dy_ac       = 29.29;

    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            dxt_ac[i][j]    = i*j*1.0;
            sqra_ac[i][j]   = i*j*2.0;
            sqro_ac[i][j]   = i*j*3.0;
            frocn_ac[i][j]  = i*j*4.0;
            frlnd_ac[i][j]  = i*j*5.0;
            frglc_ac[i][j]  = i*j*6.0;
            st_ac[i][j]     = i*j*7.0;
            sd_ac[i][j]     = i*j*8.0;
            sg_ac[i][j]     = i*j*9.0;
            horo_ac[i][j]   = i*j*10.0;
            sigoro_ac[i][j] = i*j*11.0;
        }
    }

    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            for (k=0; k<18; k++) {
                for (l=0; l<14; l++) {
                    idir_ac[i][j][k][l] = i*j*k*l*12;
                    froffa_ac[i][j][k][l] = i*j*k*l*13.0;;
                }
            }
        }
    }

    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            fr_ac[i][j] = i*j*14.0;
        }
    }

    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            for (k=0; k<43; k++) {
                asf_ac[i][j][k] = i*j*k*15.0;
            }
        }
    }
    /* END of initialisation */

    ts_ac = tdt_init ("exdat_ac.xml");
    ts_ca = tdt_init ("exdat_ca.xml");

    ts_ac = tdt_open_socket (ts_ac, "localhost", 2222, WRITE);
    ts_ca = tdt_open_socket (ts_ca, "localhost", 2224, READ);

    /* tdt_writes start here */

    tdt_write (&kcoup_ac, "kcoup_ac", ts_ac);
    tdt_write (&kice_ac, "kice_ac", ts_ac);
    tdt_write (&kocnd_ac, "kocnd_ac", ts_ac);
    tdt_write (&kmask_ac, "kmask_ac", ts_ac);
    tdt_write (&klsr_ac, "klsr_ac", ts_ac);
    tdt_write (&klwr_ac, "klwr_ac", ts_ac);
    tdt_write (&t0_ac, "t0_ac", ts_ac);
    tdt_write (&sigm_ac, "sigm_ac", ts_ac);
    tdt_write (&cle_ac, "cle_ac", ts_ac);
    tdt_write (&clm_ac, "clm_ac", ts_ac);
    tdt_write (&rw_ac, "rw_ac", ts_ac);
    tdt_write (&ro_ac, "ro_ac", ts_ac);
    tdt_write (&cr_ac, "cr_ac", ts_ac);
    tdt_write (&cav_ac, "cav_ac", ts_ac);
    tdt_write (&cpgn_ac, "cpgn_ac", ts_ac);
    tdt_write (&ra_ac, "ra_ac", ts_ac);
    tdt_write (&ntsmx_ac, "ntsmx_ac", ts_ac);
    tdt_write (&ntsmxr_ac, "ntsmxr_ac", ts_ac);
    tdt_write (&ntsr_ac, "ntsr_ac", ts_ac);
    tdt_write (&nyrstamp_ac, "nyrstamp_ac", ts_ac);
    tdt_write (&nyra_ac, "nyra_ac", ts_ac);
    tdt_write (&mona_ac, "mona_ac", ts_ac);
    tdt_write (&njula_ac, "njula_ac", ts_ac);
    tdt_write (&tyer_ac, "tyer_ac", ts_ac);
    tdt_write (&tst_ac, "tst_ac", ts_ac);
    tdt_write (&tday_ac, "tday_ac", ts_ac);
    tdt_write (&it_ac, "it_ac", ts_ac);
    tdt_write (&ns_ac, "ns_ac", ts_ac);
    tdt_write (&dy_ac, "dy_ac", ts_ac);
    tdt_write (&dxt_ac, "dxt_ac", ts_ac);
    tdt_write (&sqra_ac, "sqra_ac", ts_ac);
    tdt_write (&sqro_ac, "sqro_ac", ts_ac);
    tdt_write (&frocn_ac, "frocn_ac", ts_ac);
    tdt_write (&frlnd_ac, "frlnd_ac", ts_ac);
    tdt_write (&frglc_ac, "frglc_ac", ts_ac);
    tdt_write (&st_ac, "st_ac", ts_ac);
    tdt_write (&sd_ac, "sd_ac", ts_ac);
    tdt_write (&sg_ac, "sg_ac", ts_ac);
    tdt_write (&horo_ac, "horo_ac", ts_ac);
    tdt_write (&sigoro_ac, "sigoro_ac", ts_ac);
    tdt_write (&idir_ac, "idir_ac", ts_ac);
    tdt_write (&froffa_ac, "froffa_ac", ts_ac);
    tdt_write (&fr_ac, "fr_ac", ts_ac);
    tdt_write (&asf_ac, "asf_ac", ts_ac);

    tdt_read (&asf_ca, "asf_ca", ts_ca);

    
    /* test the output */
/*    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            printf("%f ", fr_ac[i][j]);
        }
        printf("\n");
    }*/

/*    for (i=0; i<18; i++) {
        for (j=0; j<14; j++) {
            for (k=0; k<43; k++) {
                printf ("%f ", asf_ac[i][j][k]);
            }
            printf ("\n");
        }
    }*/
    /* tdt_writes finish here */

    tdt_end (ts_ac);
    tdt_end (ts_ca);

    return 0;

}
