/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Windows changes 29-09-2002, Dan
 * Rewritten the functions open_client and open_server (look for #ifdef WIN32).
 * Closed socket directly with closesocket in close_socket.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef WIN32

#include <winsock.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#endif
#include "typeops.h"
#include "addrops.h"
#include "primops.h"
#include "varops.h"
#include "tdt_map.h"
#include "tdterror.h"

TDTAddr
open_client (TDTAddr ad);
TDTAddr
open_server (TDTAddr ad);
	
void
(*readfuns[]) (void *, void *, void *, void *) = {
	read_char,
	read_int,
	read_float, 
    read_double, 
    map_addr, 
    map_array, 
    map_struct
};

void
(*writefuns[]) (void *, void *, void *, void *) = {
	write_char,
	write_int,
	write_float, 
    write_double, 
    map_addr, 
    map_array, 
    map_struct
};
void
(*readfuns_blocks[]) (void *, void *, void *, void *) = {
	read_char,
	read_int,
	read_float, 
    read_double, 
    map_addr, 
    map_array_readblocks, 
    map_struct
};

void
(*writefuns_blocks[]) (void *, void *, void *, void *) = {
	write_char,
	write_int,
	write_float, 
    write_double, 
    map_addr, 
    map_array_writeblocks, 
    map_struct
};

#ifdef _MPI
void
(*readfuns_mpi[]) (void *, void *, void *, void *) = {
    read_char_mpi,
    read_int_mpi,
    read_float_mpi, 
    read_double_mpi,
    map_addr, 
    map_array_readblocks_mpi,
    map_struct
};

void
(*writefuns_mpi[]) (void *, void *, void *, void *) = {
    write_char_mpi,
    write_int_mpi,
    write_float_mpi, 
    write_double_mpi,
    map_addr, 
    map_array_writeblocks_mpi,
    map_struct
};

#endif

/* Returns NULL on error */
TDTAddr
mkfileaddr (String fname, Mode mode) {

	TDTAddr     ad = (TDTAddr) malloc (sizeof (struct tdtaddr));
	FileAddr    fa = (FileAddr) malloc (sizeof (struct faddr));

    if (ad == NULL) {
        tdt_error ("Could not allocate memory for ad in mkfileaddr");
        return NULL;
    }

    if (fa == NULL) {
        tdt_error ("Could not allocate memory for fa in mkfileaddr");
        return NULL;
    }
    
    if (fname == NULL) {
        tdt_error ("NULL String passed to mkfileaddr");
        return NULL;
    }
    
    if ((mode != READ) && (mode != WRITE)) {
        tdt_error ("Mode in mkfileaddr not READ or WRITE");
        return NULL;
    }
    
	fa->mode = mode;
	fa->fname = fname;

	ad->prot = FILEP;
	ad->addr = fa;
	ad->fp = NULL;

	return ad;
}

/* Returns NULL on error */
TDTAddr
file_open (TDTAddr ad) {

	FileAddr    fa;
	char       *mode;
	FILE       *fp;

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to file_open");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to file_open");
        return NULL;
    }
    
    fa = (FileAddr) ad->addr;
    
	mode = ((fa->mode) == READ) ? "r" : "w";
    
	fp = fopen (fa->fname, mode);

	if (fp == NULL) {
		tdt_error ("fopen returned NULL file pointer in file_open");
		return NULL;
	}
    
	ad->fp = fp;
	return ad;
}

/* Returns NULL on error */
TDTAddr
mksocketaddr (String hostname, int port, Mode mode) {

	TDTAddr     ad = (TDTAddr) malloc (sizeof (struct tdtaddr));
	SocketAddr  sa = (SocketAddr) malloc (sizeof (struct saddr));

    if (ad == NULL) {
        tdt_error ("Could not allocate memory for ad in mksocketaddr");
        return NULL;
    }

    if (sa == NULL) {
        tdt_error ("Could not allocate memory for sa in mksocketaddr");
        return NULL;
    }
    
    if (hostname == NULL) {
        tdt_error ("NULL String passed to mkfileaddr");
        return NULL;
    }
    
    if ((mode != READ) && (mode != WRITE)) {
        tdt_error ("Mode in mksocketaddr not READ or WRITE");
        return NULL;
    }

	sa->hostname = hostname;
	sa->port = port;
	sa->mode = mode;
	ad->prot = SOCKETP;
	ad->addr = sa;
	ad->fp = NULL;

	return ad;
}


TDTAddr
socket_open (TDTAddr ad) {

	SocketAddr sa;
	TDTAddr    ta = (TDTAddr) malloc (sizeof (struct tdtaddr));
    
    if (ta == NULL) {
        tdt_error ("Could not allocate memory for ta in socket_open");
        return NULL;
    }

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to socket_open");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to socket_open");
        return NULL;
    }
    
    sa = (SocketAddr) ad->addr;
    
	if (sa->mode == READ) {
        ta = open_server (ad);
        
        if (ta == NULL) {
            tdt_error ("open_server returned NULL in socket_open");
            return NULL;
        }
    }
    else {
        ta = open_client (ad);

        if (ta == NULL) {
            tdt_error ("open_client returned NULL in socket_open");
            return NULL;
        }
    }

    return ta;
}

TDTAddr     
(*open_addr[]) (TDTAddr) = {
    file_open, 
    socket_open
};

TDTAddr
tdt_open_addr (TDTAddr ad) {

	TDTAddr ta      = (TDTAddr) malloc (sizeof (struct tdtaddr));
    TDTAddr temp_ta = (TDTAddr) malloc (sizeof (struct tdtaddr));
    
    if (ta == NULL) {
        tdt_error ("Could not allocate memory for ta in tdt_open");
        return NULL;
    }
    
    temp_ta = open_addr[ad->prot] (ad);
    if (temp_ta == NULL) {
        tdt_error ("open_addr returned NULL in tdt_open");
        return NULL;
    }

	memmove (ta, temp_ta, sizeof(struct tdtaddr));

    if (ta == NULL) {
        tdt_error ("open_addr returned NULL in tdt_open");
        return NULL;
    }
    
    free (temp_ta);
    return ta;
}

/*
 * The close_file and close_socket functions are, as you see, identical.
 * Therefore, we could have dispensed with the close_addr array of
 * functions. I think we should keep it for consistency, in case we
 * extend with other kinds of addresses.
 * Exits with status 1 on error.
 */

void
close_file (TDTAddr ad) {

	free (ad->addr);
    
	if (fclose (ad->fp) != 0) {
        tdt_error ("fclose failed in close_file");
        exit (1);
    }
    
	free (ad);
}

/* Exits with status 1 on error */
void
close_socket (TDTAddr ad) {

	free (ad->addr);
#ifdef WIN32 
	closesocket(ad->sock);
#else 
	if (fclose (ad->fp) != 0) {
        	tdt_error ("fclose failed in close_socket");
	        exit (1);
    	}
#endif    
	free (ad);
}

void
(*close_addr[]) (TDTAddr ad) = {
    close_file, 
    close_socket,
#ifdef _MPI
    close_mpi
#endif	
};

void
tdt_close_addr (TDTAddr ad) {

	close_addr[ad->prot] (ad);
}

int
tdt_recv (void *var, TypeDesc td, TDTAddr ad) {

#ifdef _MPI

	if(ad->prot == MPI) {
		tdt_map (var, td, ad, readfuns_mpi);
		return 0;
	}
#endif
	if(ad->transfer_blocks)
		tdt_map (var, td, ad, readfuns_blocks);
	else
		tdt_map (var, td, ad, readfuns);
	return 0;
}

/* Returns 1 on error */
int
tdt_send (void *var, TypeDesc td, TDTAddr ad) {

#ifdef _MPI
	if(ad->prot == MPI) {
		tdt_map (var, td, ad, writefuns_mpi);
		return 0;
	}
#endif

	tdt_map (var, td, ad, writefuns_blocks);
    
	if (fflush (ad->fp) != 0) {
        tdt_error ("fflush failed in tdt_send");
        return 1;
    }

	return 0;
}



#ifndef WIN32

/* Returns NULL on error */
TDTAddr
open_server (TDTAddr ad) {

	SocketAddr  sa;
	int         sfd;
	struct      sockaddr_in sa_in;
	struct      sockaddr_in *clnt_addr;
	socklen_t   *clnt_len;
	int         realsfd;
	FILE        *fp;
	int			max_tries = 10;
	int			i;
   	int         *known_int;
	int opt = 1;

    known_int = (int *) malloc (sizeof(int));

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to open_server");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to open_server");
        return NULL;
    }
    
    sa = (SocketAddr) ad->addr;

	sfd = socket (PF_INET,      /* IPv4, for now */
				  SOCK_STREAM,  /* reliable byte stream */
				  0);           /* default protocol: tcp/ip */
    
	if (sfd < 0) {	
		tdt_error ("Cannot open socket in open_server");
		return NULL;
	}

	memset (&sa_in, 0, sizeof (struct sockaddr_in));

	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons (sa->port);
	/* ignore the hostname argument, serve any address */
	sa_in.sin_addr.s_addr = htonl (INADDR_ANY);

	/*to avoid the port to remain opened - uncomment if necessary*/
	setsockopt (sfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof (opt));
									  
	/* Sometimes, bind fails because the operating system has not closed
	 * a socket. We retry a number of max_tries, before we give up. */
	i = 0;
	while (i < max_tries) {
		if (0 > bind (sfd, (struct sockaddr *) &sa_in, sizeof (sa_in))) {
			tdt_error ("Bind failed in open_server... retrying");
			sleep (1);
			i++;
		} else {
			i = 0;
			break;
		}
	}
	if (i != 0) {
		tdt_error ("Bind failed too many times: aborting...");
		return NULL;
	}

	/* start listening */
	if (0 > listen (sfd, 1)) {
		tdt_error ("Listen failed in open_server");
		return NULL;
	}

	clnt_addr = (struct sockaddr_in *) malloc (sizeof (struct
													   sockaddr_in));

    if (clnt_addr == NULL) {
        tdt_error ("Cannot allocate memory for clnt_addr in open_server");
        return NULL;
    }
    
	clnt_len = (socklen_t *) malloc (sizeof (socklen_t));

    if (clnt_len == NULL) {
        tdt_error ("Cannot allocate memory for clnt_len in open_server");
        return NULL;
    }

	realsfd = accept (sfd, (struct sockaddr *) clnt_addr, clnt_len);

    if (realsfd < 0) {
        tdt_error ("accept failed in open_server");
        return NULL;
    }

	close (sfd);

	fp = fdopen (realsfd, "r");
	if (fp == NULL) {
		tdt_error ("fdopen returned NULL file pointer in open_server");
		return NULL;
	}

	ad->fp = fp;

    read_int(known_int, NULL, ad, NULL);
    /*fprintf(stderr, "known int is %d", *known_int);*/

    if (*known_int != 42) {
        /* Endianness must be different, need to do byte-swapping */
        set_swap (ad, 1);
        /*Won't read by blocks*/
        ad->transfer_blocks = FALSE;
    } else {
        /* Endianness is the same, nothing to do */
        set_swap (ad, 0);
        /*Read by blocks*/
        ad->transfer_blocks = TRUE;
    }

    free(known_int);

	return ad;

}

/* Returns NULL on error */
TDTAddr
open_client (TDTAddr ad) {

	SocketAddr  sa;
	int         sfd;
	struct      sockaddr_in sa_in;
	FILE        *fp;
	struct      hostent *hostinfo;	/* used to retrieve the ip */
	Boolean     firsttry;
    
	struct      sockaddr_in me;
    int         i, len_me;
    int         *known_int;

    known_int = (int *) malloc (sizeof(int));
    *known_int = 42;

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to open_client");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to open_client");
        return NULL;
    }
    
    sa = (SocketAddr) ad->addr;
    
	memset (&sa_in, 0, sizeof (sa_in));
	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons (sa->port);
	hostinfo = gethostbyname (sa->hostname);
    
	if (hostinfo == NULL) {
		tdt_error ("gethostbyname returned a NULL hostent in open_client");
		return NULL;
	}
    
	sa_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;

	firsttry = TRUE;
	while (TRUE) {
        
        while (TRUE) {

		    sfd = socket (PF_INET, SOCK_STREAM, 0);
        
    		if (sfd < 0) {
    			tdt_error ("Cannot open socket in open_client");
    			return NULL;
    		}
            
            memset (&me, 0, sizeof (struct sockaddr_in));
            me.sin_family = AF_INET;
            me.sin_port = htons(0);
            me.sin_addr.s_addr = htonl (INADDR_ANY);
            
            i = bind (sfd, (struct sockaddr *) &me, sizeof (me));
            
            len_me = sizeof (me);
            getsockname(sfd, (struct sockaddr *) &me, &len_me);
            /*printf("binding port = %d\n",ntohs(me.sin_port));*/
            
            if (ntohs (me.sin_port) == sa->port) {
                
                /*printf ("skip port %d\n",sa->port);*/
                close (sfd);
                
            } else {
                break;
            }
        }

		if (0 > connect (sfd, (struct sockaddr *) &sa_in, sizeof (sa_in))) {
			if (firsttry == TRUE) {
				fprintf (stdout, "Waiting for server...\n");
				firsttry = FALSE;
			}
			close (sfd);
		} else {
			break;
		}
	}

	fp = fdopen (sfd, "w");
	if (fp == NULL) {
		tdt_error ("fdopen returned NULL file pointer in open_client");
		return NULL;
	}

	ad->fp = fp;

	/* For endian check */
	write_int(known_int, NULL, ad, NULL);

	free(known_int);

	return ad;
}

#else 

/* Returns NULL on error */
TDTAddr
open_server (TDTAddr ad) {

	SocketAddr  sa;
	SOCKET      sfd;
	SOCKADDR_IN sa_in;
	struct      sockaddr_in *clnt_addr;
	socklen_t   clnt_len;
	SOCKET      realsfd;
	int			max_tries = 10;
	int			i;
	int         nCode; /*dan*/
	struct      WSAData wsaData;
	int			i;
    int         *known_int;

    known_int = (int *) malloc (sizeof(int));

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to open_server");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to open_server");
        return NULL;
    }
    if ((nCode = WSAStartup(MAKEWORD(1, 1), &wsaData )) != 0) {		/*dan*/
		i=WSAGetLastError();
        return NULL;
	}
    sa = (SocketAddr) ad->addr;

	sfd = socket (PF_INET,      /* IPv4, for now */
				  SOCK_STREAM,  /* reliable byte stream */
				  IPPROTO_TCP); /* default protocol: tcp/ip */
    
	if (sfd < 0) {	
		tdt_error ("Cannot open socket in open_server");
		return NULL;
	}

	memset (&sa_in, 0, sizeof (struct sockaddr_in));

	sa_in.sin_family = PF_INET;
	sa_in.sin_port = htons ((short)sa->port);
	/* ignore the hostname argument, serve any address */
	sa_in.sin_addr.s_addr = htonl (INADDR_ANY);

	/* Sometimes, bind fails because the operating system has not closed
	 * a socket. We retry a number of max_tries, before we give up. */
	i = 0;
	while (i < max_tries) {
		if (0 > bind (sfd, (LPSOCKADDR) &sa_in, sizeof (struct sockaddr))) {
			tdt_error ("Bind failed in open_server... retrying");
			Sleep (1000);
			i++;
		} else {
			i = 0;
			break;
		}
	}
	if (i != 0) {
		tdt_error ("Bind failed too many times: aborting...");
		return NULL;
	}

	/* start listening */
	if (0 > listen (sfd, 1)) {
		tdt_error ("Listen failed in open_server");
		return NULL;
	}

	clnt_addr = (struct sockaddr_in *) malloc (sizeof (struct
													   sockaddr_in));

    if (clnt_addr == NULL) {
        tdt_error ("Cannot allocate memory for clnt_addr in open_server");
        return NULL;
    }
    
	clnt_len =sizeof(struct sockaddr_in);

	realsfd = accept (sfd, (LPSOCKADDR) clnt_addr, &clnt_len);

    if (realsfd < 0) {
        tdt_error ("accept failed in open_server");
        return NULL;
    }

	ad->sock=realsfd;

    read_int(known_int, NULL, ad, NULL);
    /*fprintf(stderr, "known int is %d", *known_int);*/

    if (*known_int != 42) {
        /* Endianness must be different, need to do byte-swapping */
        set_swap (ad, 1);
        /*Won't read by blocks*/
        ad->transfer_blocks = FALSE;
    } else {
        /* Endianness is the same, nothing to do */
        set_swap (ad, 0);
        /*Read by blocks*/
        ad->transfer_blocks = TRUE;
    }

    free(known_int);

	return ad;

}

/* Returns NULL on error */
TDTAddr
open_client (TDTAddr ad) {

	SocketAddr  sa;
	SOCKET      sfd;
	SOCKADDR_IN sa_in;
	struct      hostent *hostinfo;	/* used to retrieve the ip */
	Boolean     firsttry;
	struct      WSAData wsaData;
	int         nCode;
    
	SOCKADDR_IN me;
    int         i, len_me;
    int         *known_int;

    known_int = (int *) malloc (sizeof(int));
    *known_int = 42;

    if (ad == NULL) {
        tdt_error ("NULL TDTAddr passed to open_client");
        return NULL;
    }
    
    if (ad->addr == NULL) {
        tdt_error ("NULL ad->addr passed to open_client");
        return NULL;
    }
   	if ((nCode = WSAStartup(MAKEWORD(1, 1), &wsaData )) != 0) {
        return NULL;
	}

    sa = (SocketAddr) ad->addr;
    
	memset (&sa_in, 0, sizeof (sa_in));
	sa_in.sin_family = PF_INET;
	sa_in.sin_port = htons ((short)sa->port);
	hostinfo = gethostbyname (sa->hostname);
    
	if (hostinfo == NULL) {
		tdt_error ("gethostbyname returned a NULL hostent in open_client");
		return NULL;
	}
    
	sa_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;

	firsttry = TRUE;
	while (TRUE) {
        
        sfd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
       
        if (sfd < 0) {
            tdt_error ("Cannot open socket in open_client");
            return NULL;
        }
        
		if (0 > connect (sfd, (LPSOCKADDR) &sa_in, sizeof (struct sockaddr))) {
			if (firsttry == TRUE) {
				fprintf (stdout, "Waiting for server...\n");
				firsttry = FALSE;
			}
		} else {
			break;
		}
	}

	ad->sock=sfd;

	/* For endian check */
	write_int(known_int, NULL, ad, NULL);

	free(known_int);

	return ad;
}
#endif

void
set_swap (TDTAddr ta, int requires_swap) {

    ta->requires_swap = requires_swap;
}

int
get_swap (TDTAddr ta) {

    return ta->requires_swap;
}


#ifdef _MPI

TDTAddr
mkmpiaddr (int i) {

	MPIAddr ma = (MPIAddr) malloc (sizeof (struct mpiaddr));
	TDTAddr ad = (TDTAddr) malloc (sizeof (struct tdtaddr));
	
	ma->rank = i;
	ad->prot = MPI;
	ad->addr = ma;

	return ad;
}

void
close_mpi (TDTAddr ad) {

	free (ad->addr);
	free (ad);
}
#endif	
