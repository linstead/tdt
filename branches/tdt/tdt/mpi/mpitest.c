/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* MPI EXAMPLE */

#include <stdio.h>
#include <stdlib.h>
#include "mtdt.h"

int
main (int argc, char **argv) {

    int np, rank;
    /* a loop counter */
    int i;

    /* declare the variables we're going to write/read */
    double new_double1, new_double2;
    
    struct {
        int    firstthing[2];
        double secondthing;
    } astruct1, astruct2;

    int new_int1, new_int2;
    double *dynarray1, *dynarray2;

    /* declare some variables required by TDT */
    TDTState*  ts;

    ts = tdt_init_mpi ("example.xml",&argc,&argv);
    np = tdt_get_np();
    if (np != 2) {
        printf("You should use exactly 2 processors\n");
        tdt_end_mpi(ts);
    }

    rank = tdt_get_rank();
    if (rank==0) {

        /* Now make up some meaningful data to write... */
        astruct1.firstthing[0] = 10;
        astruct1.firstthing[1] = 20;
        astruct1.secondthing = 199.99;

        new_double1 = 123.45;
        new_int1 = 12345;
		
        dynarray1 = (double *) malloc (new_int1 * sizeof (double));
        for (i = 0; i < new_int1; i++) {
            dynarray1[i] = i * 0.33;
        }
		
        /* now write the data */
        tdt_write (ts[1], &new_double1, "new_double");
        tdt_write (ts[1], &astruct1, "astruct");
        tdt_write (ts[1], &new_int1, "new_int");
		
        /* we haven't yet told TDT how big dynarray will be, so do that now ... */
        tdt_size_array (ts[1], "dyn", new_int1);
        /* ... and write it */
        tdt_write (ts[1], dynarray1, "dyn");
    }

    else {

        /* start reading the data */
        tdt_read (ts[0], &new_double2, "new_double");

        tdt_read (ts[0], &astruct2, "astruct");
        printf ("qewqew\n");
		
        tdt_read (ts[0], &new_int2, "new_int");
		
        /* reading the dynamic array is done here ... */
        dynarray2 = (double *) malloc (new_int2 * sizeof (double));
        tdt_size_array (ts[0], "dyn", new_int2);
        tdt_read (ts[0], dynarray2, "dyn");

        /* the rest just prints out what we've read */
        printf ("Read this:\n");
        printf ("    %d %d\n     %f\n", astruct2.firstthing[0],
            astruct2.firstthing[1], astruct2.secondthing);
        printf ("new_int is %d\n", new_int2);
        printf ("new_double is %f\n", new_double2);
        for (i = 0; i < new_int2; i+=100) {
            printf ("dyn[%d]=%f\n", i, dynarray2[i]);
        }
    }
	
    return 0;
}
