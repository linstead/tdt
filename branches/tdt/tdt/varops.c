/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


/*
 * File varops.c : Operations on variables.
 *
 * The operations defined here are the default ones for compound
 * variables. Together with operations on primitive variables, they can
 * be used in an array of functions and passed to the functions tdt_map.
 * The idea is that these functions are probably be going to be used as
 * they are, but some experiments might require changes to them. The
 * primitive operations are most likely to change, whereas tdt_map is
 * not likely to change at all.
 * 
 * Change History:
 *          - 02112001, originated by Cezar
 *          
 * 05112001 - 09112001, Ciaron.
 * Added "void *args" to map_composite, map_addr, map_array and
 * map_struct as per TODO file.
 * 12112001 - 14112001, Ciaron.
 * funarray now passed via a void*, and cast to array of function
 * pointers...see NOTES for an explanation.
 * 27/11/01, Cezar
 * Added code to solve the structure offset problem. Lots of comments
 * explaining how it works.
 * 29/11/01, Cezar
 * Unified the primitive and compound types dispatch.
 * 
 */

/* Windows changes 29-09-2002, Dan
 * Changed map_struct (repaired allignement bug)
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tdterror.h"
#include "typeops.h"
#include "varops.h"
#include "addrops.h"
#include "primops.h"


/* Crashes on error -- exit (1) */
void
map_addr (void *val, void *type_val, void *args, void *functions) {


	AddrDesc    ad = (AddrDesc) type_val;
	TypeDesc    td = addr_type  	(ad);
	TypeName    tn = type_name  	(td);
	TypeVal     tv = type_value 	(td);
	char      **newval;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_addr");
		exit (1);
	}
	if (ad == NULL) {
		tdt_error ("NULL ad in map_addr.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_addr.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_addr.");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_addr.");
		exit (1);
	}
	/* End of error handling section */

	if (TRUE == is_primitive (tn)) {	/* No redirection needed */
		funarray[tn] (val, tv, args, functions);
	} else {
		newval = (char **) val;	/* One level indirection */
		funarray[tn] ((*newval), tv, args, functions);
	}

}


/* Crashes on error -- exit (1) */
void
map_array (void *val, void *type_val, void *args, void *functions) {

	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	int         size = array_sz (ar);
	int         i;
	int         sz = type_size (td);
	char       *elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_array");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_array.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_array.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_array.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_array");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_array");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_array.");
		exit (1);
	}
	/* End of error handling section */


	for (i = 0; i < size; i++) {
		funarray[tn] ((elem + (i * sz)), tv, args, functions);
	}
}



/* Crashes on error -- exit (1) */
void
map_array_readblocks (void *val, void *type_val, void *args, void *functions) {

	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	TypeDesc    tda;
	int         size = array_sz (ar);
	int         i;
	int         msize;
	int         sz = type_size (td);
	char       *elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_array");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_array.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_array.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_array.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_array");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_array");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_array.");
		exit (1);
	}

	if (args == NULL) {
		tdt_error ("args parameter passed to write_double is NULL");
		exit (1);
	}

	tda = multi_array_type (ar);
	if (is_primitive (tda->name) ) {
		msize = multi_array_size (ar);
		if (msize == -1) {
			tdt_error ("multi_array_size in map_array_read");
			exit (1);
		}
		msize = msize * type_size (tda);
		read_block(val, msize, args); 
		return;
	
	}
	/* End of error handling section */

	for (i = 0; i < size; i++) {
		funarray[tn] ((elem + (i * sz)), tv, args, functions);
	}
}

/* Crashes on error -- exit (1) */
void
map_array_writeblocks (void *val, void *type_val, void *args, void *functions) {

	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	TypeDesc    tda;
	int         size = array_sz (ar);
	int         i;
	int         msize;
	int         sz = type_size (td);
	char       *elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_array");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_array.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_array.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_array.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_array");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_array");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_array.");
		exit (1);
	}
	/* End of error handling section */

	tda = multi_array_type (ar);
	if (is_primitive (tda->name)) {
		msize = multi_array_size (ar);
		if (msize == -1) {
			tdt_error ("multi_array_size in map_array_read");
			exit (1);
		}
		msize = msize * type_size (tda);
		write_block(val, msize, args); 
		return;
	
	}

	for (i = 0; i < size; i++) {
		funarray[tn] ((elem + (i * sz)), tv, args, functions);
	}
}

#ifdef _MPI

void
map_array_writeblocks_mpi (void *val, void *type_val, void *args, void *functions) {

	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	TypeDesc    tda;
	int         size = array_sz (ar);
	int         i;
	int         msize;
	int         sz = type_size (td);
	char       *elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_array");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_array.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_array.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_array.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_array");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_array");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_array.");
		exit (1);
	}
	/* End of error handling section */

	tda = multi_array_type (ar);
	if (is_primitive (tda->name)) {
		msize = multi_array_size (ar);
		if (msize == -1) {
			tdt_error ("multi_array_size in map_array_read");
			exit (1);
		}
		msize = msize * type_size (tda);
		write_block_mpi(val, msize, args); 
		return;
	
	}

	for (i = 0; i < size; i++) {
		funarray[tn] ((elem + (i * sz)), tv, args, functions);
	}
}


void
map_array_readblocks_mpi (void *val, void *type_val, void *args, void *functions) {

	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	TypeDesc    tda;
	int         size = array_sz (ar);
	int         i;
	int         msize;
	int         sz = type_size (td);
	char       *elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *);

	funarray = (void (**)(void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_array");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_array.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_array.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_array.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_array");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_array");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_array.");
		exit (1);
	}

	if (args == NULL) {
		tdt_error ("args parameter passed to write_double is NULL");
		exit (1);
	}

	tda = multi_array_type (ar);
	if (is_primitive (tda->name) ) {
		msize = multi_array_size (ar);
		if (msize == -1) {
			tdt_error ("multi_array_size in map_array_read");
			exit (1);
		}
		msize = msize * type_size (tda);
		read_block_mpi(val, msize, args); 
		return;
	
	}
	/* End of error handling section */

	for (i = 0; i < size; i++) {
		funarray[tn] ((elem + (i * sz)), tv, args, functions);
	}
}
#endif

/* Crashes on errors -- exit (1) */
void
map_struct (void *val, void *type_val, void *args, void *functions) {

	StructDesc  sd = (StructDesc) type_val;
	Decl        d;
	TypeDesc    td;
	TypeName    tn;
	TypeVal     tv;
	char       *elem = (char *) val;
	int         increment = 0;
	int			tsz;
	int			tal;
	void        (**funarray) (void *, void *, void *, void *);
	int i;
	
	funarray = (void (**)(void *, void *, void *, void *)) functions;
	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val in map_struct");
		exit (1);
	}
	if (sd == NULL) {
		tdt_error ("NULL type_val in map_struct");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_struct");
		exit (1);
	}
	/* End of error handling section */

	/* The problem of parsing struct vars is complicated by the offset
	 * rules for characters. When using gcc under Linux, the rules for
	 * finding out what increment to use in order to get a pointer to
	 * the next element seem to be the following:
	 *      if the current element is not a CHAR, the increment =
	 *          type_size (element)
	 *      else (the current element is a CHAR)
	 *          if the next element is also a CHAR, the increment is 1
	 *          else (the next element is not a CHAR)
	 *              let n be the number of chars *contiguously*
	 *              preceding the current one
	 *                  the increment is 5 - ((n % 4) + 1)
	 *
	 * The last line is explained as follows:
	 *      if there were no preceeding CHARs, or 4, or a multiple of
	 *      four, the current char takes up a four byte chunk by itself,
	 *      therefore we should increment by four bytes to find the next
	 *      element
	 *
	 *      if there was one preceeding CHAR (or 5, or 4*m + 1 for some
	 *      m), then it plus the current one take up four bytes
	 *      together, the current one takes up three of these, and
	 *      therefore we need to increment by three to find the next
	 *      element.
	 *
	 *      similarly, for n = 2 (or 4*m + 2), the current char will
	 *      take up the remaining two bytes, and we need to increment by
	 *      two. For n = 3 (or 4*m + 3), the current char will just take
	 *      up one byte, so we need to just increment by one.
	 *
	 *      ...and these are the only cases possible.
	 *      ...and the above formula works for all.
	 *
	 * We implement all this machinery by having a variable that stores
	 * the number of preceding contiguous CHARs. This variable is set to
	 * zero after treating a non-char, and incremented after treating a
	 * char.
	 */

	i = 0;
	reset_decls (sd);
	increment = 0;
	while (TRUE == has_more_decls (sd)) {
		d = next_decl (sd);
		td = decl_type (d);
		tn = type_name (td);
		/* Error handling section */
		if (d == NULL) {
			tdt_error ("next_decl in map_struct");
			exit (1);
		}
		if (td == NULL) {
			tdt_error ("decl_type in map_struct");
			exit (1);
		}
		if (tn == TDT_NOTYPE) {
			tdt_error ("type_name in map_struct");
			exit (1);
		}
		/* End error handling section */
		tv = type_value (td);
#ifdef _AIX
	tal = type_align(td);
	if (i == 0 && type_name(td) == TDT_DOUBLE)
		tal = 8;
	else if ( i == 0 && is_struct_one_double (td) )
		tal = 8;

	i = 1;
#else
    tal = type_align(td);
#endif
	
		if (increment > 0)
			increment += tal - (increment - 1) % tal - 1;
		funarray[tn] ((elem + increment), tv, args, functions);
		tsz = type_size (td);
		if (tsz == -1) {
			tdt_error ("type_size in map_struct");
			exit (1);
		}
		increment += tsz;
	}
}
