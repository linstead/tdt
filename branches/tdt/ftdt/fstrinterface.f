		SUBROUTINE tdt_fconfigure (tdtconf, confname)
		INTEGER tdtconf
		CHARACTER *(*) confname

		CALL tdt_fconfigure_internal (tdtconf, confname,
     *								LEN_TRIM (confname))

		END


		SUBROUTINE tdt_finit (tdtstate, tdtconf, conn)
		INTEGER tdtstate
		INTEGER tdtconf
		CHARACTER *(*) conn

		CALL tdt_finit_internal (tdtstate, tdtconf, conn, 
     *								LEN_TRIM (conn))

		END

		SUBROUTINE tdt_fopen (tdtstate, tdtconf, conn)
		INTEGER tdtstate
		INTEGER tdtconf
		CHARACTER *(*) conn

		CALL tdt_fopen_internal (tdtstate, tdtconf, conn,
     *								LEN_TRIM (conn))
		
		END

		SUBROUTINE tdt_fread (tdtstate, name, val) 
		INTEGER tdtstate
		INTEGER val
		CHARACTER *(*) name

		CALL tdt_fread_internal (tdtstate, val, 
     *						name, LEN_TRIM (name))

		END

		SUBROUTINE tdt_fwrite (tdtstate, name, val) 
		INTEGER tdtstate
		INTEGER val
		CHARACTER *(*) name

		CALL tdt_fwrite_internal (tdtstate, val, 
     *					name, LEN_TRIM (name))

		END

		SUBROUTINE tdt_fclose (tdtstate)
		INTEGER tdtstate

		CALL tdt_fclose_internal (tdtstate)

		END

		SUBROUTINE tdt_fend (tdtconf)
		INTEGER tdtconf

		CALL tdt_fend_internal (tdtconf)

		END

                SUBROUTINE tdt_finit_mpi (tdtstate, xmlname)
                INTEGER :: tdtstate, err
                CHARACTER *(*) xmlname
                
                CALL MPI_Init(err)
                CALL tdt_finit_mpi_internal (tdtstate, xmlname,
     *                                            LEN_TRIM (xmlname))
                END

                SUBROUTINE tdt_fend_mpi (tdtstate)
                INTEGER tdtstate(*)

                END

                SUBROUTINE tdt_fget_rank (k)
                INTEGER k

                CALL tdt_fget_rank_internal(k)

                END

                SUBROUTINE tdt_fget_np (k)
                INTEGER k

                CALL tdt_fget_np_internal(k)

                END
