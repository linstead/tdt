/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef WIN32

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#endif


#include "tdterror.h" 
#include <stdio.h>
#include <stdlib.h>
#ifdef _MPI
#include "mtdt.h"
#else
#include "tdt.h"
#endif

/* Returns NULL on error */
TDTState
intptots (int *pts) {
	TDTState ts;

    if (pts == NULL) {
        tdt_error ("pts is NULL in intptots");
        return NULL;
    }
    
	ts = (TDTState) (*pts);

	return ts;
}

/* Returns NULL on error */
TDTConfig
intptotc (int *ptc) {
	TDTConfig tc;

    if (ptc == NULL) {
        tdt_error ("ptc is NULL in intptotc");
        return NULL;
    }

	tc = (TDTConfig) (*ptc);

	return tc;
}

/* Exits with status 1 on error */
void
tstointp (TDTState ts, int *pts) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in tstointp");
        exit (1);
    }

	*pts = (int) ts;

}

/* Exits with status 1 on error */
void
tctointp (TDTConfig tc, int *ptc) {

    if (tc == NULL) {
        tdt_error ("TDTConfig is NULL in tctointp");
        exit (1);
    }

	*ptc = (int) tc;

}

/* Returns NULL on error */
char *
fstrtocstr (char *fstr, int len) {
	char *cstr;
	int i;

    if (fstr == NULL) {
        tdt_error ("fstr is NULL in fstrtocstr");
        return NULL;
    }
    
	cstr = (char *) malloc ((len + 1) * sizeof(char));

    if (cstr == NULL) {
        tdt_error ("Cannot allocate memory for cstr in fstrtocstr");
        return NULL;
    }
    
	for (i = 0; i < len; i++)
		cstr[i] = fstr[i];

	cstr[len] = '\0';

	return cstr;
}

/* Exits with status 1 on error */
void
#ifdef _AIX
tdt_finit_internal (int *pts, int *ptc, char *conn, int *flen) {
#else
#ifdef _MSVC
__stdcall TDT_FINIT_INTERNAL (int *pts, int *ptc, char *conn, int *flen, int nouse) {
#else
tdt_finit_internal__ (int *pts, int *ptc, char *conn, int *flen) {
#endif
#endif
    
    TDTState ts;
    
    if (flen == NULL) {
        tdt_error ("flen is NULL in tdt_finit_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "conn"
     * so no need to check here.
     */

	ts = tdt_init (intptotc(ptc), fstrtocstr(conn, *flen));

    if (ts == NULL) {
        tdt_error ("tdt_init returned NULL in tdt_finit_internal");
        exit (1);
    }
    
	tstointp (ts, pts);
}

/* exits with status 1 on error */
void
#ifdef _AIX
tdt_fopen_internal (int *pts, int *ptc, char *conn, int *flen) {
#else
#ifdef _MSVC
__stdcall TDT_FOPEN_INTERNAL (int *pts, int *ptc, char *conn,  int nouse, int *flen) {
#else
tdt_fopen_internal__ (int *pts, int *ptc, char *conn, int *flen) {
#endif
#endif

	TDTState ts;

    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fopen");
        exit (1);
    }

    if (ptc == NULL) {
        tdt_error ("ptc is NULL in tdt_fopen");
        exit (1);
    }

    if (conn == NULL) {
        tdt_error ("conn is NULL in tdt_fopen");
        exit (1);
    }

    if (flen == NULL) {
        tdt_error ("flen is NULL in tdt_fopen");
        exit (1);
    }

	ts = tdt_open (intptotc(ptc), fstrtocstr(conn, *flen));

    if (ts == NULL) {
        tdt_error ("tdt_open returned NULL ts in tdt_fopen_socket");
        exit (1);
    }

	tstointp(ts, pts);
}

/* exits with status 1 on error */
void
#ifdef _AIX
tdt_fconfigure_internal (int *ptc, char *confname, int *conflen) {
#else
#ifdef _MSVC
__stdcall TDT_FCONFIGURE_INTERNAL (int *ptc, char *confname,  int *nouse ,int *conflen) {
#else
tdt_fconfigure_internal__ (int *ptc, char *confname, int *conflen) {
#endif 
#endif

	TDTConfig tc;

    if (confname == NULL) {
        tdt_error ("confname is NULL in tdt_fconfigure_internal");
        exit (1);
    }

    tc = tdt_configure (fstrtocstr(confname, *conflen));

    if (tc == NULL) {
        tdt_error ("tdt_configure returned NULL in tdt_fconfigure_internal");
    }

    tctointp(tc, ptc);

}

/* exits with status 1 on error */
void
#ifdef _AIX
tdt_fwrite_internal (int *pts, void *val, char *name, int *namelen) {
#else
#ifdef _MSVC 
__stdcall TDT_FWRITE_INTERNAL (int *pts, void *val, char *name, int nouse, int *namelen) {
#else
tdt_fwrite_internal__ (int *pts, void *val, char *name, int *namelen) {
#endif
#endif

    TDTState ts;

    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fwrite_internal");
        exit (1);
    }
   
    if (namelen == NULL) {
        tdt_error ("namelen is NULL in tdt_fwrite_internal");
        exit (1);
    }
    
    ts = intptots (pts);
    
    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fwrite_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "name"
     * so no need to check here.
     */
	tdt_write (intptots(pts), val, fstrtocstr(name, *namelen));

}

void
#ifdef _AIX
tdt_fread_internal (int *pts, void *val, char *name, int *namelen) {
#else
#ifdef _MSVC
__stdcall TDT_FREAD_INTERNAL (int *pts, void *val, char *name,  int nouse, int *namelen) {
#else
tdt_fread_internal__ (int *pts, void *val, char *name, int *namelen ) {
#endif
#endif

    TDTState ts;
    
    if (namelen == NULL) {
        tdt_error ("namelen is NULL in tdt_fread_internal");
        exit (1);
    }

    /* call to intptots will check pts for NULL
     */
    ts = intptots (pts);
    
    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fread_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "name"
     * so no need to check here.
     */
	tdt_read(ts, val, fstrtocstr(name, *namelen));

}

/* Exits with status 1 on error */
void
#ifdef _AIX
tdt_fclose_internal (int *pts) {
#else
#ifdef _MSVC
__stdcall TDT_FCLOSE_INTERNAL (int *pts) {
#else
tdt_fclose_internal__ (int *pts) {
#endif
#endif
    TDTState ts;
	
    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fclose");
        exit (1);
    }
    
    ts = intptots(pts);

    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fclose");
        exit (1);
    }
    
	tdt_close (ts);

}

/* Exits with status 1 on error */
void
#ifdef _AIX
tdt_fend_internal (int *ptc) {
#else
#ifdef _MSVC
__stdcall TDT_FEND_INTERNAL (int *ptc) {
#else
tdt_fend_internal__ (int *ptc) {
#endif
#endif
    TDTConfig tc;

    if (ptc == NULL) {
        tdt_error ("tc is NULL in tdt_fend");
        exit(1);
    }

    tc = intptotc(ptc);

    tdt_end(tc);

}

#ifdef _MPI

void
#ifdef _AIX
tdt_finit_mpi_internal (int *pts, char *xmlfname, int *flen) {
#else
tdt_finit_mpi_internal__ (int *pts, char *xmlfname, int *flen) {
#endif

	TDTState *ts;
	
	if (flen == NULL) {
		tdt_error ("flen is NULL in tdt_finit_internal");
		exit (1);
	}

	ts = tdt_init_mpif (fstrtocstr(xmlfname, *flen));


	pts[0] = (int) ts[0];
	pts[1]=(int)ts[1];

}


void
#ifdef _AIX
tdt_fget_rank_internal (int *k) {
#else
tdt_fget_rank_internal__ (int *k) {
#endif

	int k1;
	k1 = tdt_get_rank();
	*k = k1;

}


void
#ifdef _AIX
tdt_fget_np_internal (int *k) {
#else
tdt_fget_np_internal__ (int *k) {
#endif

	int k1;
	k1 = tdt_get_np();
	*k = k1;
}

#endif
