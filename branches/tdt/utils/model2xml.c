#include <stdio.h>
#include "typeops.h"
#include "xmlhandlers.h"

void
wtabs (FILE *f, int l) {

	int i;
	for(i=0;i<l;i++)
		fprintf(f,"\t");
}


int
char2xml (void *val, int level, FILE *xml) {

	wtabs (xml, level);
	fprintf(xml, "char\n");
	wtabs (xml, level + 1);
}

int
int2xml (void *val, int level, FILE *xml) {

	wtabs (xml, level);
	fprintf(xml, "int\n");
	wtabs (xml, level + 1);
}		

int
float2xml (void *val, int level, FILE *xml) {

	wtabs (xml, level);
	fprintf(xml, "float\n");
	wtabs (xml, level + 1);
}

int
double2xml (void *val, int level, FILE *xml) {

	wtabs (xml, level);
	fprintf(xml, "double\n");
	wtabs (xml, level + 1);
}

int
addr2xml (void *val, int level, FILE *xml) {

	AddrDesc ad;
	ad = (AddrDesc) val;
	wtabs (xml, level);
	fprintf(xml, "<addr>\n");
	type2xml (ad->type, level + 1, xml);
	fprintf(xml, "</addr>");
}

int
array2xml (void *val, int level, FILE *xml) {

	ArrayDesc ar;
	ar = (ArrayDesc) val;
	wtabs (xml, level);
	fprintf(xml, "<array size=\"%d\">\n", ar->size);
	type2xml (ar->type, level + 1, xml);
	fprintf(xml, "</array>");
}

int
struct2xml (void *val, int level, FILE *xml) {
		
	StructDesc sd;
	int i;
	
	sd = (StructDesc) val;
	wtabs (xml, level);
	fprintf (xml, "<struct>\n");
	for (i=0; i < sd->num_decls; i++) 
		decl2xml (sd->decls[i], level + 1, xml);
	wtabs (xml, level);
	fprintf (xml, "</struct>");
}

int (*typefuncs[]) (void *, int, FILE*) = {
	char2xml,
	int2xml,
	float2xml,
	double2xml,
	addr2xml,
	array2xml,
	struct2xml
};
	
int
type2xml (TypeDesc td, int level, FILE *xml) {

	typefuncs[td->name] (td->value, level, xml);
}

int
decl2xml ( Decl decl, int level, FILE *xml) {
	
	int i;
	StructDesc sd;

	wtabs(xml,level);
	fprintf(xml, "<decl name=\"%s\">\n", decl->name);
	type2xml (decl->type, level + 1, xml);
	fprintf(xml, "</decl>\n");
	
}

int
model2xml (DataDesc md, char *xmlname) {

	
	int i;
	FILE *xml;

	xml = fopen (xmlname, "w");
	fprintf(xml,"<data_desc>\n");
	for (i = 0; i < md->nd; i++)
		decl2xml (md->ad[i], 1, xml);
	fprintf(xml,"</data_desc>");
	fclose(xml);
}

