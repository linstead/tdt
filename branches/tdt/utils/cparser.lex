%{
#include "defs.h"
//#include "y.tab.h"
#include <stdlib.h>
char num;
int state;
int level = 0;
char *name;
int line = 1;
#define MAX_LEVEL 100

%}
N       [0-9]
A       [A-Z_a-z] 
AN      [0-9A-Z_a-z]

%%
struct 	{state = STRUCT; return(STRUCT);}
char	{state = CHAR;  return(CHAR);}
int	{state = INT;  return(INT);}
float	{state = FLOAT;  return(FLOAT);}
double	{state = DOUBLE; return(DOUBLE);}


\{	{level++; return  *yytext;}
\}      {level--; return  *yytext;}
{N}+	{state = NUMBER; 
	if(name == NULL)
		name=(char*) malloc (strlen(yytext));
	else
		name=(char*) realloc ((void*) name,strlen(yytext));
	strcpy(name,yytext);		
	return(NUMBER);}

{A}{AN}* {state = NAME;
	if(name == NULL)
		name=(char*) malloc (strlen(yytext));
	else
		name=(char*) realloc ((void*) name,strlen(yytext));
	strcpy(name,yytext);
	return(NAME);
	}
[\t \{] ;
,	return *yytext;

\n	line++;

[\[ \] * ;]  return *yytext;
:q
. {printf("unknown token : %s\n",yytext); return (UNKNOWN);}
%%
