class tdtServer {
    public static void main(String[] args) {

        int[][] values = new int[6][2];
        double[][][][] dvalues = new double[10][10][10][10];
        

        int tc;
        int ts;
        jTDT tdt = new jTDT();
        tc = tdt.config("serv.xml");
        ts = tdt.open(tc, "clnt_to_serv");

        tdt.read (ts, values, "dataarray");
        tdt.read (ts, dvalues, "dataarray_d");

        System.out.println("java values: " + values[1][0]);
        
        System.out.println("java dvalues: " + dvalues[7][2][4][9]);

        tdt.close(ts);
        tdt.end(tc);
    }
}
