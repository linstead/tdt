#include <stdio.h>
#include <jni.h>
#include <stdlib.h>
#include "jTDT.h"
#include "tdt.h"
#include "tdt_inner.h"
#include "typeops.h"
#include "primops.h"
#include "addrops.h"
#include "xmlhandlers.h"
#include "tdt_map.h"

void (*jreadfuns[]) (void *,void *, void *, void *, void *);
void (*jwritefuns[]) (void *,void *, void *, void *, void *);


void *GetArrayElements (JNIEnv *env, jobject obj, void *iscopy, TypeName tn){
	switch (tn) {
		case TDT_CHAR:
			return ((void*)(*env)->GetCharArrayElements (env, obj, iscopy));
		case TDT_INT:
			return ((void*)(*env)->GetIntArrayElements (env, obj, iscopy));
		case TDT_FLOAT:
			return ((void*)(*env)->GetFloatArrayElements (env, obj, iscopy));
		case TDT_DOUBLE:
			return ((void*)(*env)->GetDoubleArrayElements (env, obj, iscopy));
		default:
			return (NULL);
	}
};

void *ReleaseArrayElements (JNIEnv *env, jobject obj, void *buff, int mode, TypeName tn){
        switch (tn) {
                case TDT_CHAR:
                        (*env)->ReleaseCharArrayElements (env, obj, buff, mode);
                case TDT_INT:
                        (*env)->ReleaseIntArrayElements (env, obj, buff, mode);
                case TDT_FLOAT:
                        (*env)->ReleaseFloatArrayElements (env, obj, buff, mode);
                case TDT_DOUBLE:
                        (*env)->ReleaseDoubleArrayElements (env, obj, buff, mode);
        }
};
          
void SetArrayRegion (JNIEnv *env, jobject obj, jsize start, jsize len ,void *buff, TypeName tn) {
	switch (tn) {
		case TDT_CHAR:
			(*env)->SetCharArrayRegion (env, obj, start, len, buff);
		case TDT_INT:
			(*env)->SetIntArrayRegion (env, obj, start, len, buff);
		case TDT_FLOAT:
			(*env)->SetFloatArrayRegion (env, obj, start, len, buff);
		case TDT_DOUBLE:
			(*env)->SetDoubleArrayRegion (env, obj, start, len, buff);
	}
};


JNIEXPORT jint JNICALL 
Java_jTDT_config
    (JNIEnv *env, jobject obj, jstring mystring) {

    jint tdtconf;
    TDTConfig tc;

    const char *str = (*env)->GetStringUTFChars(env, mystring, 0);

    tc = tdt_configure((char *) str);

    tdtconf = (int) tc;

    (*env)->ReleaseStringUTFChars(env, mystring, str);

    return tdtconf;
}

JNIEXPORT jint JNICALL 
Java_jTDT_open
    (JNIEnv *env, jobject obj, jint tdtconf, jstring chname) {

    jint tdtstate;
    TDTState ts;
    TDTConfig tc;

    const char *str = (*env)->GetStringUTFChars(env, chname, 0);

	tc = (TDTConfig) tdtconf;

    ts = tdt_open (tc, (char *) str);

    tdtstate = (int) ts;

    (*env)->ReleaseStringUTFChars(env, chname, str);

    return tdtstate;
}

/*
    Reading and writing integer arrays
*/


JNIEXPORT void JNICALL Java_jTDT_read
(JNIEnv *env, jobject obj, jint tdtstate, jobject values, jstring name) {

	TDTState ts = (TDTState)tdtstate;
	TypeDesc    td;
	TDTAddr     ta;
	DataDesc    md;
	TypeName tn;
	const char *str = (*env)->GetStringUTFChars(env, name, 0); 
//	md = get_datadesc (ts);

	md = ts->md;
	    
	if (md == NULL) {
        printf ("get_datadesc returned NULL in jtdt_read");
        exit (1);
    }
	
        td = find_type_in_datadesc (md, (char*) str);

	tn = type_name(td);
    if (td == NULL) {
        tdt_error ("find_type_in_datadesc returned NULL in jtdt_read");
        //exit (1);
    }
	if (is_primitive(tn)) {
		tdt_error("type not implemented");
		exit(1);
	}

//	ta = get_tdtaddr (ts);
	ta = ts->ta;
    if (ta == NULL) {
       tdt_error ("get_tdtaddr returned NULL in jtdt_read");
        exit (1);
    }

	jreadfuns[tn] (env, values, td->value, ta, jreadfuns);
	(*env)->ReleaseStringUTFChars(env, name, str);
}

//write object
JNIEXPORT void JNICALL Java_jTDT_write
(JNIEnv *env, jobject obj, jint tdtstate, jobject values, jstring name) {

	TDTState ts = (TDTState)tdtstate;
	TypeDesc    td;
	TDTAddr     ta;
	DataDesc    md;
	TypeName tn;
	const char *str = (*env)->GetStringUTFChars(env, name, 0); 
//	md = get_datadesc (ts);

	md = ts->md;
	    
	if (md == NULL) {
        printf ("get_datadesc returned NULL in jtdt_read");
        exit (1);
    }
	
        td = find_type_in_datadesc (md, (char*) str);

	tn = type_name(td);

    if (td == NULL) {
        tdt_error ("find_type_in_datadesc returned NULL in jtdt_read");
        //exit (1);
    }
	if (is_primitive(tn)) {
		tdt_error("type not implemented");
		exit(1);
	}

//	ta = get_tdtaddr (ts);
	ta = ts->ta;
    if (ta == NULL) {
       tdt_error ("get_tdtaddr returned NULL in jtdt_read");
        exit (1);
    }

	jwritefuns[tn] (env, values, td->value, ta, jwritefuns);


}



JNIEXPORT void JNICALL Java_jTDT_close
    (JNIEnv *env, jobject obj, jint tdtstate) {

    tdt_close ( (TDTState) tdtstate);
}

JNIEXPORT void JNICALL Java_jTDT_end
    (JNIEnv *env, jobject obj, jint tdtconfig) {

    tdt_end ( (TDTConfig) tdtconfig);
}


void
map_jaddr (void *env,void *val, void *type_val, void *args, void *functions) {

	tdt_error("no addr type in java version");
	exit(1);
}

void
map_jstruct (void *env, void *val, void *type_val, void *args, void *functions) {

	tdt_error("no struct type in java version");
	exit(1);
}


void
map_jarray_read (void *env1,void *val1, void *type_val, void *args, void *functions) {

        JNIEnv      *env = env1;
        jobject     val = val1;
	ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td   = array_type (ar);
	TypeName    tn   = type_name (td);
	TypeVal     tv   = type_value (td);
	int         size =  array_sz (ar);
	int         i;
	int         sz  = type_size (td);
	void        (**funarray) (void *,void *, void *, void *, void *);
	void		*buff;
	jobject		newval;
	
        funarray = (void (**)(void *,void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_jarray");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_jarray.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_jarray.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_jarray.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_jarray");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_jarray");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_jarray.");
		exit (1);
	}
	/* End of error handling section */



	if (is_primitive (tn)) {
		
		buff = GetArrayElements (env, val,0, tn);
		read_block (buff, size*type_size(td), args);
//                printf("ch = %s\n",((char *)buff));
		SetArrayRegion(env, val, 0, size, buff, tn);
		
	}
	else
	for (i = 0; i < size; i++) {
		newval =  (*env)->GetObjectArrayElement(env,val,i);
		funarray[tn] (env, newval, tv, args, jreadfuns);
	}
}

void
map_jarray_write (void *env1,void *val1, void *type_val, void *args, void *functions) {

        JNIEnv      *env = env1;
        jobject     val = val1;
        ArrayDesc   ar  = (ArrayDesc) type_val;
	TypeDesc    td  = array_type (ar);
	TypeName    tn  = type_name (td);
	TypeVal     tv  = type_value (td);
	int         size = array_sz (ar);
	int         i;
	int         sz = type_size (td);
	char		*elem = (char *) val;
	void        (**funarray) (void *, void *, void *, void *, void *);
	void		*buff;
	jobject		newval;

	funarray = (void (**)(void *,void *, void *, void *, void *)) functions;

	/* Start of error handling section */
	if (val == NULL) {
		tdt_error ("NULL val passed to map_jarray");
		exit (1);
	}
	if (ar == NULL) {
		tdt_error ("NULL ad in map_jarray.");
		exit (1);
	}
	if (td == NULL) {
		tdt_error ("addr_type in map_jarray.");
		exit (1);
	}
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in map_jarray.");
		exit (1);
	}
	if (size == -1) {
		tdt_error ("array_sz in map_jarray");
		exit (1);
	}
	if (sz == -1) {
		tdt_error ("type_size in map_jarray");
		exit (1);
	}
	if (funarray == NULL) {
		tdt_error ("NULL funarray in map_jarray.");
		exit (1);
	}
	/* End of error handling section */



	if (is_primitive (tn)) {
		buff = GetArrayElements (env, val,0, tn);
//                printf("ch = %c\n",((char *)buff)[0]);
		write_block (buff, size*type_size(td), args);
//		ReleaseArrayElements (env, val, buff, JNI_ABORT, tn);
	}
	else
	for (i = 0; i < size; i++) {
		newval =  (*env)->GetObjectArrayElement(env,val,i);
		funarray[tn] (env, newval, tv, args, jwritefuns);
	}
}



void
read_jchar (void *env, void *val, void *type_val, void *args, void *functions) {

	read_char(val,type_val,args,functions);
}

void
read_jint (void *env, void *val, void *type_val, void *args, void *functions) {

//	jint i = (*env)->GetStaticIntField(env, cls, fid_input);;
	printf("jint=%d", (int) val);
//	read_int(val,type_val,args,functions);
}

void
read_jfloat (void *env, void *val, void *type_val, void *args, void *functions) {

	read_float(val,type_val,args,functions);
}

void
read_jdouble (void *env, void *val, void *type_val, void *args, void *functions) {

	read_double(val,type_val,args,functions);
}


void
write_jchar (void *env, void *val, void *type_val, void *args, void *functions) {

	write_char(val,type_val,args,functions);
}

void
write_jint (void *env, void *val, void *type_val, void *args, void *functions) {

	write_int(val,type_val,args,functions);
}

void
write_jfloat (void *env, void *val, void *type_val, void *args, void *functions) {

	write_float(val,type_val,args,functions);
}

void
write_jdouble (void *env,void *val, void *type_val, void *args, void *functions) {

	write_double(val,type_val,args,functions);
}
void
(*jreadfuns[]) (void *,void *, void *, void *, void *) = {
	read_jchar,
	read_jint,
	read_jfloat, 
    read_jdouble, 
    map_jaddr, 
    map_jarray_read, 
    map_jstruct
};

void
(*jwritefuns[]) (void *, void *, void *, void *, void *) = {
	write_jchar,
	write_jint,
	write_jfloat, 
    write_jdouble, 
    map_jaddr, 
    map_jarray_write, 
    map_jstruct
};
