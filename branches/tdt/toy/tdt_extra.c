/* Communicating with the controller */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "tdt.h"

TDTState
tdt_open_channel (TDTState ts, char *ctrlhost, int ctrlport, Mode mode) {
	TDTState ts_c; /* for communicating with controller */

	int len; /* channel hostname length */
	char *hostname; /* name of channel host */
	int port; /* port of channel host */

	ts_c = tdt_init ("addr.xml");
	ts_c = tdt_open_socket (ts_c, ctrlhost, ctrlport, READ);

	tdt_read (ts_c, &len, "len");
	if (len == 0) {
		tdt_error ("Stop signal from controller: NULL host received");
		tdt_end (ts);
		return NULL;
	}

	hostname = (char *) malloc ((len+1)*sizeof (char));
	hostname[len] = '\0';
	tdt_size_array (ts_c, "hostname", len);
	tdt_read (ts_c, hostname, "hostname");
	tdt_read (ts_c, &port, "port");
	
	tdt_end (ts_c);
	
	ts = tdt_open_socket (ts, hostname, port, mode);

	return ts;
}

void
tdt_send_addr (char *hostname, int port,
		char *msg_hostname, int msg_port) {
	
	int len;
	TDTState ts;

	ts = tdt_init ("addr.xml");
	ts = tdt_open_socket (ts, hostname, port, WRITE);
	len = strlen (msg_hostname);
	tdt_size_array (ts, "hostname", len);
	tdt_write (ts, &len, "len");
	tdt_write (ts, msg_hostname, "hostname");
	tdt_write (ts, &msg_port, "port");
	tdt_end (ts);
}
	
