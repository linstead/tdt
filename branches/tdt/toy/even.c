/* Part of the Collatz conjecture example.
 * Takes the number stored in the file ineven and divides it by two.
 * The result is stored in file n.
 * The program will crash if:
 *      the number in ineven is not even
 *      the required files not accessible
 */

#include <stdio.h>

int
main (int argc, char** argv) {
  FILE *in;
  FILE *out;
  int n;

  in = fopen ("ineven", "r");
  if (in == NULL) {
    fprintf (stderr, "Could not open input file\n");
    return 1;
  }

  fscanf (in, "%d", &n);
  fclose (in);

  if (n % 2 != 0) {
    fprintf (stderr, "%d is not an even number!\n", n);
    return 1;
  }

  n = n/2;

  out = fopen ("n", "w");
  if (out == NULL) {
    fprintf (stderr, "Could not open output file\n");
    return 1;
  }

  fprintf (out, "%d", n);
  fclose (out);

  return 0;

}

  
