#!/bin/bash
# Part of the Collatz conjecture example.
# Runs the eval, even or odd loop until
# convergence.

STEPS=0
./eval
RES=`cat res`

while [ $RES != "0" ]
do
  let STEPS=$STEPS+1
  if [ $RES == "1" ]
  then
      echo "N =" `cat n` "running odd"
      ./odd
  else
      echo "N =" `cat n` "running even"
      ./even
  fi

  ./eval
  RES=`cat res`
done

echo "Finished in $STEPS steps"
