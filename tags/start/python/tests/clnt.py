from tdt import TDT

tdt = TDT()

tdt.config ("clntconf.xml")
tdt.open ("output1")

d = 123.456
s = { 'firstthing':[42, 49], 'secondthing':987.654 }
i = 12

tdt.size_array ("dyn", i)

dyn = []
for j in range(i):
	dyn.append(10.33*j)

tdt.write (d, 'new_double')
tdt.write (s, 'astruct')
tdt.write (i, 'new_int')
tdt.write (dyn, "dyn")

#tdt_close ()
tdt.end ()
