from tdt import TDT

tdt = TDT ()

tdt.config ("servconf.xml")
tdt.open ("input1")

d = tdt.read ("new_double")
print 'd = ', str (d)

astruct = tdt.read ("astruct")
print 'astruct = ', repr (astruct)

i = tdt.read ("new_int")
print 'i = ', str (i)

tdt.size_array ("dyn", i)

dyn = tdt.read ("dyn")

print dyn

#tdt.close ()
tdt.end ()
