
		SUBROUTINE tdt_finit (tdtstate, xmlname)
		INTEGER tdtstate
		CHARACTER *(*) xmlname

		CALL tdt_finit_internal (tdtstate, xmlname, 
     *							LEN (xmlname))

		END

		SUBROUTINE tdt_fopen_socket (tdtstate, ip, port, mode)
		INTEGER tdtstate
		INTEGER port
		INTEGER mode
		CHARACTER *(*) ip

		CALL tdt_fopen_socket_internal (tdtstate, (ip), 
     *				LEN (ip), port, mode)
		
		END

		SUBROUTINE tdt_fread (tdtstate, val, name) 
		INTEGER tdtstate
		INTEGER val
		CHARACTER *(*) name

		CALL tdt_fread_internal (tdtstate, val, 
     *						name, LEN (name))

		END

		SUBROUTINE tdt_fwrite (tdtstate, val, name) 
		INTEGER tdtstate
		INTEGER val
		CHARACTER *(*) name

		CALL tdt_fwrite_internal (tdtstate, val, 
     *					name, LEN (name))

		END
