		PROGRAM fclnt
        	character*20 a0
                character*20 a1
		integer b0,b1
		integer k,n
                integer ts(20)

		b0=104
		a0="12345678901234567890"
		CALL tdt_finit_mpi (ts, 'example.xml')
                
                call tdt_fget_np(n)
                if (n.ne.2) then
                        print *,'you should use 2 processors'
                        call tdt_fend(ts)
                        call exit()
                endif
		CALL tdt_fget_rank(k)

		if (k.eq.0)  then
                        call tdt_fread (ts(2), 'int', b1) 
                        CALL tdt_fread (ts(2), 'str', a1)
                        print *,b1
                        print *,a1
                else
                        
        		CALL tdt_fwrite (ts(1), 'int', b0)
        		CALL tdt_fwrite (ts(1), 'str', a0)
                endif

		CALL tdt_fend_mpi (ts)

		END
