		PROGRAM fclnt
		INTEGER:: firstthing(2,5)
		REAL*8:: secondthing(100,15,100,100)
		INTEGER tdtstate1
		INTEGER tdtstate2
		INTEGER tdtconf

		DO k = 1,2
			DO l = 1,5
			firstthing(k,l) = k*10 + l
			PRINT *, firstthing(k, l)
			END DO
		END DO

        CALL tdt_fconfigure (tdtconf, 'clntconf.xml')

		CALL tdt_fopen (tdtstate1, tdtconf, 'conn1')
		CALL tdt_fwrite (tdtstate1, 'a', firstthing)

		CALL tdt_fopen (tdtstate2, tdtconf, 'conn2')
		CALL tdt_fread (tdtstate2, 'b', secondthing)

		PRINT *, 'I read:'
		PRINT *, secondthing(100,15,100,100)

		CALL tdt_fclose (tdtstate1)
		CALL tdt_fclose (tdtstate2)
		CALL tdt_fend (tdtconf)


		END
