		PROGRAM fclnt
        character*10 astring		
        INTEGER tdtstate1
        INTEGER tdtconf
        INTEGER b

        CALL tdt_fconfigure (tdtconf, 'servconf.xml')
		CALL tdt_fopen (tdtstate1, tdtconf, 'conn1')

		CALL tdt_fread (tdtstate1, 'astring', astring)
		CALL tdt_fread (tdtstate1, 'b', b)

        PRINT *,astring
        PRINT *,b

		CALL tdt_fend (tdtstate1)
        CALL tdt_fcleanup (tdtconf)

		END
