/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32

#include <winsock.h>

#else

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#endif
#include "tdt.h"

/* Returns NULL on error */
TDTState
intptots (int *pts) {
	TDTState ts;

    if (pts == NULL) {
        tdt_error ("pts is NULL in intptots");
        return NULL;
    }
    
	ts = (TDTState) (*pts);

	return ts;
}

/* Exits with status 1 on error */
void
tstointp (TDTState ts, int *pts) {

    if (ts == NULL) {
        tdt_error ("TDTState is NULL in tstointp");
        exit (1);
    }

	*pts = (int) ts;

}

/* Returns NULL on error */
char *
fstrtocstr (char *fstr, int len) {
	char *cstr;
	int i;

    if (fstr == NULL) {
        tdt_error ("fstr is NULL in fstrtocstr");
        return NULL;
    }
    
	cstr = (char *) malloc ((len + 1) * sizeof(char));

    if (cstr == NULL) {
        tdt_error ("Cannot allocate memory for cstr in fstrtocstr");
        return NULL;
    }
    
	for (i = 0; i < len; i++)
		cstr[i] = fstr[i];

	cstr[len] = '\0';

	return cstr;
}

/* Exits with status 1 on error */
void
tdt_finit_internal__ (int *pts, char *xmlfname, int *flen) {

	TDTState ts;
    
    if (flen == NULL) {
        tdt_error ("flen is NULL in tdt_finit_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "xmlfname"
     * so no need to check here.
     */
	ts = tdt_init (fstrtocstr(xmlfname, *flen));

    if (ts == NULL) {
        tdt_error ("tdt_init returned NULL in tdt_finit_internal");
        exit (1);
    }
    
	tstointp (ts, pts);
}

/* exits with status 1 on error */
void
tdt_fopen_socket_internal__ (int *pts, char *hname, int *hnamelen, int *port, int
					*mode) {
	TDTState ts;

    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fopen_socket");
        exit (1);
    }

    if (hnamelen == NULL) {
        tdt_error ("hnamelen is NULL in tdt_fopen_socket");
        exit (1);
    }

    if (port == NULL) {
        tdt_error ("port is NULL in tdt_fopen_socket");
        exit (1);
    }

    if (mode == NULL) {
        tdt_error ("mode is NULL in tdt_fopen_socket");
        exit (1);
    }

    /* call to fstrtocstr will also check "hname"
     * so no need to check here.
     */
	ts = tdt_open_socket (intptots(pts), fstrtocstr(hname, *hnamelen), 
						  *port, *mode);
						
    if (ts == NULL) {
        tdt_error ("tdt_open_socket returned NULL ts in tdt_fopen_socket");
        exit (1);
    }

	tstointp(ts, pts);
}

/* exits with status 1 on error */
void
tdt_fwrite_internal__ (int *pts, void *val, char *name, int *namelen) {
	
    TDTState ts;
    
    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fwrite_internal");
        exit (1);
    }
   
    if (namelen == NULL) {
        tdt_error ("namelen is NULL in tdt_fwrite_internal");
        exit (1);
    }
    
    ts = intptots (pts);
    
    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fwrite_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "name"
     * so no need to check here.
     */
	tdt_write (intptots(pts), val, fstrtocstr(name, *namelen));

}

void
tdt_fread_internal__ (int *pts, void *val, char *name, int *namelen) {

    TDTState ts;
    
    if (namelen == NULL) {
        tdt_error ("namelen is NULL in tdt_fread_internal");
        exit (1);
    }
    
    /* call to intptots will check pts for NULL
     */
    ts = intptots (pts);
    
    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fread_internal");
        exit (1);
    }
    
    /* call to fstrtocstr will also check "name"
     * so no need to check here.
     */
	tdt_read(ts, val, fstrtocstr(name, *namelen));

}

/* Exits with status 1 on error */
void
tdt_fend__ (int *pts) {

    TDTState ts;
	
    if (pts == NULL) {
        tdt_error ("pts is NULL in tdt_fend");
        exit (1);
    }
    
    ts = intptots(pts);

    if (ts == NULL) {
        tdt_error ("intptots returned NULL in tdt_fend");
        exit (1);
    }
    
	tdt_end (ts);

}
