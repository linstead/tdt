#ifndef __XML2CODE_H__
#define __XML2CODE_H__

#include <stdio.h>
typedef enum {FORTRAN_CODE, C_CODE, NO_CODE} code_type;

typedef struct {
	FILE *fw;
	FILE *fr;
	code_type ct;
	int port;
	char *ts_name;
	char *hostname;
	char *xmlfile;
} coding_data;

void 
start(void *data, const char *el, const char **attr);

void
end(void *data, const char *el);

void
chardata(void *data, const char *el, int len);

#endif
