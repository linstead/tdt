%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "typeops.h"
#include "xmlhandlers.h"
#include "defs.h"
#include "model2xml.h"
DataDesc   md;

typedef struct decls1 {
	Decl* decls;
	int nd;
}* Decls;

typedef struct comp1 {
	TypeDesc td;
	char *name;
}* Comp;

typedef struct multicomp1 {
	Comp *comps;
	int ncomps;
}* Multicomp;

extern char num;
extern int level;
extern char *name;
extern int line;
int array_size;
int tabs=0;
char *aux;

char *cat(), *ds();
TypeDesc join(TypeDesc, TypeDesc);
%}

%token STRUCT CHAR INT NAME DOUBLE FLOAT UNKNOWN NUMBER

%union {
	TypeDesc typedesc;
	ArrayDesc ar;
	AddrDesc ad;
	StructDesc structdesc;
	Decl decl;
	Decls decls;
	DataDesc md;
	Multicomp multicomp;
	Comp comp;
	int num;
}

%type <md> model
%type <decls> decls, multidecl
%type <typedesc> type, point, brack, 
%type <multicomp> multicomp
%type <decl> decl
%type <num> NUMBER
%type <comp> comp, comp1
%type <structdesc> struct



%%
model: decls  {
     	
     	int i;
     	/*printf("correct list of declarations\n");*/
	md = (DataDesc) malloc (sizeof (struct data_desc));
	if (md == NULL) {
		printf("Memory allocation failed for md in model\n");
		exit(1);
	}
	md->ad = (Decl *) malloc ($1->nd * sizeof (Decl));
	if (md->ad == NULL) {
		printf("Memory allocation failed for md->ad in model\n");
		exit(1);
	}
	md->nd = $1->nd;
	for (i=0;i<md->nd;i++) {
		md->ad[i] = $1->decls[i];
	}
}
	;

decls: decl {

	/*printf("decl\n");*/
	$$ = (Decls ) malloc (sizeof (Decls));
	$$->nd = 1;
	$$->decls = (Decl *) malloc ( sizeof(void*) );
	$$->decls[0] = $1;
	/*printf("end decl\n");*/
}

	| multidecl {
     
	$$ = $1;
}
	| decl decls {
	
	int i;

	/*printf("decl decls\n");*/
	$$ = (Decls ) malloc (sizeof (Decls));
	$$->nd = 1 + $2->nd;
	$$->decls = (Decl *) malloc ( (1 + $2->nd) * sizeof(void*));
	$$->decls[0] = $1;
	for(i=0; i<$2->nd; i++)
		$$->decls[i+1] = $2->decls[i];
	free($2->decls);
	free($2);
	/*printf("end decl decls\n");*/
	
}
	| multidecl decls {

	int i;
	/*printf("multidecl decls\n");*/
	
	$$ = (Decls ) malloc (sizeof (Decls));
	$$->nd = $1->nd + $2->nd;
	$$->decls = (Decl *) malloc ( ($1->nd + $2->nd) * sizeof(void*));
	for(i=0; i<$1->nd; i++)
		$$->decls[i] = $1->decls[i];
	for(i=0; i<$2->nd; i++)
		$$->decls[i + $1->nd] = $2->decls[i];

	free($1);
	free($2);
	/*printf("end multidecl decls\n");*/
	
};

multidecl: type multicomp ';'	{
	
	int i;
	Decl decl;
	$$ = (Decls ) malloc (sizeof (Decls));
	$$->nd = $2->ncomps;
	$$->decls = (Decl *) malloc ($2->ncomps);

	for (i=0; i<$2->ncomps; i++) {
		$$->decls[i] = (Decl) malloc (sizeof(Decl));
		decl = $$->decls[i];
		decl->name = $2->comps[i]->name;
		decl->type = $2->comps[i]->td;
		decl->type = join (decl->type, $1);
	}	
}
	;

decl: type comp ';' {
	
	/*printf("type comp\n");*/
	$$ = (Decl) malloc (sizeof (Decl));
	$$->name = $2->name;
	$$->type = $2->td;
	$$->type = join ($$->type, $1);
	/*printf("type comp end\n");*/
	
};
    
type: struct {
	
	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_STRUCT;
	$$->value = $1;
}
	| CHAR {

	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_CHAR;
	$$->value = NULL;
}
	| INT {

	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_INT;
	$$->value = NULL;
}
	| FLOAT {

	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_FLOAT;
	$$->value = NULL;
}
	| DOUBLE{

	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_DOUBLE;
	$$->value = NULL;
}
	;

struct: STRUCT '{' decls '}' {

	/*printf("struct\n");*/
	$$ = (StructDesc) malloc (sizeof (StructDesc));
	$$->num_decls = $3->nd;
	$$->decls = $3->decls;
	free($3);
}
        ;

multicomp: comp ',' comp {

	$$ = (Multicomp) malloc (sizeof (Multicomp));
	$$->comps = (Comp*) malloc (2  *  sizeof(void*));
	$$->comps[0] = $1;
	$$->comps[1] = $3;
	$$->ncomps = 2;
	 
}
	| comp ',' multicomp {

	int i;
	$$ = (Multicomp) malloc (sizeof (Multicomp));
	$$->comps = (Comp*) malloc( 1 + $3->ncomps);
	$$->comps[0] = $1;
	for(i=0; i<$3->ncomps; i++)
		$$->comps[1+i] = $3->comps[i];
	free($3->comps);
	free($3);
}
	
	;
	 
comp: comp1 {
    
	$$ = $1;
}
	| point  comp {

	TypeDesc tmp;
	ArrayDesc ar;
	AddrDesc ad;
	
	/*printf("point comp\n");*/
	tmp = $2->td;
	$$ = $2;
	$$->td = join (tmp, $1);
} 
	;

point: '*'  {
	
	ArrayDesc ar;
	AddrDesc ad;
	TypeDesc td;

	/*printf("*\n");*/
	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_ADDR;
	ad = (AddrDesc) malloc (sizeof (AddrDesc));
	$$->value = ad;

	td = (TypeDesc) malloc (sizeof (TypeDesc));
	ad->type = td;
	ar = (ArrayDesc) malloc (sizeof (ArrayDesc));
	td->name = TDT_ARRAY;
	td->value = ar;
	ar->size = 0;
	ar->type = NULL;
	
}
	| '*' brack {

	AddrDesc ad;
	
	/*printf("* brack\n");*/
	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_ADDR;
	ad = (AddrDesc) malloc (sizeof (AddrDesc));

	$$->value = ad;
	ad->type = $2;
}
	;

brack: '[' NUMBER ']' {

	ArrayDesc ar;

	/*printf("brack\n");*/
	$$ = (TypeDesc) malloc (sizeof (TypeDesc));
	$$->name = TDT_ARRAY;
	ar = (ArrayDesc) malloc (sizeof (ArrayDesc));
	$$->value = ar;
	ar->size = atoi (name);
	/*printf("NUMBER = %d\n", ar->size);*/
	ar->type = NULL;
}
;

comp1: NAME {
	
	/*printf("NAME = %s\n",name);*/
	$$ = (Comp) malloc (sizeof(Comp));
	$$->name = (char*) strdup(name);
	$$->td = NULL;
}
	| comp1 brack {
	
	/*printf("comp1 brack\n");*/
	$$ = $1;
	if ($$->td == NULL)
		$$->td = $2;
	else
		$$->td = join ($$->td, $2);
}
	;

%%

TypeDesc join(TypeDesc td1, TypeDesc td2)
{
	TypeDesc tmp;
	ArrayDesc ar;
	AddrDesc ad;

	if (td2 == NULL) return (td1);
	if (td1 == NULL) return (td2);
	tmp = td1;
	while (1) {
		if (tmp->name != TDT_ARRAY && tmp->name != TDT_ADDR){
			printf("Could not join\n");
			return (NULL);
		}
		if (tmp->name == TDT_ARRAY) {
			ar = (ArrayDesc) tmp->value;
			if (ar->type == NULL) {
				ar->type = td2;
				return (td1);
			}
			else 
				tmp = ar->type;
		}
		else if (tmp->name == TDT_ADDR) {
			ad = (AddrDesc) tmp->value;
			if (ad->type == NULL) {
				ad->type = td2;
				return (td1);
			}
			else
				tmp = ad->type;
		}
	}
}





extern FILE *yyin;
main(int argc,char  **argv)
{
	int i;
	char *nume;
	
	if (argc > 1)
		yyin = fopen( argv[1], "r" );
	else {
		printf ("Usage: c2xml file\nThe output with the xml description is : file.xml\n");
		exit(0);
	}
	
	do {
		yyparse();
	}
	while(!feof(yyin));
	nume = (char*) malloc (strlen(argv[1]) + 5);
	strcpy (nume, argv[1]);
	strcat (nume, ".xml"); 
	
	model2xml (md, nume);

	free(nume);
}

yyerror(char *s)
{
	printf("%s at line %d\n",s , line);
	exit(0);
}
