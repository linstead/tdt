#include <stdio.h>
#include <expat.h>

typedef char* String;

typedef struct channel {
    String name;
    String host;
    int    port;
    String type;
    String datadesc;
    String mode;
}* Channel;

typedef struct tdt_config {
    String progname;
    Channel *channels;
    int num_channels;
} *TDTConfig;

void
print_channel (Channel c) {
    printf ("name is %s\n", c->name);
    printf ("host is %s\n", c->host);
    printf ("port is %d\n", c->port);
    printf ("type is %s\n", c->type);
    printf ("datadesc is %s\n", c->datadesc);
    printf ("mode is %s\n", c->mode);
}

TDTConfig
init_tc() {

    TDTConfig tc;
            
    tc = (TDTConfig) malloc (sizeof (struct tdt_config));

    if (tc == NULL) {
        //tdt_error ("Cannot allocate memory for TDTConfig in tdt_configure()");
        fprintf (stderr, "Cannot allocate memory for TDTConfig in tdt_configure()\n");
         return NULL;
    }
    
    tc->num_channels = 0;
    
    return tc;  
}


void
start (void *data, const char *el, const char **attr) {
    fprintf(stderr, "In START\n");
}

void
end (void *data, const char *el) {
    fprintf(stderr, "In END\n");
}

void
chardata (void *data, const char *el, int len) {
    fprintf(stderr, "In CHARDATA\n");
}

TDTConfig
parse_conf (char *fn) {
    
    FILE *xfp;
    int done;
    int len;
    TDTConfig tc;
    int BUFFSIZE = 32000;
    char buff[BUFFSIZE];

    XML_Parser p = XML_ParserCreate (NULL);

    if (!p) {
        fprintf (stderr, "XML_ParserCreate failed\n");
        //tdt_error ("XML_ParserCreate failed in parse_config");
    }

    tc = init_tc ();
    
    if (tc == NULL) {
        fprintf (stderr, "init_tc in parse_config\n");
        //tdt_error ("init_tc in parse_config");
        return NULL;
    }
                
    if (fn == NULL) {
        fprintf (stderr, "NULL file name passed to parse_config\n");
        //tdt_error ("NULL file name passed to parse_config");
        return NULL;
    }
                    
    if ((xfp = fopen (fn, "r")) == NULL) {
        fprintf (stderr, "failed to open file in read_datadesc for file: %s\n", fn);
        //tdt_error ("failed to open file in read_datadesc for file:");
        //tdt_error (fn);
        return NULL;
    }

    XML_SetUserData(p, tc);

    /* start() and end() are XML start and end tag handlers 
     * chardata() is the character data handler */

    XML_SetElementHandler (p, start, end);
    XML_SetCharacterDataHandler (p, chardata);

    /* The XML Parsing loop */
    for (;;) {
        len = fread (buff, 1, BUFFSIZE, xfp);
        if (ferror (xfp)) {
           fprintf (stderr, "error in fread in read_datadesc\n");
           //tdt_error ("error in fread in read_datadesc");
           return NULL;
         }

         done = feof (xfp);

         if (!XML_Parse (p, buff, len, done)) {
             fprintf (stderr, "XML_Parse in read_datadesc\n");
             //tdt_error ("XML_Parse in read_datadesc");
             return NULL;
         }

         if (done) {
             fclose (xfp);
             return tc;
         }
    }
    
    return tc;
}

int
main (int argc, char **argv) {

    TDTConfig tc;
        
    tc = parse_conf ("servconf.xml");
//    print_channel(tc->channels[1]);

}
