#include <expat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "xml2code.h"
#include "edit_code.h"

#define BUFFSIZE 8192

int level;

void
xml_error (char *error_message) {
    fprintf (stderr, "%s\n", error_message);
}

char*
get_decl_name (const char **attr) {

	if (attr == NULL) {
		xml_error ("NULL parameter to get_decl_name");
		return NULL;
	}
	if (strcasecmp (attr[0], "name") == 0) 
		return (char*) attr[1];

	return NULL;

}

int
is_valid_type(char *string) {

	if (strcasecmp (string, "INT") == 0) {
		return 1;
	} else if (strcasecmp (string, "CHAR") == 0) {
	 	return 1;
	} else if (strcasecmp (string, "FLOAT") == 0) {
		return 1;
	} else if (strcasecmp (string, "DOUBLE") == 0) {
		return 1;
	}

	return 0;
}

int
xml2code (char *xmlfile, coding_data cd)
{

	char buff[BUFFSIZE];
	FILE *fxml;
	int len;
	int done;
	char *name_read;
	char *name_write;
	int n;
	XML_Parser  p = XML_ParserCreate (NULL);

	level=0;
	if (!p) {
		xml_error ("XML_ParserCreate error in xml2code");
		return 0;
	}

	if (xmlfile == NULL) {
		xml_error ("NULL file name passed to xml2code");
		return 0;
	}
	if ((fxml = fopen (xmlfile, "r")) == NULL) {
		xml_error("failed to open file in xml2code for file:");
		xml_error (xmlfile);
		return 0;
	}
	
	n = strlen (xmlfile);
	name_read = (char*) malloc (n+8);
	name_write = (char*) malloc (n+9);
	strcpy(name_read,(const char*)xmlfile);	
	strcpy(name_write,(const char*)xmlfile);
	if (cd.ct == FORTRAN_CODE) {
		strcpy(name_read+n,".read.f");
		strcpy(name_write+n,".write.f");
	}
	else if (cd.ct == C_CODE) {
		strcpy(name_read+n,".read.c");
		strcpy(name_write+n,".write.c");
	}
	cd.fr=fopen(name_read,"w");
	cd.fw=fopen(name_write,"w");
	
	if (cd.ct == C_CODE) {
	
		init_c_read_code(&cd);
		init_c_write_code(&cd);	
	}
	else if (cd.ct == FORTRAN_CODE) {

		init_fortran_read_code(&cd);
		init_fortran_write_code(&cd);
	}
	
	XML_SetUserData (p, &cd);
	XML_SetElementHandler (p, start, end);
	XML_SetCharacterDataHandler (p, chardata);

	for (;;) {
		len = fread (buff, 1, BUFFSIZE, fxml);
		
		if (ferror (fxml)) {
			xml_error ("error in fread in xml2code");
			return 0;
		}
		
		done = feof (fxml);
		
		if (!XML_Parse (p, buff, len, done)) {
			xml_error ("XML_Parse in xml2code");
			return 0;
		}

		if (done) {
			if (cd.ct == FORTRAN_CODE) {

				end_fortran_read_code(&cd);
				end_fortran_write_code(&cd);
			}

			else if (cd.ct == C_CODE) {
			
				end_c_read_code(&cd);
				end_c_write_code(&cd);
			}
			fclose (fxml);
			return 1;
		}
	}
}


void
start (void *data, const char *el, const char **attr) {

	coding_data *cd;
	char *decl_name;
	
	level++;
	cd = (coding_data*) data;
	
	if (0 == strcasecmp (el, "decl") ) {
		if (level==2) {
			decl_name=get_decl_name(attr);
			if(decl_name == NULL) {
				xml_error("Name of decl is NULL in start");
				exit(1);
			}
	
			if(cd->ct == C_CODE) {
				add_c_read_code(cd,decl_name);
				add_c_write_code(cd,decl_name);
			}
			else if(cd->ct == FORTRAN_CODE) {
				add_fortran_read_code(cd,decl_name);
				add_fortran_write_code(cd,decl_name);
			}
		}
	}
	else if (0 == strcasecmp (el, "data_desc")) {}
	else if (0 == strcasecmp (el, "typedef")) {}
	else if (0 == strcasecmp (el, "struct")) {}
	else if (0 == strcasecmp (el, "array")) {}
	else if (0 == strcasecmp (el, "addr")) {}
	
	else {
		xml_error ("unknown tag in start");
		exit(1);
	}

}

void
end(void *data, const char *el) {
	level--;
}


void
chardata(void *data, const char *el, int len){

	int i,j;
	char *temp;
	
	temp = (char*) malloc(len);

	j=0;
	for (i=0;i<len;i++) {
		if (isalnum (el[i])) {
			temp[j]=el[i];
			j++;
		}
	}

	if(j==0) {
		free(temp);
		return;
	}
	
	temp[j]=0;
	
	if(!is_valid_type(temp)) {
		xml_error("error : is_valid_type in chardata");
		exit(1);
	}
	
}




void
print_help() {
	printf("\nusage : xml2code {-f,-c} [-p port] [-ts tdt_state_name] -h [hostname]  xmlfile\n\n\
where -f will produce a Fortran code and -c a C one\n\
      -p port is the listening (connecting) port \n\
      -ts tdt_state_name is the name of the variabile TDTState\n\
      -h hostname is the name of the host\n\
      'xmlfile' is the file containing the xml description\n\n\
The default values are p=2255, ts=\"ts\", h=\"localhost\".\n\n\
The program will produce 2 files named 'xmlfile.read.f' and 'xmlfile.write.f'\
in the case of Fortran code and xmlfile.read.c' and 'xmlfile.write.c' in the case of C code.\n\n");
}

int
main(int argc, char **argv) {
	
	coding_data cd;
	int i;
	char *xml_filename = NULL;
	
	cd.ct= NO_CODE;
	cd.ts_name=NULL;
	cd.port=2255;
	cd.hostname=NULL;

	if (argc<=1 || strcmp(argv[1],"--help")==0) {
		print_help();
		exit(1);
	}
	
	for(i=1;i<argc;i++) {

	
		if (strcmp(argv[i],"-f") == 0)
			cd.ct = FORTRAN_CODE;

		else if(strcmp(argv[i],"-c") == 0)
			cd.ct = C_CODE;

		else if(strcmp(argv[i],"-p") == 0) {
			i++;
			if(i>=argc) {
				xml_error ("error : no port specified");
				exit(1);
			}
			cd.port=atoi(argv[i]);
			if (cd.port <= 0 || cd.port >= 65536) {
				xml_error ("error : invalid port");
				exit(1);
			}
		}
		else if(strcmp(argv[i],"-ts") == 0) {
			i++;
			if(i>=argc) {
				xml_error ("error : no tdt state name specified");
				exit(1);
			}
			cd.ts_name=strdup(argv[i]);
		}

		else if(strcmp(argv[i],"-h") == 0) {
			i++;
			if(i>=argc) {
				xml_error ("error : no hostname name specified");
				exit(1);
			}
			cd.hostname=strdup(argv[i]);
		}
		
		else {
			if(i<argc-1) {
				xml_error ("error : bad command line");
				exit(1);
			}
			if(cd.ct==NO_CODE) {
				xml_error ("error : you didn't specify the language for the code");
				exit (1);
			}
			xml_filename=strdup(argv[i]);
		}
	}
		
	if(xml_filename == NULL) 
		xml_error ("error : you didn't provide a xml filename");
	
	else {
		if (cd.ts_name == NULL)
			cd.ts_name=strdup("ts");
		if (cd.hostname == NULL)
			cd.hostname=strdup("localhost");
		cd.xmlfile=strdup(argv[i-1]);
		return(xml2code(argv[i-1],cd));
	}
	
	return 1;

}

