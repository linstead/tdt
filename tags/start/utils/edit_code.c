#include "edit_code.h"
#include <stdio.h>

void
init_fortran_write_code(coding_data *cd)
{
	fprintf(cd->fw,"INTEGER %s\n\n",cd->ts_name);
	fprintf(cd->fw,"CALL tdt_finit (\'%s\', LEN (\'%s\'), %s)\n",\
		cd->xmlfile,cd->xmlfile,cd->ts_name);
	fprintf(cd->fw,"CALL tdt_fopen_socket (%s,\'%s\', LEN (\'%s\'), %d, 1)\n\n",cd ->ts_name,cd->hostname,cd->hostname,cd->port);
	
}

void
init_fortran_read_code(coding_data *cd)
{
	fprintf(cd->fr,"INTEGER %s\n\n",cd->ts_name);
	fprintf(cd->fr,"CALL tdt_finit (\'%s\', LEN (\'%s\'), %s)\n",\
		cd->xmlfile,cd->xmlfile,cd->ts_name);
	fprintf(cd->fr,"CALL tdt_fopen_socket (%s,\'%s\', LEN (\'%s\'), %d, 0)\n\n",cd ->ts_name,cd->hostname,cd->hostname,cd->port);
	
}

void
add_fortran_write_code(coding_data *cd, char *name)
{
	fprintf(cd->fw,"CALL tdt_fwrite (%s, %s, \'%s\')\n",\
	cd->ts_name, name, name);
}

void
add_fortran_read_code(coding_data *cd, char *name)
{
	fprintf(cd->fr,"CALL tdt_fread (%s, %s, \'%s\')\n",\
		cd->ts_name, name, name);
}

void
end_fortran_write_code(coding_data *cd)
{
	 fprintf(cd->fw,"\nCALL tdt_fend(%s)\n",cd->ts_name);
}
void
end_fortran_read_code(coding_data *cd)
{
	 fprintf(cd->fr,"\nCALL tdt_fend(%s)\n",cd->ts_name);
}


void
init_c_write_code(coding_data *cd)
{
        
	fprintf(cd->fw,"TDTState %s;\n",cd->ts_name);
	fprintf(cd->fw,"%s = tdt_init (\"%s\")\n",cd->ts_name,cd->xmlfile);
	fprintf(cd->fw,"%s = tdt_open_socket (%s, \"%s\", %d, WRITE);\n\n",cd->ts_name, cd->ts_name, cd->hostname, cd->port);

}

void
init_c_read_code(coding_data *cd)
{

	fprintf(cd->fr,"TDTState %s;\n",cd->ts_name);
	fprintf(cd->fr,"%s = tdt_init (\"%s\")\n",cd->ts_name,cd->xmlfile);
	fprintf(cd->fr,"%s = tdt_open_socket (%s, \"%s\", %d, READ);\n\n",cd->ts_name, cd->ts_name, cd->hostname, cd->port);

}

void
add_c_write_code(coding_data *cd, char *name)
{
	fprintf(cd->fw,"tdt_write(%s,&%s,\"%s\");\n",cd->ts_name,name,name);
}

void
add_c_read_code(coding_data *cd, char *name)
{
	fprintf(cd->fr,"tdt_read(%s,&%s,\"%s\");\n",cd->ts_name,name,name);

}

void
end_c_write_code(coding_data *cd)
{
	fprintf(cd->fw,"\ntdt_end(%s);\n",cd->ts_name);
}
void
end_c_read_code(coding_data *cd)
{
	fprintf(cd->fr,"\ntdt_end(%s);\n",cd->ts_name);
}

