typedef struct parse_state * parser; 

parser parse_init(char *name);
char* parse_get_next(parser p);
char* parse_get_last(parser p);
int parse_get_line(parser p);
void parse_stop(parser p);

