#include "decl.h"
#include "make_struct.h"
#include "make_rand_struct.h"

main(int argc, char **argv)
{
	char *str;
	int i;
	mydecl *d;
	model *mod;
	if(argc<=1)
		mod=add_rand_model("test.conf");
	else if(strcmp(argv[1],"--help")==0)
		printf("./test file : create files after model from file 'file'\n./test : create files randomly with parameters from 'test.conf'\n./test --help : help\n");
	else
		mod=add_model(argv[1]);
	if(mod!=0)
	{
		write_var_h(mod);
		write_var_c(mod);
		write_server_client_c(mod);
		write_td_h(mod);
		write_xml(mod);
	}
		
}
	
