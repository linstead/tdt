/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File typeops.h
 * Includes declarations for implementing operations on type
 * descriptions.
 * See the file NOTES in this directory for an idea of what these
 * declarations implement.
 */

/*dan*/
/* Windows changes 29-09-2002, Dan
 * Simplified typedef Boolean (didn't work for Windows)
 * Added TDT_ in front of the defined types
 * Added header tdt_allign
 */

#ifndef __TYPEOPS_H__
#define __TYPEOPS_H__


#ifdef _MPI
#include "mtdt.h"
#else
#include "tdt.h"
#endif

typedef void *TypeVal;

/*typedef char* String;*/

typedef enum  type_name {TDT_CHAR, TDT_INT, TDT_FLOAT, TDT_DOUBLE, 
						 TDT_ADDR, TDT_ARRAY, TDT_STRUCT, TDT_NOTYPE} TypeName;

typedef int Boolean; 
#define FALSE 0
#define TRUE 1


/* The value of a TypeDesc is as follows:
 * if name == INT (or CHAR or FLOAT): NULL
 * if name == ADDR: AddrDesc
 * if name == ARRAY: ArrayDesc
 * if name == STRUCT: StructDesc
 */
typedef struct typedesc {
	TypeName name;
	void* value;
}* TypeDesc;

typedef struct array_desc {
	int size;
	TypeDesc type;
}* ArrayDesc;

typedef struct addr_desc {
	TypeDesc type;
}* AddrDesc;

typedef struct decl_desc {
	String name;
	TypeDesc type;
}* Decl;

typedef struct struct_desc {
	int num_decls;
	Decl* decls;

	/* needed by the iterator functions */
	int _current;
}* StructDesc;

typedef struct typedef_desc {
	String typedefname;
	TypeDesc basetype;
}* TypedefDesc;

int indent_level;
void print_indent (int n);

/* Function Declarations */

void
tdt_error (char *err_msg);
    
/* Operations on type descriptions */
TypeName
type_name  (TypeDesc td);

void *
type_value (TypeDesc td);

void
set_typename (TypeDesc td, TypeName tn);

void
set_typevalue (TypeDesc td, void *val);

/* Operations on declarations */
/* Getting the name declared by the declaration */
String
decl_name (Decl d);

/* Getting the type in declaration */
TypeDesc
decl_type (Decl d);

/* Operations on addr */
/* Getting the addressed type */
TypeDesc
addr_type (AddrDesc a);

/* Operations on array */
/* Getting the size of the array */
int
array_sz (ArrayDesc a);

/* Getting the type of the elements of the array */
TypeDesc
array_type (ArrayDesc a);

/* Operations on struct */
/* Points _current to the first declaration in the struct */
void
reset_decls (StructDesc s);

/* Checking for the existence of more declarations */
Boolean 
has_more_decls (StructDesc s);

/* Getting the next declaration */
Decl 
next_decl (StructDesc s);

/* Adding a decl to a StructDesc */
void
add_decl(StructDesc sd, Decl decl);

/* Operations on typedefs */
/* Return the base type of the typedef */
TypeDesc
typedef_type(TypedefDesc tdd);

/* Return the name of the typedef */
String
typedef_name(TypedefDesc tdd);

/* Output routines */

void
print_typedesc (TypeDesc td);

void
print_decl (Decl d);

void 
print_typedefdesc (TypedefDesc tdd);

/* Other useful operations. */
Boolean
is_primitive (TypeName tn);

int
type_size (TypeDesc td);

TypeDesc
find_type_in_decls(Decl *decls, int num_decls, String name);

int
type_align (TypeDesc td);

TypeDesc
multi_array_type (ArrayDesc ar);

int
multi_array_size (ArrayDesc ar);

Boolean
is_struct_one_double ( TypeDesc td);

#endif
