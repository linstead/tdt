/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


/* <Ciaron, 29/10/2001> */

/* File typeops.c
 * Includes implementation of operations on type descriptions
 *
 * Ciaron 29/10/2001:
 * Added typedef type and it's operations
 *
 * 03/12/2001: Added XML Parsing function get_datadesc()
 *
 */


/* Windows changes 29-09-2002, Dan
 * Added TDT_ in front of the defined types
 * Added allign function for every type
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "typeops.h"
#include "xmlhandlers.h"

void
print_indent (int n) {

	int         i;

	for (i = 0; i < n; i++) {
		printf ("  ");
	}
}

/* Returns NOTYPE on error */
TypeName
type_name (TypeDesc td) {

	if (td == NULL) {
		tdt_error ("Null argument to type_name.");
		return TDT_NOTYPE;
	}
	return td->name;
}

/* Crashes on error -- exit (1) */
void       *
type_value (TypeDesc td) {

	if (td == NULL) {
		tdt_error ("Null argument to type_value.");
		exit (1); 
	}

	return td->value;
}

/* Crashes on error -- exit (1) */
void
set_typename (TypeDesc td, TypeName tn) {

	if (td == NULL) {
		tdt_error ("Null argument to set_typename.");
		exit (1); 
	}
	td->name = tn;
}

/* Crashes on error -- exit (1) */
void
set_typevalue (TypeDesc td, void *val) {

	if (td == NULL) {
		tdt_error ("Null argument to set_typevalue.");
		exit (1); 
	}
	td->value = val;
}

/* Returns NULL on error */
String
decl_name (Decl d) {

	if (d == NULL) {
		tdt_error ("Null argument to decl_name.");
		return NULL;
	}
	return d->name;
}

/* Returns NULL on error */
TypeDesc
decl_type (Decl d) {

	if (d == NULL) {
		tdt_error ("Null argument to decl_type.");
		return NULL;
	}

	return d->type;
}

/* Returns -1 on error */
int
array_sz (ArrayDesc ar) {

	if (ar == NULL) {
		tdt_error ("Null argument to array_sz.");
		return -1;
	}

	return ar->size;
}

/* Returns NULL on error */
TypeDesc
array_type (ArrayDesc ar) {

	if (ar == NULL) {
		tdt_error ("Null argument to array_type.");
		return NULL;
	}

	return ar->type;
}

/* Returns NULL on error */
TypeDesc
addr_type (AddrDesc ad) {

	if (ad == NULL) {
		tdt_error ("Null argument to addr_type.");
		return NULL;
	}
	return ad->type;
}

/* Crashes on error -- exit (1) */
void
reset_decls (StructDesc sd) {

	if (sd == NULL) {
		tdt_error ("Null argument to reset_decls.");
		exit (1); 
	}

	sd->_current = 0;
}

/* Crashes on error -- exit (1) */
Boolean
has_more_decls (StructDesc sd) {

	if (sd == NULL) {
		tdt_error ("Null argument to has_more_decls.");
		exit (1); 
	}

	if (sd->_current < sd->num_decls)
		return TRUE;
	return FALSE;
}

/* Returns NULL on error */
Decl
next_decl (StructDesc sd) {

	Decl        d;

	if (sd == NULL) {
		tdt_error ("Null argument to next_decls.");
		return NULL;
	}

	d = sd->decls[sd->_current];
	sd->_current++;
	return d;
}

/* Crashes on error -- exit (1) */
void
add_decl (StructDesc sd, Decl decl) {


	if (sd == NULL) {
		tdt_error ("Null argument to add_decl.");
		exit (1); 
	}

	sd->num_decls++;

	sd->decls =
		(Decl *) realloc (sd->decls, sd->num_decls * sizeof (Decl));

	if (sd->decls == NULL) {
		tdt_error ("Memory allocation in add_decl.");
		exit (1);
	}

	sd->decls[sd->num_decls - 1] = decl;
}

/* Returns NULL on error */
TypeDesc
typedef_type (TypedefDesc tdd) {

	if (tdd == NULL) {
		tdt_error ("Null argument to typedef_type.");
		return NULL;
	}

	return tdd->basetype;
}

/* Returns NULL on error */
String
typedef_name (TypedefDesc tdd) {

	if (tdd == NULL) {
		tdt_error ("Null argument to typedef_name.");
		return NULL;
	}
	return tdd->typedefname;
}

/* Other useful operations */

/* Returns TRUE if the argument is the name of a primitive type. */
Boolean
is_primitive (TypeName tn) {

	if (tn <= TDT_DOUBLE)
		return TRUE;
	return FALSE;
}

/* Functions to compute the sizes of different types. */
int
char_size (TypeVal tv) {

	return sizeof (char);
}

int
int_size (TypeVal tv) {

	return sizeof (int);
}

int
float_size (TypeVal tv) {

	return sizeof (float);
}

int
double_size (TypeVal tv) {

	return sizeof (double);
}

int
addr_size (TypeVal tv) {

	return sizeof (char *);
}

/* Returns -1 on error */
int
array_size (TypeVal tv) {

	ArrayDesc   ar = tv;


	if (ar == NULL) {
		tdt_error ("Null argument to array_size.");
		return -1;
	}

	return array_sz (ar) * type_size (array_type (ar));
}

/* Returns -1 on error */
int
struct_size (TypeVal tv) {

	StructDesc  sd = tv;
	Decl        d;
	int         sz = 0, al = 0;
	int 		tsz, tal;
	TypeDesc	td;
	int i;

	if (sd == NULL) {
		tdt_error ("Null argument to struct_size.");
		return -1;
	}

	i = 0;
	reset_decls (sd);
	while (has_more_decls (sd)) {
		d = next_decl (sd);
		if (d == NULL) {
			tdt_error ("next_decl in struct_size.");
			return -1;
		}

		td = decl_type (d);
		if (td == NULL) {
			tdt_error ("decl_type in struct_size.");
			return -1;
		}

		tsz = type_size (td);
		if (tsz == -1) {
			tdt_error ("type_size in struct_size.");
			return -1;
		}
#ifdef _AIX
		tal = type_align(td);
		if ( i == 0 && type_name(td) == TDT_DOUBLE )
			tal = 8;
		else if ( i == 0 && is_struct_one_double (td) )
			tal = 8; 

		i = 1;
#else
        tal = type_align(td);
#endif

		if (tal == -1) {
			tdt_error ("type_align in struct_size.");
			return -1;
		}
		if (al < tal)
			al = tal;
		if(sz == 0)
			sz = tsz;
		else
			sz += tsz + (tal - (sz - 1) % tal - 1);
	}
	sz = sz + (al - (sz - 1) % al - 1);
	
	return sz;
}



int
(*get_size[]) (TypeVal tv) = {
	 	char_size,
		int_size,
		float_size, 
		double_size, 
		addr_size, 
		array_size, 
		struct_size};

/* Returns -1 on error */
int
type_size (TypeDesc td) {

	TypeName    tn;
	TypeVal     tv;

	if (td == NULL) {
		tdt_error ("Null argument to type_size.");
		return -1;
	}

	tn = type_name (td);
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in type_size.");
		return -1;
	}

	tv = type_value (td); /* type_value crashes on error */

	return get_size[tn] (tv);
}

/* function to return type of a decl */
/* Returns NULL on error */
TypeDesc
find_type_in_decls (Decl *decls, int num_decls, String name) {

	int         i;
	String 		dname;
	TypeDesc 	td;

	if (decls == NULL) {
		tdt_error ("Null argument decls in find_type_in_decls.");
		return NULL;
	}

	for (i = 0; i < num_decls; i++) {

		dname = decl_name (decls[i]);
		if (dname == NULL) {
			tdt_error ("Found NULL-named decl in find_type_in_decls.");
			return NULL;
		}

		if (strcasecmp (dname, name) == 0) {
			td = decl_type (decls[i]);
			if (td == NULL) {
				tdt_error ("decl_type in find_type_in_decls.");
				return NULL;
			}
			return td;
		}
	}

	return NULL; /* no decl named name was found */
}


/*dan*/


/* Functions to compute the alignements of different types. */
int
char_align (TypeVal tv) {

	return sizeof (char);
}

int
int_align (TypeVal tv) {

	return sizeof (int);
}

int
float_align (TypeVal tv) {

	return sizeof (float);
}

int
double_align (TypeVal tv) {

#ifdef _AIX 
	return sizeof (int);
#else
	return sizeof(int);  /* 4 for Linux */
#endif
}

int
addr_align (TypeVal tv) {

	return sizeof (char *);
}

/* Returns -1 on error */
int
array_align (TypeVal tv) {

	ArrayDesc   ar = tv;


	if (ar == NULL) {
		tdt_error ("Null argument to array_align.");
		return -1;
	}
	
	
	return type_align (array_type (ar));
}

/* Returns -1 on error */
int
struct_align (TypeVal tv) {

	StructDesc  sd = tv;
	Decl        d;
	int         sz = 0;
	int 		tsz;
	TypeDesc	td;
#ifdef _AIX
	TypeDesc	aux;
#endif
	int i;
	
	if (sd == NULL) {
		tdt_error ("Null argument to struct_align.");
		return -1;
	}
#ifdef _AIX
	/*is_struct_one_double takes as parameter TypeDesc, not StructDesc
	so we create it and the delete it*/

	aux = (TypeDesc) malloc (sizeof (struct typedesc));
	aux->name = TDT_STRUCT;
	aux->value = (void*) sd;
	if ( is_struct_one_double (aux) )
		return 4;
	free (aux);
#endif
	reset_decls (sd);
	i = 0;
	while (has_more_decls (sd)) {
		d = next_decl (sd);
		if (d == NULL) {
			tdt_error ("next_decl in struct_align.");
			return -1;
		}

		td = decl_type (d);
		if (td == NULL) {
			tdt_error ("decl_type in struct_align.");
			return -1;
		}
#ifdef _AIX
		tsz = type_align (td);
		if (i == 0 && type_name(td) == TDT_DOUBLE)
			tsz = 8;
		else if ( i == 0 && is_struct_one_double (td) )
			tsz = 8;
		i = 1 ;
#else
		tsz = type_align (td);
#endif
		if (tsz == -1) {
			tdt_error ("type_align in struct_align.");
			return -1;
		}

		if (sz < tsz)
			sz = tsz;
	}

	return sz;
}

int
(*get_align[]) (TypeVal tv) = {
	 	char_align,
		int_align,
		float_align, 
		double_align, 
		addr_align, 
		array_align, 
		struct_align};

/* Returns -1 on error */
int
type_align (TypeDesc td) {

	TypeName    tn;
	TypeVal     tv;

	if (td == NULL) {
		tdt_error ("Null argument to type_align.");
		return -1;
	}

	tn = type_name (td);
	if (tn == TDT_NOTYPE) {
		tdt_error ("type_name in type_align.");
		return -1;
	}

	tv = type_value (td); /* type_value crashes on error */

	return get_align[tn] (tv);
}

/*end dan*/

TypeDesc
multi_array_type (ArrayDesc ar)
{
	ArrayDesc art = ar;
	TypeDesc t = ar->type;

	while (t->name == TDT_ARRAY) {
	
		art = (ArrayDesc) t->value;
		t=art->type;
	}

	return (t);
}


int
multi_array_size (ArrayDesc ar) {

	int size;
	ArrayDesc art = ar;
	TypeDesc t = ar->type;
	
	size = array_sz (ar);
	if (size == -1 )
		return (-1);
	
	while (t->name == TDT_ARRAY) {

		art = (ArrayDesc) t->value;
		size = size * array_sz (art);
		t = art->type;
	}

	return (size);
	
}

Boolean is_struct_one_double ( TypeDesc td)
{
    Decl dec;
    StructDesc sd;

    if ( td->name != TDT_STRUCT )
        return (FALSE);

    sd = (StructDesc) td->value;

    if ( sd->num_decls > 1 )
        return (FALSE);

    dec = sd->decls[0];

    if (dec->type->name == TDT_DOUBLE)
        return (TRUE);
    else if(dec->type->name == TDT_STRUCT)
        return (is_struct_one_double (  dec->type ));
    else
        return (FALSE);
}

