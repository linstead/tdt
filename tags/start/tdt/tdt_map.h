/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * 03/12/01: Cezar
 * Separated tdt_map from varops.
 * The idea is that tdt_map is a universal interface to varops, and will
 * not change, whereas the various varops are just default
 * implementations, with various degrees of stability.
 */
#ifndef __TDT_MAP_H__
#define __TDT_MAP_H__

/*
 * tdt_map: acts on the variable val according to td, using functions
 * arguments:
 *      void *val: a pointer to a value of arbitrary type, sent by the
 *      user
 *      TypeDesc td: a type description of the val
 *      void *args: additional arguments, to be sent to the functions
 *      void *functions: an array of functions that implement operations
 *      on primitive and composite types.
 * error conditions:
 *  TODO:
 */
void tdt_map (void *val, TypeDesc td, void *args, void *functions);

#endif
