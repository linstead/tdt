/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* CLIENT (WRITER) EXAMPLE */

#include <stdio.h>
#include <stdlib.h>
#include "tdt.h"

int
main (int argc, char **argv) {

    /* a loop counter */
    int i,size;
    int *array;

    /* declare some variables required by TDT */
    TDTConfig tc;
    TDTState  ts;

    size = 3;

    array = (int *) malloc (size * sizeof (int));
    for (i = 0; i < size; i++) {
        array[i] = i * 2;
    }

    /* Get the configuration */
    tc = tdt_configure ("clntconf.xml");
    
    /* Establish the connection */
    ts = tdt_open (ts, tc, "clnt_to_serv");
    
    /* we haven't yet told TDT how big dynarray will be, so do that now ... */
    tdt_size_array (ts, "arr", size);

    for (i=0; i<size; i++) {
        printf ("writing %d\n", array[i]);
    }

    /* ... and write it */
    tdt_write (ts, array, "arr");
    
    /* tidy up, close connections etc. */
    tdt_close (ts);
    tdt_end (tc);

    return 0;
}
