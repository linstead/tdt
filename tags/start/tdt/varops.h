/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * The prototypes here belong to the operations on compound  types. 
 * Originated by Cezar.
 */
#ifndef __VAROPS_H__
#define __VAROPS_H__

/*
 * map_addr: the indirection operation.
 * arguments:
 *      void * val: a value, sent by the user, representing a pointer to
 *                  something
 *      void *type: a TypeValue that must cast to an AddrDesc
 *      void *args: additional arguments
 *      void *functions: the array of operations on all types.
 * error conditions:
 *  TODO: 
 */
void
map_addr (void *, void *, void *, void *functions);

/*
 * map_array: stepping through an array
 * arguments:
 *      void *val: a value, sent by the user, representing an array
 *      void *type: a TypeValue that must cast to an ArrayDesc
 *      void *args: additional arguments
 *      void *functions: the array of operations on all types.
 * error conditions:
 *  TODO:
 */

void
map_array (void *, void *, void *, void *functions);

void
map_array_readblocks (void *, void *, void *, void *functions);
void
map_array_writeblocks (void *, void *, void *, void *functions);

#ifdef _MPI
void
map_array_readblocks_mpi (void *, void *, void *, void *functions);
void
map_array_writeblocks_mpi (void *, void *, void *, void *functions);
#endif

/*
 * map_array: stepping through the elements of a struct
 *      void *val: a value, sent by the user, representing a struct
 *      void *type: a TypeValue that must cast to a StructDesc
 *      void *args: additional arguments
 *      void *functions: the array of operations on all types.
 * error conditions:
 *  TODO:
 */
void
map_struct (void *, void *, void *, void *functions);

#endif
