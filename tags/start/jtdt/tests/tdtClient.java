class tdtClient {

    public static void main(String[] args) {


        int[][] values = new int[6][2];
        double[][][][] dvalues = new double[10][10][10][10];

        int tc;
        int ts;

        jTDT tdt = new jTDT();
        tc = tdt.config("clnt.xml");
        ts = tdt.open(tc, "clnt_to_serv");

        values[1][0] = 333;
        
        dvalues[7][2][4][9] = 788.123;

        tdt.write (ts, values, "dataarray");
        tdt.write (ts, dvalues, "dataarray_d");

        tdt.close(ts);
        tdt.end(tc);
    }
}
