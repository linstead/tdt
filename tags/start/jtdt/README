Creating a Java interface to the TDT library using the Java Native Interface (JNI) programming interface.

(see the Java tutorial: http://java.sun.com/docs/books/tutorial/native1.1/stepbystep/step1.html)

Step 1: Write the Java code.
Step 2: Compile the Java class : javac
Step 3: Generate the header file : javah
Step 4: Write the implementation of the native method
Step 5: Compile header and implementation into a shared object
gcc -I/usr/java/j2sdk1.4.1_03/include/ -I/usr/java/j2sdk1.4.1_03/include/linux \
    -shared TDT.c -o libjtdt.so 
Step 6: Run the java program


When building an application (e.g. TDT client), make sure that the current
directory is in the CLASSPATH When running the java program, make sure that
LD_LIBRARY_PATH contains the directory path to the shared object file. The
following is an example of how to correctly set up the environment variables:

unset CLASSPATH
unset LD_LIBRARY_PATH
export CLASSPATH=$CLASSPATH:$HOME/tdtcvs/jtdt:.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/tdtcvs/tdt:$HOME/tdtcvs/jtdt

*** NB ***
This assumes that a shared library for the TDT (e.g. libtdt.so.1) exists in
$HOME/tdtcvs/tdt

This is built from the tdtcvs/tdt directory by the following commands:

make -f Makefile.shared shlib
ln -s libtdt.so libtdt.so.1

***

Limitations

The Java Native Interface to the TDT library is restricted by not having access
to the physical layout of data in memory. Separate interface functions are
therefore required in order to transfer arrays of integers, doubles or floats.
Transfer of Java classes has been investigated and seems feasible but has not
been implemented.

The following section from the Java Language Environment White Paper explains
this limitation in more detail:

"White Paper
The Java Language Environment
        
6.1 Memory Allocation and Layout

    One of the Java compiler's primary lines of defense is its memory
allocation and reference model. First of all, memory layout decisions are not
made by the Java language compiler, as they are in C and C++. Rather, memory
layout is deferred to run time, and will potentially differ depending on the
characteristics of the hardware and software platforms on which the Java system
executes.

    Secondly, Java does not have "pointers" in the traditional C and C++ sense
of memory cells that contain the addresses of other memory cells.The Java
compiled code references memory via symbolic "handles" that are resolved to
real memory addresses at run time by the Java interpreter. Java programmers
can't forge pointers to memory, because the memory allocation and referencing
model is completely opaque to the programmer and controlled entirely by the
underlying run-time platform.

    Very late binding of structures to memory means that programmers can't
infer the physical memory layout of a class by looking at its declaration. By
removing the C and C++ memory layout and pointer models, the Java language has
eliminated the programmer's ability to get behind the scenes and either forge
or otherwise manufacture pointers to memory. These features must be viewed as
positive benefits rather than a restriction on the programmer, because they
ultimately lead to more reliable and secure applications."
