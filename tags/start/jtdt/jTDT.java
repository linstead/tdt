public class jTDT {

    public int tdtconf;
    public int tdtstate;

    public native int  config (String confname);
    public native int  open   (int tdtconf, String cname);

  
    public native void read   (int tdtstate, Object values, String name);
    public native void write  (int tdtstate, Object values, String name);

  
    public native void close  (int tdtstate);
    public native void end    (int tdtconfig);

    static {
        System.loadLibrary("jtdt");
    }

}
