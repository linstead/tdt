\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage{html}
\usepackage{fancyvrb}
\usepackage[latin1]{inputenc}
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}

\usepackage{babel}
\makeatother
\begin{document}

\title{Typed Data Transfer (TDT) User's Guide}


\author{Ciaron Linstead <linstead@pik-potsdam.de>}

\maketitle
Revision History:

04/03/2002 CL - version 0.1

12/09/2003 CL - version 0.2, extensive revision

29/07/2004 CL - version 0.3, converted master document to \LaTeX\ from LyX

18/11/2004 CL - version 0.4, minor mods to C section, added Python section

\section{Introduction}

The Typed Data Transfer (TDT) Library provides a simple, consistent interface
for the transmission of data between programs in a platform- and
language-independent way. It moves the complexities of handling data types and
data sources into a self-contained library of functions.

In this way, complex data types (i.e. data types composed of elements with
different data types, like a "struct" in C) can be passed between TDT-enabled
programs with a single function call.

The TDT library also takes care of byte swapping when transferring data between
big-endian and little-endian architectures.

The speed of transferring blocks of homogenous data (like arrays) is
practically the same as with the non-TDT method.

The flexibility of the TDT approach means that modules can be coupled in
various configurations and by various communications methods simply by altering
configuration files: no re-compilation of modules is necessary.

The TDT functions are written in C, and are provided with Fortran interface
functions for using the library in Fortran programs. Opening and closing of
sockets and files are handled by TDT functions, and data is written or read by
means of a call to the appropriate TDT function.

Apart from adding function calls the code, a programmer must also provide an
XML (eXtensible Markup Language) description of the data to be transferred and
a configuration file for each program, also in XML. Each data structure being
transferred needs its own XML description, each of which may be in separate XML
files, or in just one.

The purpose of this document is to explain the use of the TDT function library
when writing programs in C or Fortran. Some example programs are included and
will be explained. 


\section{Using the TDT in C}


\subsection{Prerequisites}


\subsubsection{"\#include"s}

To include the TDT library, simply

\begin{verbatim}
    #include "tdt.h"
\end{verbatim}

\subsubsection{Declarations}

It is necessary to declare a variable of type \texttt{TDTState}. This
variable serves as a unique identifier for each data item being transferred,
by bundling an XML description and a descriptor for the communication
channel.

A variable of type \texttt{TDTConfig} is also required. This is used
to refer to the data which will be read from a configuration file.

Examples:

\begin{verbatim}
    TDTState ts;
    TDTConfig tc;
\end{verbatim}

\subsection{User functions}


\subsubsection{tdt\_configure()}

Purpose

Read and parse the specified configuration file.

Input parameter

\begin{verbatim}
    String config_filename /* the name of the config file*/
                           /* to be parsed */
\end{verbatim}
Output parameter

\begin{verbatim}
    TDTConfig tc /* a parsed copy of the configuration file */
\end{verbatim}
Example:

\begin{verbatim}
    tc = tdt_configure("config.xml");
\end{verbatim}

\subsubsection{tdt\_open()}

Purpose

\texttt{tdt\_open()} opens the communication channel specified by
the connection name

Input parameters

\begin{verbatim}
    TDTConfig tc            /* the TDTConfig variable */

    String connection_name  /* the connection to open */
\end{verbatim}
Output parameter

\begin{verbatim}
    TDTState ts             /* a completed TDTState variable */
\end{verbatim}
Example:

\begin{verbatim}
    ts = tdt_open (tc, "client_to_server");
\end{verbatim}

\subsubsection{tdt\_read()}

Purpose

To read the data specified by the given XML identifier, from the connection
given in the \texttt{TDTState} parameter. 

Input parameters

\begin{verbatim}
    TDTState ts         /* identifier for this data transfer */

    void *value         /* where to store the incoming data */

    String name         /* the name of the data to be read, */
                        /* as it appears in the datadesc */
\end{verbatim}
Output parameters

None.

Example

\begin{verbatim}
    tdt_read (ts, &astruct, "astruct");
\end{verbatim}

\subsubsection{tdt\_write()}

Purpose

To write data to the connection given by the \texttt{TDTState} parameter
as per the XML identifer string (parameter \texttt{"name"}).

Input parameters

\begin{verbatim}
    TDTState ts         /* identifier for this data transfer */

    void *value         /* pointer to the data being written */

    String name         /* the name of the data to be written, */
                        /* as it appears in the datadesc */
\end{verbatim}
Output parameters

None.

Example

\begin{verbatim}
    tdt_write (ts, &astruct, "astruct");
\end{verbatim}

\subsubsection{tdt\_size\_array()}

Purpose

This function is used to redefine the size of an array given in a datadesc. If
your program uses dynamically sized arrays, you probably won't know in advance
what size the array is when you create the datadesc XML. In this case, you can
set the array size to zero in the datadesc and adjust the size later. 

Input parameters

\begin{verbatim}
    TDTState ts        /* identifier for this data transfer */

    String name        /* name of the array being resized */

    int size           /* the new size of the array */
\end{verbatim}

Output parameters

None.

Example

\begin{verbatim}
    tdt_size_array (ts, "dyn", new_int);
\end{verbatim}

\subsubsection{tdt\_close()}

Purpose

Closes the connection or open files and frees TDT-allocated memory.

Input parameters

\begin{verbatim}
    TDTState ts             /* The connection to be closed */
\end{verbatim}
Output parameters

None.

Example

\begin{verbatim}
    tdt_close (ts);
\end{verbatim}

\subsubsection{tdt\_end()}

Purpose

Frees memory allocated by the TDT for the configuration information.

Input parameters

\begin{verbatim}
    TDTConfig tc            /* The config data to be freed */
\end{verbatim}
Output parameters

None.

Example

\begin{verbatim}
    tdt_end (tc);
\end{verbatim}

\subsection{Compiling and linking}


\subsubsection{Compiling the library}

The TDT can be compiled as a static library, \texttt{libtdt.a}. This must
be in the library path of the development environment, or a directory
specified by the \texttt{-L} flag in gcc. A makefile is included with
the source files. Running \texttt{"make lib"} from within the \texttt{tdt}
source directory will rebuild the TDT library.


\subsubsection{Linking with your own programs}

First build the library \texttt{libtdt.a} as explained above. 

Compile your application...

\begin{verbatim}
    gcc -c -I<location of tdt.h> -Wall <program>.c
\end{verbatim}
... and link: 

\begin{verbatim}
    gcc <program>.o -L<location of TDT library> \
                    -ltdt -lexpat_linux -o<program>
\end{verbatim}
where \texttt{<program>.o} is the output from the compilation of \texttt{<program>.c}

This assumes that the Expat XML parser library is available system-wide
on your machine. If it is not, and you want to use the Expat library
supplied with the TDT, use the following link command:

\begin{verbatim}
    gcc <program>.o -L<location of TDT library> -ltdt \
        -L<location of Expat library> -lexpat -o<program>
\end{verbatim}

\subsection{An example program}

The following client program and it's associated server and a Makefile can be
found in the \texttt{tdt/tests} subdirectory of the TDT distribution.

\begin{Verbatim}[numbers=left]
/* CLIENT (WRITER) EXAMPLE */

#include <stdio.h>
#include <stdlib.h>
#include "tdt.h"

int
main (int argc, char **argv) {

    /* a loop counter */
    int i;

    /* declare the variables we're going to write */
    double vardouble;

    struct {
        int    elem1[2];
        double elem2;
    } astruct;

    int varint;
    double *dynarray;

    /* declare some variables required by TDT */
    TDTConfig tc;
    TDTState  ts;

    /* Now make up some meaningful data to write... */
    astruct.elem1[0] = 10;
    astruct.elem1[1] = 20;
    astruct.elem2 = 199.99;

    vardouble = 123.45;
    varint = 12345;

    dynarray = (double *) malloc (varint * sizeof (double));
    for (i = 0; i < varint; i++) {
        dynarray[i] = i * 0.33;
    }

    /* Get the configuration */
    tc = tdt_configure ("clntconf.xml");

    /* Establish the connection */
    ts = tdt_open (tc, "clnt_to_serv");

    /* now write the data */
    tdt_write (ts, &vardouble, "vardouble");
    tdt_write (ts, &astruct, "astruct");
    tdt_write (ts, &varint, "varint");

    /* we haven't yet told TDT how big dynarray will be, so do that now ... */
    tdt_size_array (ts, "dyn", varint);

    /* ... and write it */
    tdt_write (ts, dynarray, "dyn");

    /* tidy up, close connections etc. */
    tdt_close (ts);
    tdt_end (tc);

    return 0;
}
\end{Verbatim}

The TDT-specific lines to note are these:

5: Includes \texttt{tdt.h}

25, 26 : Declare \texttt{TDTState} (for connection-specific configuration) and
\texttt{TDTConfig} (for program-specific configuration)

42: Calling \texttt{tdt\_configure()} gets all the configuration data required

45: Open the connection. \texttt{clnt\_to\_serv} is the name in
\texttt{clntconf.xml} which uniquely identifies the connection to open.

48, 49, 50: Each call to \texttt{tdt\_write()} does just that: it writes the data.
On line 48 for example, the variable \texttt{vardouble} is written to
connection \texttt{ts}, and the description comes from \texttt{vardouble}. Note
that the name of the decl in the data description does not have to be the same
as that of the variable, but it makes it clearer if it is.

53: Using \texttt{tdt\_size\_array} to dynamically size an array. Note that
we've already written the variable \texttt{varint} to the server. It will use
this variable in a similar call to \texttt{tdt\_size\_array} before reading the
dynamic array.

59, 60: Tidying up. \texttt{tdt\_close} closes the connection \texttt{ts}. We
could re-open it later, but not after we've called \texttt{tdt\_end} which
removes the configuration information from memory.

\subsection{Error conditions}

The user functions of the TDT will crash, returning with error code
1, if an error occurs. It will also output a debugging message to
\texttt{stderr}. 

\section{Using the TDT in Fortran}

\subsection{Prerequisites}

\subsubsection{Declarations}

The following declarations are needed:

\begin{verbatim}
    INTEGER tdtstate
    INTEGER tdtconfig
\end{verbatim}

\subsection{User functions}


\subsubsection{tdt\_fconfigure()}

Purpose

Read and parse the specified configuration file.

Input parameter

\begin{verbatim}
    C the name of the configuration file
    configfilename
\end{verbatim}
Output parameter

\begin{verbatim}
    C a parsed copy of the configuration information
    tdtconf
\end{verbatim}
Example

\begin{verbatim}
    CALL tdt_fconfigure(tdtconf, "config.xml");
\end{verbatim}

\subsubsection{tdt\_fopen()}

Purpose

\texttt{tdt\_fopen()} opens the communication channel specified by
the connection name.

Input parameters

\begin{verbatim}
    C a TDTState variable
    tdtstate

    C a TDTConfig variable
    tdtconf

    C the connection name
    connection
\end{verbatim}
Output parameter

\begin{verbatim}
    C a completed TDTState variable
    tdtstate
\end{verbatim}
Examples:

\begin{verbatim}
    CALL tdt_fopen (tdtstate, tdtconf, "client_to_server")
\end{verbatim}

\subsubsection{tdt\_fwrite()}

Purpose

To write data to the connection given by the \texttt{TDTState} parameter
as per the XML identifer string (parameter \texttt{"name"}).

Input parameters

\begin{verbatim}
    C The identifier for this data transfer
    tdtstate

    C the data being written
    val

    C a string containing the name of the data to be written, 
    C as it appears in the XML description 
    name
\end{verbatim}
Output parameter

None.

Example

\begin{verbatim}
    CALL tdt_fwrite (tdtstate, astruct, "astruct");
\end{verbatim}

\subsubsection{tdt\_fread()}

Purpose

To read data from the connection given by the \texttt{TDTState} parameter
as per the XML identifer string (parameter \texttt{"name"}).

Input parameters

\begin{verbatim}
    C The identifier for this data transfer
    tdtstate

    C the data being read
    val 

    C a string containing the name of the data to be read, 
    C as it appears in the XML description
    name 
\end{verbatim}
Output parameter

None.

Example

\begin{verbatim}
    CALL tdt_fread (tdtstate, astruct, "astruct");
\end{verbatim}

\subsubsection{tdt\_fclose()}

Purpose

Closes the connection or open files and frees TDT-allocated memory.

Input parameters

\begin{verbatim}
    C The identifier of the connection to be closed
    ts
\end{verbatim}
Output parameter

None.

Example

\begin{verbatim}
    CALL tdt_fclose (tdtstate);
\end{verbatim}

\subsubsection{tdt\_fend()}

Purpose

Frees memory allocated by the TDT for the configuration information.

Input parameters

\begin{verbatim}
    C The identifier of the stored configuration data
    tc 
\end{verbatim}
Output parameter

None.

Example

\begin{verbatim}
    CALL tdt_fend (tdtconf);
\end{verbatim}

\subsection{Compiling and linking}


\subsubsection{Compiling the library}

The TDT can be compiled as a library, \texttt{libtdt.a}. This must
be in the library path of the development environment, or a directory
specified by the \texttt{-L} flag in gcc. A makefile is included with
the source files. Running '\texttt{make lib}' from within the \texttt{tdt}
source directory will rebuild the TDT library.


\subsubsection{Linking with your own programs}

First build the library \texttt{libtdt.a} as explained above. 

Compile your application...

\begin{verbatim}
    xlf -c -Wall <program>.f
\end{verbatim}
... and link: 

\begin{verbatim}
    xlf <program>.o -L../tdt -o <program> -ltdt -lexpat_linux \
        -o <program>
\end{verbatim}
where \texttt{<program>.o} is the output from the compilation of \texttt{<program>.f}

This assumes that the Expat XML parser library is available system-wide
on your machine. If it is not, and you want to use the Expat library
supplied with the TDT, use the following link command:

\begin{verbatim}
    xlf <program>.o -L<location of TDT library> -ltdt /
                    -L<location of Expat library> -lexpat
\end{verbatim}

\subsection{An example program}

\begin{Verbatim}[numbers=left]
PROGRAM fclnt
INTEGER:: data1(2,5)
REAL*8:: data2(10,15,10,10)
INTEGER tdtstate1
INTEGER tdtstate2
INTEGER tdtconf

DO k = 1,2
    DO l = 1,5
    data1(k,l) = k*10 + l
    PRINT *, data1(k, l)
    END DO
END DO

CALL tdt_fconfigure (tdtconf, 'clntconf.xml')

CALL tdt_fopen (tdtstate1, tdtconf, 'conn1')
CALL tdt_fwrite (tdtstate1, 'a', data1)

CALL tdt_fopen (tdtstate2, tdtconf, 'conn2')
CALL tdt_fread (tdtstate2, 'b', data2)

PRINT *, 'I read:'
PRINT *, data2

CALL tdt_fclose (tdtstate1)
CALL tdt_fclose (tdtstate2)
CALL tdt_fend (tdtconf)

END
\end{Verbatim}

The TDT-specific lines are the following:

4, 5: Declare two \texttt{TDTState}s. One is for a \texttt{tdt\_fread} and the
other for a \texttt{tdt\_fwrite}.

6: Declare a \texttt{TDTConfig} variable.

15: Parse the configuration data

17: Open the first connection 

18: Write data on the open connection

20: Open another connection

21: Read data from the new connection

26, 27, 28: Close connections and tidy up.

\subsection{Error conditions}

The user functions of the TDT will crash, returning with error code
1, if an error occurs. It will also output a debugging message to
\texttt{stderr}. 

\section{Using the TDT in Python}

\subsection{Prerequisites}

\subsubsection{Imports}
The line
\texttt{from tdt import TDT} should appear at the start of your programs. This
requires that tdt.py be in your PYTHONPATH.

\subsubsection{Declarations}
Declare a new instance of a TDT object like this:
\texttt{tdt = TDT()}

\subsection{User Functions}

\subsubsection{config}
Purpose

Read and parse the given configuration file.

Input parameter

\begin{verbatim}
    config_filename /* the name of the config file to be parsed */
\end{verbatim}

Output parameter

None

Example

\begin{verbatim}
tdt.config ("clntconf.xml")
\end{verbatim}


\subsubsection{open}
Purpose

\texttt{open()} opens the communication channel specified by the connection name

Input parameters
\begin{verbatim}
    channel_name /* the name of the channel to be opened */
\end{verbatim}

Example
\begin{verbatim}
    tdt.open("channel1")
\end{verbatim}

\subsubsection{read}
Purpose

To read the data specified by the given XML identifier.

Input parameters
\begin{verbatim}
    datadesc     /* the description of the data being read */
    variable     /* the data to be read */
\end{verbatim}
Example
\begin{verbatim}
    tdt.read(d, "a_double")
\end{verbatim}


\subsubsection{write}
Purpose

To write data identified by the given string.

Input parameters
\begin{verbatim}
    datadesc     /* the description of the data being written */
    variable     /* the data to be written */
\end{verbatim}
Example
\begin{verbatim}
    tdt.write(d, "a_double")
\end{verbatim}



\subsubsection{size\_array}
Purpose

This function is used to redefine the size of an array given in a datadesc. If
your program uses dynamically sized arrays, you probably won't know in advance
what size the array is when you create the datadesc XML. In this case, you can
set the array size to zero in the datadesc and adjust the size later.

Input parameters
\begin{verbatim}
    array        /* the array being resized */
    size         /* the new size of the array */
\end{verbatim}
Example
\begin{verbatim}
    tdt.size_array("dynarray", 10)
\end{verbatim}

\subsubsection{end}
Purpose

Closes connections or open files and releases TDT structures.

Input parameters

None

Example
\begin{verbatim}
    tdt.end()
\end{verbatim}

\subsection{Example program}
\begin{Verbatim}[numbers=left]
from tdt import TDT

tdt = TDT ()

tdt.config ("servconf.xml")
tdt.open ("input1")

d = tdt.read ("new_double")
print 'd = ', str (d)

astruct = tdt.read ("astruct")
print 'astruct = ', repr (astruct)

i = tdt.read ("new_int")
print 'i = ', str (i)

tdt.size_array ("dyn", i)

dyn = tdt.read ("dyn")

print dyn

tdt.end ()
\end{Verbatim}


\section{TDT Data descriptions in XML}

XML (eXtensible Markup Language) is a widely used format for structured data and
documents. For a more detailed introduction to XML, see 

\htmladdnormallink{http://www.w3schools.com/xml/default.asp}{http://www.w3schools.com/xml/default.asp}

\subsection{<data\_desc> tags}

This is the root element (or document element) required by XML.

Attributes : none.


\subsection{<decl> tag}

Each XML description of a model is made up of one or more variable
declarations, which are identified by \texttt{<decl>} tags.

Attributes : \texttt{name="abcde"} where \texttt{abcde}
is one or more alphanumeric characters.


\subsection{<struct> tag}

The \texttt{<struct>} and \texttt{</struct>} tags surround one or
more declarations, analogous to a \texttt{struct} declaration in C.

Attributes : none.


\subsection{<array> tag}

The \texttt{<array>} and \texttt{</array>} tags surround a text element
indicating the primitive type, or a declaration if the array is made
up of composite types.

Attributes : \texttt{size="n"} where n is an integer. If the size of the array
is unknown at the time the datadesc is being created, it can be set to zero and
later given a size with the \texttt{tdt\_size\_array()} function.


\subsection{<addr> tag}

The \texttt{<addr>} and \texttt{</addr>} tags surround declarations,
indicating that this is a pointer to a value, not the value itself.

Attributes : none.


\subsection{Text elements}

Text elements are the value which appear between start and end tags.
For TDT XML, text elements are used to indicate the primitive data
type of a declaration (\texttt{<decl>}) or an array (\texttt{<array>}).

The allowed values are \texttt{"int"}, \texttt{"double"},
\texttt{"float"}, and \texttt{"char"}.


\subsection{Example}

The following XML

\begin{verbatim}
<?xml version="1.0" encoding="ISO-8859-1"?>
<data_desc>

    <decl name="vardouble">double</decl>

    <decl name="astruct">
        <struct>
            <decl name="elem1">
                    <array size="2">int</array></decl>
            <decl name="elem2">double</decl>
        </struct>
    </decl>

    <decl name="varint">int</decl>

    <decl name="dyn">
        <array size="0">double</array>
    </decl>

</data_desc>
\end{verbatim}
represents describes the data being transferred in the client program above. It describes the following C declarations:

\begin{verbatim}
double vardouble;

struct {
    int    elem1[2];
    double elem2;
} astruct;

int varint;
double *dynarray;
\end{verbatim}

\section{TDT Configuration files}

The configuration files used by TDT are also written in XML. 


\subsection{<program> tag}

The document-level tag for a configuration file is the \texttt{<program>}
tag.

Attributes: \texttt{name="abcde"}, where \texttt{{}"abcde"}
is an alphanumeric string.


\subsection{<channel> tag}

The \texttt{<channel>} tag specifies all the information required
to establish a TDT connection and send or receive the data in the
correct format.

Attributes:

\texttt{name} : the name by which the channel will be referenced in
the code. 

\texttt{mode} : the mode of the channel, i.e. input or output (\texttt{"in"
| "out"})

\texttt{type} : the type of the connection: \texttt{"socket"
| "file"{}} 

\texttt{host} : if type is \texttt{"socket"}, the
hostname to use 

\texttt{port} : if type is \texttt{"socket"}, the
port to use 

\texttt{filename} : if type is \texttt{"file"},
the filename to read/write from/to 

\texttt{datadesc} : the name of the datadesc XML which describes the
data that will pass on this channel


\subsection{Example configuration file}

\begin{verbatim}

<program name="client">
  <channel name="clnt_to_serv" 
           mode="out"
           host="localhost" 
           port="2222" 
           type="socket" 
           datadesc="datadesc.xml">
  </channel> 
</program> 
\end{verbatim}

This configuration describes a program called \verb1"client"1 which has one
communication channel, namely output socket 2222 from machine \verb2"localhost"2. The
datadesc for this connection is in the file \verb3"datadesc.xml"3. Note that the
attribute \verb4"filename"4 is not required in this example. 

\section{Sample applications}

The sample applications are provided with Makefiles in order to compile
and link the programs. 


\subsection{C Examples}

The sample C programs (\texttt{testclnt.c} and \texttt{testserv.c}
in the \texttt{tests} subdirectory of the TDT source directory) demonstrate
writing data from a client application (\texttt{testclnt}) to a server
application (\texttt{testserv}). \texttt{testclnt} writes three variables
(a \texttt{double}, a \texttt{struct} containing an array of \texttt{int}
and a \texttt{double}, and an \texttt{int}) to \texttt{testserv}.
In this case, only one \texttt{TDTState} (for one XML description
and one communication channel) is required.

Building the examples

Type \texttt{"make all"} from within the \texttt{tdt/tests}
directory.

In \texttt{tdt/tests} this will create the programs \texttt{"serv"}
and \texttt{"clnt"}.


\subsection{Fortran examples}

The sample Fortran applications are two programs (\texttt{prog1.f}
and \texttt{prog2.f} in the \texttt{ftdt/tests} subdirectory) which
read and write data to/from each other. \texttt{prog1} writes a 2x5
matrix of INTEGERs for \texttt{prog2} to read and reads a 5x2 matrix
of doubles (REAL{*}8) which is written by \texttt{prog2}.

Two XML files are provided (\texttt{"example.xml"}
and \texttt{"example2.xml"}). These describe the
two matrices. In these examples the XML files are expected to be in
the same directory as the programs reading them.

Two \texttt{TDTState} variables are used by each program (\texttt{tdtstate1}
and \texttt{tdtstate2}). Each \texttt{TDTState} contains information
about one matrix and the channel by which that data will be transferred.

Building the examples

Type \texttt{"make all"} from within the \texttt{ftdt/tests}
directory.

In \texttt{ftdt/tests}, the programs will be called \texttt{"prog1"}
and \texttt{"prog2"}.


\section{"make" parameters}

The following parameters can be passed to \texttt{make}:

\texttt{FF} : specifies the name of the Fortran compiler (default
is \texttt{xlf}).

\texttt{EXPAT} : specifies the location of the Expat libraries (default
is \texttt{../../expat}).

\texttt{OS} : specifies the operating system (Linux or AIX). This
is used to select the appropriate Expat library (default is Linux).

Example:

\begin{verbatim}
    make FF=g77 EXPAT=~/expat1.95.2 OS=AIX all
\end{verbatim}
\texttt{"make clean"} will remove old object (\texttt{{*}.o})
files and executable programs from the \texttt{tdt/tests} and \texttt{tdt}
directories (or \texttt{ftdt/tests} and \texttt{ftdt}).

\subsection{Python examples}
The Python examples are two programs \texttt{clnt.py} and \texttt{serv.py} in
the \texttt{python/tests} subdirectory which read and write data between each
other. \texttt{clnt.py} writes four items to \texttt{serv.py}: a double, a
dictionary, an integer and a dynamic array (a Python list).

To run the examples, go to the python/tests directory and run \texttt{python
clnt.py} and \texttt{serv.py} (in any order).

If the following error occurs:

\begin{verbatim}
Traceback (most recent call last):
  File "clnt.py", line 1, in ?
      from tdt import TDT
ImportError: No module named tdt
\end{verbatim}

you need to adjust the PYTHONPATH variable on your system to include the
location of the \texttt{tdt.py} file.

\section{Further information}

If you need further information, have comments or requests for enhancements,
or you've found a bug, you can contact the authors:

Ciaron Linstead <linstead@pik-potsdam.de>

Cezar Ionescu <ionescu@pik-potsdam.de>

Dan Beli <beli@pik-potsdam.de>
\end{document}
