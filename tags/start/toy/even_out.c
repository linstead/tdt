
#include <stdio.h>

#include "tdt.h"
#include "tdt_extra.h"

int
main (int argc, char **argv) {

	int n;

	TDTState ts_out; 
	FILE *fp;
	char *ctrlhost = "pc08";
	int ctrlport = 2225;

	fp =  fopen ("n", "r");
	fscanf (fp, "%d", &n);
	fclose (fp);

	ts_out = tdt_init ("n.xml");
	ts_out = tdt_open_channel (ts_out, ctrlhost, ctrlport, WRITE);
	if (ts_out == NULL) {
		tdt_error ("tdt_open_channel returned NULL");
		return 1;
	}

	tdt_write (ts_out, &n, "n");
	tdt_end (ts_out);


	return 0;
}
