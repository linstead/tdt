/* Part of the Collatz conjecture example.
 * Takes as input the number stored in file n.
 * Outputs a file res and at most one of ineven, inodd.
 * If the input number is even, res will contain 2 and the number
 * will be stored in ineven.
 * If the input is odd and greater than 1, res will contain 1 and
 * the number will be stored in inodd.
 * If the input is 1, res will contain 0 and no other file is
 * produced.
 * The program will crash if:
 *      the input is smaller than 1 (this can happen if there is an
 *      overflow in program odd)
 *
 *      the required files not accessible
 */

#include <stdio.h>

int
main (int argc, char** argv) {
  FILE *in;
  FILE *res;
  FILE *out;
  int n;

  in = fopen ("n", "r");
  if (in == NULL) {
    fprintf (stderr, "Could not open input file\n");
    return 1;
  }

  fscanf (in, "%d", &n);
  fclose (in);

  if (n < 1) {
    fprintf (stderr, "Bad input: %d\n", n);
    return 1;
  }

  res = fopen ("res", "w");
  if (res == NULL) {
    fprintf (stderr, "Could not open output file res\n");
    return 1;
  }

  if (n == 1) {
    fprintf (res, "%d", 0);
    fclose (res);
    return 0;
  }

  if (n % 2 == 0) {
    fprintf (res, "%d", 2);
    fclose (res);
    out = fopen ("ineven", "w");
    if (out == NULL) {
      fprintf (stderr, "Could not open output file ineven\n");
      return 1;
    }
  } else {
    fprintf (res, "%d", 1);
    fclose (res);
    out = fopen ("inodd", "w");
    if (out == NULL) {
      fprintf (stderr, "Could not open output file inodd\n");
      return 1;
    }
  }
  
  fprintf (out, "%d", n);
  fclose (out);

  return 0;

}

  
