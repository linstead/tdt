
#include <stdio.h>

#include "tdt.h"
#include "tdt_extra.h"

int
main (int argc, char **argv) {

	int n;
	int res;

	FILE *fp_n;
	FILE *fp_res;

	TDTState ts_n; 
	TDTState ts_res;

	char *ctrlhost = "pc08";
	int ctrlport = 2223;

	fp_n =  fopen ("n", "r");
	fscanf (fp_n, "%d", &n);
	fclose (fp_n);

	fp_res =  fopen ("res", "r");
	fscanf (fp_res, "%d", &res);
	fclose (fp_res);

	ts_res = tdt_init ("res.xml");
	ts_res = tdt_open_channel (ts_res, ctrlhost, ctrlport, WRITE);
	if (ts_res == NULL) {
		tdt_error ("tdt_open_channel returned NULL");
		return 1;
	}

	tdt_write (ts_res, &res, "res");
	tdt_end (ts_res);

	ts_n = tdt_init ("n.xml");
	ts_n = tdt_open_channel (ts_n, ctrlhost, ctrlport, WRITE);
	if (ts_n == NULL) {
		tdt_error ("tdt_open_channel returned NULL");
		return 1;
	}

	tdt_write (ts_n, &n, "n");
	tdt_end (ts_n);

	return 0;
}
