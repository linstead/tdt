#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "tdt.h"
#include "tdt_extra.h"

int
main (int argc, char **argv) {

	int n=17;
	int res;

	TDTState ts_n;
	TDTState ts_res;
	
	int ctrl_eval_in, /* ctrl to eval*/
		ctrl_eval_out,
		ctrl_even_in,
		ctrl_even_out,
		ctrl_odd_in,
		ctrl_odd_out,
		eval_even,
		even_eval,
		eval_odd,
		odd_eval,
		eval_res,
		ctrl_n;

	ctrl_eval_in  = 2222;
	ctrl_eval_out = 2223;
	ctrl_even_in  = 2224;
	ctrl_even_out = 2225;
	ctrl_odd_in   = 2226;
	ctrl_odd_out  = 2227;
	eval_even     = 2228;
	even_eval     = 2230;
	eval_odd      = 2232;
	odd_eval      = 2234;
	ctrl_n	      = 2236;
	eval_res      = 2238;

	tdt_send_addr ("pc08", ctrl_eval_in, "pc08", ctrl_n);

	ts_n = tdt_init ("n.xml");
	ts_n = tdt_open_socket (ts_n, "pc08", ctrl_n, WRITE);
	tdt_write (ts_n, &n, "n");
	tdt_end (ts_n);

	do {

		ts_res = tdt_init ("res.xml");
		tdt_send_addr ("pc08", ctrl_eval_out, "pc08", eval_res);
		ts_res = tdt_open_socket (ts_res, "pc08", eval_res, READ);
		tdt_read (ts_res, &res, "res");
		tdt_end(ts_res);

		if (res == 0) {
			printf ("Process finished.\n");
			return 0;
		}

		if (res == 1) { /* run odd */

			/* send eval_out the address of odd_in */
			tdt_send_addr ("pc08", ctrl_eval_out, "pc08", eval_odd);
			/* send odd_in the address of eval_out */
			tdt_send_addr ("pc08", ctrl_odd_in, "pc08", eval_odd);
			/* send odd_out the address of eval_in */
			tdt_send_addr ("pc08", ctrl_odd_out, "pc08", odd_eval);
			/* send eval_in the address of odd_out */
			tdt_send_addr ("pc08", ctrl_eval_in, "pc08", odd_eval);

		} else { /* run even */
			/* send eval_out the address of even_in */
			tdt_send_addr ("pc08", ctrl_eval_out, "pc08", eval_even);
			/* send even_in the address of eval_out */
			tdt_send_addr ("pc08", ctrl_even_in, "pc08", eval_even);
			/* and the other way 'round */
			tdt_send_addr ("pc08", ctrl_even_out, "pc08", even_eval);
			tdt_send_addr ("pc08", ctrl_eval_in, "pc08", even_eval);
		}
	} while (TRUE);

			

	return 0;
}
