#include "math.h"
#include "mex.h"
#include "tdt.h"
#include "typeops.h"
#include "xmlhandlers.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int n;
	int len;
	int aux;
	char *name;
	void *val;
	TDTState ts;
	TypeName tn,tn1;
	TypeDesc td,td1;
	ArrayDesc ar;
	DataDesc    md;
	int *dims;
	int dims1[]={1};
	int dims2[]={1,0};
	int nodims;

	if (nrhs != 2 ) 
		mexErrMsgTxt ("tdt_read gets two input arguments"); 
	else if (nlhs != 1) 
		mexErrMsgTxt ("tdt_read returns one value");
	
	aux = *(mxGetPr (prhs[0]));
	
        ts = (TDTState)  aux;
	
	len = mxGetM(prhs[1]) * mxGetN(prhs[1]) + 1;
	
	name = mxCalloc(len, sizeof(char));
	mxGetString(prhs[1],name,len);
	
	md = get_datadesc (ts);
	td = find_type_in_datadesc (md, name);
	
	tn = type_name (td);
	if ( is_primitive (tn) ){
		if ( tn == TDT_CHAR ) {
			printf("Characters in MATLAB not yet implemented!\n");
			plhs[0] = NULL;
			return;
			plhs[0] = mxCreateCharArray ( 1, dims1);
		}
		else if ( tn == TDT_INT )
			plhs[0] = mxCreateNumericArray (1, dims1, mxINT16_CLASS, mxREAL);
		else if ( tn == TDT_FLOAT )
			plhs[0] = mxCreateNumericArray (1, dims1, mxSINGLE_CLASS, mxREAL);
		else if ( tn == TDT_DOUBLE )
			plhs[0] = mxCreateNumericArray (1, dims1, mxDOUBLE_CLASS, mxREAL);
		
	}
	else if ( tn == TDT_ARRAY ) {
		ar = (ArrayDesc) td->value;
		td1 = multi_array_type(ar);
		tn1 = type_name (td1);
		if ( !is_primitive (tn1) ) {
			printf("Unexistent MATLAB type!\n");
			exit (1);
		}
		nodims = multi_array_nodims (ar);

		dims = multi_array_dims (ar);
		
		if ( tn1 == TDT_CHAR ) {
			printf("Arrays of characters in MATLAB not yet implemented!\n");
			return;
			if (nodims > 1)
				plhs[0] = mxCreateCharArray (nodims, dims);
			else {
				dims2[1] = dims[0];
				plhs[0] = mxCreateCharArray (2, dims2);
			}
		}
		else if ( tn1 == TDT_INT )
			plhs[0] = mxCreateNumericArray (nodims, dims, mxINT16_CLASS, mxREAL);
		else if ( tn1 == TDT_FLOAT )
			plhs[0] = mxCreateNumericArray (nodims, dims, mxSINGLE_CLASS, mxREAL);
		else if ( tn1 == TDT_DOUBLE )
			plhs[0] = mxCreateNumericArray (nodims, dims, mxDOUBLE_CLASS, mxREAL);
	}
	else {
		printf("Unexistent MATLAB type!\n");
		exit (1);
	}



	tdt_read (ts, mxGetPr (plhs[0]), name);
 	mxFree(name);
}


