#include "math.h"
#include "mex.h"
#include "tdt.h"
#include "typeops.h"
#include "xmlhandlers.h"
                                                                                                              

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	

	int len;
	int n;
	int aux;
	char *name;
	double *val;
	TDTState ts;
        DataDesc    md;
	TypeName tn;
	TypeDesc td;
		

	if (nrhs != 3 ) 
		mexErrMsgTxt ("tdt_write gets three input arguments"); 
	else if (nlhs >= 1) 
		mexErrMsgTxt ("tdt_write returns no value");
	
	aux = *(mxGetPr (prhs[0]));
	ts = (TDTState) aux;
                                                                                                              
	len = mxGetM(prhs[2]) * mxGetN(prhs[2]) + 1;
	name = mxCalloc(len, sizeof(char));
	mxGetString(prhs[2],name,len);
				
	md = get_datadesc (ts);
        td = find_type_in_datadesc (md, name);
        tn = type_name (td);
	if( tn == TDT_CHAR ) {
		printf("Characters and arrays of characters in MATLAB not yet implemented!\n");
		return;
	}
								
	
	val=mxGetData(prhs[1]);
	
	tdt_write (ts, val, name);
	mxFree(name);
}
