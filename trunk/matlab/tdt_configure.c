#include "math.h"
#include "mex.h"
#include "tdt.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int n;
	int len;
	char *name;
	TDTConfig tc;

	if (nrhs != 1 ) 
		mexErrMsgTxt ("tdt_configure gets one input argument"); 
	else if (nlhs != 1) 
		mexErrMsgTxt ("tdt_configure returns one value");
	
	len = mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1;
	name = mxCalloc(len, sizeof(char));
	 
	mxGetString(prhs[0],name,len);
	tc = tdt_configure (name);
	tdt_set_array_order_oncolumns (tc);
	
	mxFree(name);

	plhs[0] = mxCreateDoubleMatrix (1,1, mxREAL); 
	*(mxGetPr (plhs[0])) = (int) tc;
	
}
