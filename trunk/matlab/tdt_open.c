#include "math.h"
#include "mex.h"
#include "tdt.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int n;
	int len;
	char *name;
	TDTConfig tc;
	TDTState ts;

	if (nrhs != 2 ) 
		mexErrMsgTxt ("tdt_open gets two input arguments"); 
	else if (nlhs > 1) 
		mexErrMsgTxt ("tdt_open returns one value");
	len =   *(mxGetPr (prhs[0]));
	tc = (TDTConfig) len;
	
	len = mxGetM(prhs[1]) * mxGetN(prhs[1]) + 1;
	name = mxCalloc(len, sizeof(char));
	 
	mxGetString(prhs[1],name,len);
	ts = tdt_open (tc,name);
	mxFree(name);
	plhs[0] = mxCreateDoubleMatrix (1,1, mxREAL); 
	*(mxGetPr (plhs[0])) = (int) ts;
}
