#include "math.h"
#include "mex.h"
#include "tdt.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int n;
	TDTState ts;

	if (nrhs != 1) 
		mexErrMsgTxt ("tdt_close gets one input argument"); 
	else if (nlhs >= 1) 
		mexErrMsgTxt ("tdt_close has no return values");
	n =   *(mxGetPr (prhs[0]));
	ts = (TDTState) n;
	 
	
	tdt_close (ts);
	
}
