# Simple XML-RPC server for registering TDT ports
# Ciaron Linstead, 24. November 2004.

import SimpleXMLRPCServer
import logging
import socket
import os

class RegistryFunctions:
    def __init__(self,host,port):

        #LOGGING = False
        self.LOGGING = True

        self.registry = {}
        self.logger = logging.getLogger('TDTRegistry')

        if os.name == 'nt':
            logfilename = "C:\WINDOWS\TEMP\TDTRegistry.log"
        elif os.name == 'posix':
            logfilename = "/var/tmp/TDTRegistry.log"

        if (self.LOGGING and (os.access (os.path.dirname (logfilename), os.W_OK) == True)):
            # set up logging

            hdlr = logging.FileHandler(logfilename)
            
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            self.logger.addHandler(hdlr) 
            self.logger.setLevel(logging.INFO)

            self.logger.info(" *** ")
            self.logger.info(" *** Registry started on %s, port %d *** " % (host, port))
            self.logger.info(" *** ")

        else:
            print "no write permission for directory %s, or directory does not exist. Logging is disabled" % os.path.dirname(logfilename)
            self.LOGGING = False

        print "TDT port registry is up and running on %s, port %s" % (host, port)

    def showregistered(self, hostname):

        # returns a list of currently registered ports
        if self.LOGGING:
            self.logger.info(hostname+": show registered channels")
            
        return self.registry

    def register(self, hostname, channelname, port):

        # return codes are:
        # 0 : success
        # 1 : channel already registered

        if self.registry.has_key(channelname):
            # channel has already been registered
            if self.LOGGING:
                self.logger.error(hostname+": Channel "+channelname+" already registered")
            return 1
        else:
            # add the channel to the registry
            self.registry[channelname] = {}
            self.registry[channelname]['port'] = port
            self.registry[channelname]['host'] = hostname
            
            if self.LOGGING:
                self.logger.info(hostname+": Registered "+channelname+" on port "+port)

        return 0

    def request(self, hostname, channelname):
        # returns hostname and port number (as a dictionary) or 1 on key error
        
        if self.LOGGING:
            self.logger.info(hostname+": Requested "+channelname)

        if self.registry.has_key(channelname):
            return self.registry[channelname]
        else:
            if self.LOGGING:
                self.logger.error(hostname+": "+channelname+" not registered")
            return 1

    def deregister(self, hostname, channelname):
        if self.LOGGING:
            self.logger.info(hostname+": Deregistered "+channelname)

        try:
            del(self.registry[channelname])
            return 0
        except:
            return 1

host = "localhost"
port = 8000
server = SimpleXMLRPCServer.SimpleXMLRPCServer(( host, port ))
server.register_instance(RegistryFunctions( host, port ))
server.serve_forever()
