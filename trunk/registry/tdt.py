# To use TDT, add the location of this file to the PYTHONPATH
# enivronment variable.
# E.g., in sh, bash or similar UNIX shells, do:
#       export PYTHONPATH=$PYTHONPATH:<directory of this file>
# For Windows, set the PYTHONPATH variable in autoexec.bat:
#       PYTHONPATH=<directory of this file>;%PYTHONPATH%

import time
import sys
import socket
import struct
import string
import random
import xmlrpclib
from xmllib import *

class TDT:
	"""
	Typed Data Transfer for Python.

	Together with the TDTDataParser, this is a complete implementation
	of the TDT library in Python. For more information on TDT (including
	copyright, authors and ownership), see
		http://www.pik-potsdam.de/~linstead

	USAGE:

	A typical client goes like this (running on hypothetical host named
	"cipc"):

	Python 2.1.3 (#1, Apr 20 2002, 10:14:34) 
	[GCC 2.95.4 20011002 (Debian prerelease)] on linux2
	Type "copyright", "credits" or "license" for more information.
	>>> from tdt import TDT
	>>> tdt = TDT ()
	>>> tdt.init ("example.xml")
	>>> # or, if the xml description is obtained from another source
	...  
	>>> xmlfile = open ("example.xml")
	>>> xmlstring = xmlfile.read ()
	>>> xmlfile.close ()
	>>> tdt.init_data_desc (xmlstring)
	>>> # for an example of such usage, see below in the implementation
	... # of the open_channel function
	... 
	>>> tdt.init_data_desc (xmlstring)
	>>> # for an example of such usage, see below in the implementation
	... # of the open_channel function
	... 
	>>> tdt.open_socket ("lt64", 2222, "w")
	Waiting for server ...
	Client connected.
	>>> # if the destination is not known, but to be taken from a          
	... # controller program, use open channel instead. For instance:
	... #
	... # ctrlhost = "pc08"
	... # ctrlport = 3333
	... # tdt.open_channel (ctrlhost, ctrlport, "w")
	... # will have the same effect if the controller responds with 
	... # "lt64" and 2222, using:
	... # tdt.send_addr ("cipc", ctrlport, "lt64", 2222)
	>>> new_double = 123.45
	>>> tdt.write (new_double, "new_double")
	>>> adict = { 'firstthing':[42, 42], 'secondthing':3.141567 }
	>>> tdt.write (adict, "astruct")
	>>> size = 5
	>>> alist = range (size)   
	>>> print alist
	[0, 1, 2, 3, 4]
	>>> tdt.write (size, "new_int")
	>>> tdt.size_array ("dyn", size)
	>>> tdt.write (alist, "dyn")
	>>> tdt.end ()
	>>> 

	The corresponding server takes the form:

	>>> from tdt import TDT
	>>> tdt = TDT ()
	>>> tdt.init ("example.xml")
	>>> tdt.open_socket ("cipc", 2222, "r")
	Server socket bound, accepting connections.
	>>> new_double = tdt.read ("new_double")
	>>> print new_double
	123.45
	>>> adict = tdt.read ("astruct")
	>>> print adict
	{'firstthing': [42, 42], 'secondthing': 3.1415670000000002}
	>>> size = tdt.read ("new_int")
	>>> print size
	5
	>>> tdt.size_array ("dyn", size)
	>>> alist = tdt.read ("dyn")
	>>> print alist
	[0.0, 1.0, 2.0, 3.0, 4.0]
	>>> tdt.end ()
	>>> 

	MAPPINGS:

		- "array" is mapped to the Python list type

		- "struct" is mapped to the Python dictionary type

		- "addr" is mapped to nothing at all and the parser will
		  complain if the xml string contains this tag

		- the primitive types have the natural Python meaning

	DIFFERENCES FROM THE C VERSION:

		- some numerical conversions are done automatically, as can be
		  seen above: the client sent integers instead of doubles and
		  all went well. This behavior would be difficult to implement
		  in C.

		- the C version does not currently implement init_desc

		- functions are methods of a TDT object and do not take a
		  TDTState argument

		- read functions construct and return the read value, instead of
		  taking a reference to an object to be "filled in"

		- there is no "addr" type in Python

		- the open_server function will only accept connections from the
		  specified host, while the C version will accept any attempt to
		  connect on the specified port

		- the python version transfers correctly between different
		  endian platforms (checking and transformation done on server
		  side)
	"""

########################USER INTERFACE FUNCTIONS########################

# The following functions represent the user interface to the module
# tdt. Users should not access any other functions. A gentle hint is
# provided by the fact that non-user level functions miss the pretty
# docstrings.

	def __init__ (self):
		"""
		Initializes the main class structures.

		These are: the read/write function dictionaries and the address
		string.

		The read/write dictionaries contain, for each type, a function that
		knows how to treat that type.

		The address strings represents the xml description of a host/port
		pair to be used in open_channel and send_addr. The alternative,
		constructing the string in a separate function or object, would have
		been slower (open_channel and send_addr are frequent operations) and
		hardly prettier.

		19/02/2003: Repeated requests on the DNS can bring down the
		network, so we need to remember the IPs of the hosts involved in
		TDT transactions. Therefore, we added a dictionary of name/ip
		pairs.
		"""

		self.writefuns = { \
			'char':self.write_char, \
			'int':self.write_int, \
			'float':self.write_float, \
			'double':self.write_double, \
			'array':self.write_array, \
			'struct':self.write_struct }
		
		self.readfuns = { \
			'char':self.read_char, \
			'int':self.read_int, \
			'float':self.read_float, \
			'double':self.read_double, \
			'array':self.read_array, \
			'struct':self.read_struct }

		self.addr_xml = \
			'<data_desc>' + \
		        '<decl name="len">int</decl>' + \
					'<decl name="hostname">' + \
					     '<array>char</array></decl>' + \
				    '<decl name="port">int</decl>' + \
			'</data_desc>'

		self.hosts = {}

		# Set up the port registry.
		# We'll hardcode the location for now.
		
		print "Connecting to port registry..."	
		self.svr = xmlrpclib.ServerProxy("http://localhost:8000")

	def config (self, filename):
		"""
		Parses the xml configuration file.
		"""

		fp = open (filename, "r")
		xmlstr = fp.read ()
		fp.close ()
		self.init_config (xmlstr)
	
	def init_config (self, xmlstring):
		"""
		Parses the configuration information in the given string.
		"""

		tdtc = TDTConfigParser ()
		tdtc.feed (xmlstring)
		tdtc.close ()
		self.config = tdtc.config
		self.config['open'] = []

	def init (self, filename):
		"""
		Parses the xml data description in the given file.
		"""

		fp = open (filename, "r")
		xmlstr = fp.read ()
		fp.close ()
		self.init_data_desc (xmlstr)

	def init_data_desc (self, xmlstring):
		"""
		Parses the xml data description in the given string.
		"""

		tdt_parser = TDTDataParser ()
		tdt_parser.feed (xmlstring)
		tdt_parser.close ()
		self.data_desc = tdt_parser.data_desc

	def open (self, connection):
		"""
		Opens the communication channel given by "connection"
		in the configuration file
		"""
		if connection in self.config["in"]:
			# connection is an input channel
			mode = "r" 
		elif connection in self.config["out"]:
			# connection is an output channel
			mode = "w" 
		else:
			raise "Channel not found in open()!"

		#print "host is", self.config[connection]["host"]
		#print "type is", self.config[connection]["type"]
		#print "datadesc is", self.config[connection]["datadesc"]
		#print "port is", self.config[connection]["port"]

		# Parse the datadesc for the given connection
		self.init (self.config[connection]["datadesc"])

		if self.config[connection]["type"] == "socket":
			#self.open_socket (self.config[connection]["host"],
			#		#int (self.config[connection]["port"]),
			#		connection,
			#		mode)
			self.open_socket (connection, mode)

	#def open_socket (self, hostname, channel, mode):
	def open_socket (self, channel, mode):
		"""
		Opens a socket for communication.

		The arguments are:
			hostname: a string representing a valid hostname or ip
			port: the port on which the communication will take place
			mode: one of "r" or "w"
		"""

		if mode == "r":
			self.open_server (channel)
		elif mode == "w":
			self.open_client (channel)
		else:
			raise "Unknown mode " + mode

	def end (self):
		"""
		Closes the open connection.

		We need to check that a channel does, in fact, exist. For a
		controller that only does send_addr, this assumption would be false.
		"""

		try:
			self.svr.deregister(socket.getfqdn(), self.config['open'][0])
			self.channel.close ()
		except:
			pass

		del self.channel


	def write (self, val, name):
		"""
		Sends data to the receiver.

		Arguments:
			val:	the object to send
			name:	the decl name of the object in the data description
		"""

		self.send (val, self.data_desc[name])

	def read (self, name):
		"""
		Reads data from the sender.

		Arguments:
			name:	the decl name of the object to receive in the data
			description
		"""

		return self.recv (self.data_desc[name])

	def size_array (self, name, size):
		"""
		Set at run-time the size of an array (list).

		The arguments are:
			name:	the decl name of the array in the data description
			size:	an integer to represent the size of the array.
		"""

		arraydesc = self.data_desc[name]
		arraydesc['size'] = str (size)

	def open_channel (self, ctrlhost, ctrlport, mode):
		"""
		Open a socket with an address obtained from a controller program.

		Arguments:
			ctrlhost:	the hostname of the controller program
			port:		the port on which to communicate with the controller
			mode:		"r" for input channels, "w" for output

		The controller will send the address of the source/target of the
		input/output operation. Subsequently, the open_socket function is
		used to open the channel in the appropriate mode.
		"""

		ctrltdt = TDT()
#		localhost = socket.gethostname ()
		ctrltdt.init_data_desc (self.addr_xml)
	#	ctrltdt.open_socket (localhost, ctrlport, "r")
		ctrltdt.open_socket (ctrlhost, ctrlport, "r")
		len = ctrltdt.read ("len")
		ctrltdt.size_array ("hostname", len)
		hostname = ctrltdt.read ("hostname")
		hostname = self.listtostring (hostname)
		port = ctrltdt.read ("port")
		ctrltdt.end ()
		self.open_socket (hostname, port, mode)

	def send_addr (self, host, port, msghost, msgport):
		"""
		Used by a controller program to send a socket address.

		Arguments:
			host:		the hostname of the program that will receive the
						address
			port:		the port for the communication with the receiving
						program
			msghost:	the host part of the address to send
			msgport:	the port part of the address to send
		"""

		tdt = TDT ()
		tdt.init_data_desc (self.addr_xml)
		tdt.open_socket (host, port, "w")
		tdt.write (len (msghost), "len")
		tdt.size_array ("hostname", len (msghost))
		tdt.write (msghost, "hostname")
		tdt.write (msgport, "port")
		tdt.end ()

####################END OF USER INTERFACE FUNCTIONS#####################

# the following two functions start up the recursive mechanism of
# sending and receiving data.

	def send (self, val, typedesc):

		func = self.writefuns[typedesc['type']]
		func (val, typedesc)

	def recv (self, typedesc):

		func = self.readfuns[typedesc['type']]
		return func (typedesc)

# open_server sets up an input socket on the given port
# the hostname argument used to check the source of the data.
# We also see if the data we receive will be in native format,
# and set the format strings accordingly

	def open_server (self, channel):

		sock = socket.socket (socket.AF_INET, socket.SOCK_STREAM)

		localhost = self.memo_gethostbyname ()
		tryno = 0

		# quick (or even slow) retries to bind a previously bound
		# port sometimes fail because the OS hasn't gotten around
		# to actually closing the port
		# to get around this, we go into a loop and print a message
		# (approx) every minute tries

		while 1:
			try:
				port = random.randint(1025,9999)
				sock.bind ((localhost, port))
				break
			except:
                #pass
				time.sleep(1)

		sys.stderr.write ("registering port %d\n" % port)
		hostname = socket.getfqdn()
		if ( self.svr.register(hostname, channel, str(port)) == 1):
			print "%s is already registered - exiting" % channel
			sys.exit(1)
            
		self.config['open'].append(channel)
		sock.listen (1)
		
		sys.stderr.write ("Server socket bound, accepting connections.\n")

		# we want to compare ip addresses to avoid dealing with
		# not fully qualified domain names
		hostname = self.memo_gethostbyname (hostname)

		# we loop, refusing connections from other hosts than hostname
		while 1:
			(self.channel, addr) = sock.accept ()

			# the following line makes sure that we have the ip address
			# of the client attempting the connection. Currently, 
			# addr[0] *is* the ip address, but the reference manual does
			# not guarantee that this will always be the case, and
			# "sicher ist sicher"
			clnthost = self.memo_gethostbyname (addr[0])
			if hostname != clnthost:
				self.channel.close ()
				del self.channel
				# the error message is easier to read if we revert
				# to names rather than ip addresses
				errstr = "Rejected connection attempt from " + \
					socket.gethostbyaddr (clnthost)[0] + " (expected " + \
					socket.gethostbyaddr (hostname)[0] + ")\n"
				sys.stderr.write (errstr)
			else:
				break

		sock.close ()

		self.set_format()

	def set_format (self):
		prefix = ''
		self.intf = 'i' # default guess
		i = self.read_int(None)
		if i != 42:
			# we will have to prefix the format string with
			# either '<' or '>' To find out, we determine the endianness
			# of our platform by packing and unpacking a known int
			i = 42
			istr = struct.pack ('<i', i)
			itup = struct.unpack ('@i', istr)
			if itup[0] == 42:
				prefix = ">"
			else:
				prefix = "<"

		self.intf    = prefix + 'i'
		self.floatf  = prefix + 'f'
		self.doublef = prefix + 'd'

	def open_client (self, channel):

		firsttry = 1

		# we loop here because of the same problem as in 
		# open_server (see above)

		# get the hostname and port number from the registry
		address = self.svr.request(socket.getfqdn(), channel)

		while address == 1:
			# retry
			address = self.svr.request(socket.getfqdn(), channel)
			time.sleep (1)
			
		port = int (address['port'])
		print "Got port %d" % port
		hostname = address['host']
		
		while 1:
			try:
				self.channel = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
				
				# now connect...
				self.channel.connect ((hostname, port))
				break
			except:
				self.channel.close ()
				if firsttry == 1:
					sys.stderr.write ("Waiting for server ...\n")
					firsttry = 0

		sys.stderr.write ("Client connected.\n")

		# send the agreed int for the server to decide the format
		# strings
		self.write_int (42, None)

# The following 6 read/write pairs treat the corresponding data types.
# As opposed to the C version, the read functions have to actually
# construct a value of that type and return it (no pointers in Python).

	def write_array (self, val, typedesc):

		size = string.atoi (typedesc['size'])

		for i in range (size):
			self.send (val[i], typedesc['value'])

	def read_array (self, typedesc):

		size = string.atoi (typedesc['size'])

		arrayval = []
		for i in range (size):
			arrayval.append (self.recv (typedesc['value']))

		return arrayval

	def write_struct (self, val, typedesc):

		decls = typedesc['value']
		names = decls[0] # see below, in TDTDataParser.decl_start
		for name in names:
			self.send (val[name], decls[name])

	def read_struct (self, typedesc):

		decls = typedesc['value']
		names = decls[0] # see below, in TDTDataParser.decl_start
		structval = {}
		for name in names:
			structval[name] = self.recv (decls[name])

		return structval
		
# For why we use __recvall in reading primitives, see below.

	def write_int (self, val, typedesc):

		self.channel.sendall (struct.pack ('i', val))

	def read_int (self, typedesc):

		intstr = self.__recvall (struct.calcsize ('i'))
		inttup = struct.unpack (self.intf, intstr)
		return inttup[0]

	def write_float (self, val, typedesc):

		self.channel.sendall (struct.pack ('f', val))

	def read_float (self, typedesc):

		floatstr = self.__recvall (struct.calcsize ('f'))
		floattup = struct.unpack (self.floatf, floatstr)
		return floattup[0]

	def write_double (self, val, typedesc):

		self.channel.sendall (struct.pack ('d', val))

	def read_double (self, typedesc):

		doublestr = self.__recvall (struct.calcsize ('d'))
		doubletup = struct.unpack (self.doublef, doublestr)
		return doubletup[0]

	def write_char (self, val, typedesc):

		self.channel.sendall (val)

	def read_char (self, typedesc):
		
		# The size of a char is always 1, and there is nothing to unpack

		c = self.__recvall (1) 
		return c 

# __recvall is equivalent to calling socket.recv with MSG_WAITALL as a
# parameter. However, there's no MSG_WAITALL option in MS Windows,
# which, at the moment, is still in use in some circles. 

	def __recvall (self, size):

		str   = ""
		bytes = 0

		while bytes < size:
			str += self.channel.recv (size-bytes)
			bytes += len (str)

		return str


	def listtostring (self, charlist):
		s = ""
		for i in range (len (charlist)):
			s += charlist [i]

		return s
	
	# We need to use a memoized version of gethostbyname, otherwise the
	# repeated requests on the DNS server will bring down the network.

	def memo_gethostbyname (self, hostname=None):
		# if the hostname is not in the self.hosts dictionary, we need
		# to add it
		if not self.hosts.has_key (hostname):
			if hostname is None:
				self.hosts[None] = socket.gethostname ()
			else:
				self.hosts[hostname] = socket.gethostbyname(hostname)

		return self.hosts[hostname]



class TDTDataParser (XMLParser):
	"""
	Class to parse TDT data descriptions.

	Usage:

	ionescu@cipc:~/tdtcvs/python$ python
	Python 2.1.3 (#1, Apr 20 2002, 10:14:34) 
	[GCC 2.95.4 20011002 (Debian prerelease)] on linux2
	Type "copyright", "credits" or "license" for more information.
	>>> from tdt import *
	>>> tdtp = TDTDataParser()
	>>> fp = open ("example.xml")
	>>> tdtp.feed (fp.read ())
	>>> print tdtp.data_desc
	{'new_int': {'type': 'int'}, 'dyn': {'size': '0', 'value': {'type':
	'double'}, 'type': 'array'}, 'astruct': {'value': {'secondthing':
	{'type': 'double'}, 'firstthing': {'size': '2', 'value': {'type':
	'int'}, 'type': 'array'}}, 'type': 'struct'}, 'new_double': {'type':
	'double'}}
	>>> 

	or

	$ python tdt.py <xmlfile>

	The result of parsing the xml string is a dictionary of decls.
	Each key is the name of a decl, and the corresponding value is a
	dictionary describing the type of the decl in the following way:
		for primitive types:
			key: 'type' value: 'none'
		for struct types:
			key: 'type' value: 'struct'
			key: 'value' value: a dictionary of the decls in the struct
		for array types:
			key: 'type' value: 'array'
			key: 'size' value: the size given in the xml or zero
			key: 'value' value: a dictionary describing the type of the
				array

	Methods:
		A handler for the start and end of each tag, and a handler for
		the xml data ('int', 'double', etc.). 

	Members:
		data_desc: the dictionary described above.

	Algorithm:
		The main idea is that we keep a running list of dictionaries to
		be built, each dictionary containing a reference to the next and
		being referenced by the previous one (except for the obvious
		limit cases). The last dictionary in the list is the current
		dictionay that is being built. An opening tag complets the
		'type' field of the current dictionary, possibly also some
		attributes such as "size" in the case of arrays, initializes an
		empty dictionary, makes the current dictionary reference this
		empty dictionary as a "value" field, and then adds the empty
		dictionary to the list, thereby making it "current" for the next
		opening tags. A closing tag pops (deletes) the last dictionary
		in the list, restoring the status of the current dictionary for
		the next tag. The data handler adds a "type" key to the current
		dictionary with the value being the given name of the primitive
		type. It neither adds to nor deletes from the list of
		dictionaries.  
	"""

	def data_desc_start (self, data):
		"""
		Handles the "data_desc" start tag.

		The data argument contains the attributes of the tag (not used
		at the moment).
		The function introduces the first dictionary to be built.
		The dictionary contains only one key:value pair, the explanation
		of which is given in decl_start.
		"""

		self.data_desc.append({0:[]})

	def data_desc_end (self):
		"""
		Handles the closing "data_desc" tag.

		Since this should be the last tag in the xml string, there is
		only one dictionary in the list, which contains the full data
		description.
		"""

		self.data_desc = self.data_desc.pop ()

	def decl_start(self, data):
		"""
		Handles the opening "decl" tag.

		Introduces a key:value pair in the current dictionary. The key
		is the name of the decl, as given by the 'name' attribute of the
		data argument. The value is an empty dictionary, which is then
		made the current dictionary by adding it to the data_desc list.

		Since dictionaries are arranged in arbitrary order, it becomes
		impossible to transfer e.g. "struct" types in one go. To work
		around that, we need to remember somehow the order in which
		decls are parsed. The solution we chose is to add to the
		dictionaries constructed by decls a list to which each decl will
		add the name it receives in the data. The list is initialized by
		the handlers of those tags that may have nested 'decl' tags, at
		the moment 'data_desc' and 'struct'. The key under which the
		list will be found has to guarantee that no addition by the decl
		handlers will give the same key and overwrite the list. We have
		chosen the key to be the integer 0, since all keys added by all
		handlers in TDTDataParser are, have been, and will allways be
		strings (and of course, 0, being an integer, is different from
		any string, even from '0'). 

		It really is much easier to understand than to explain.
		"""

		current_dict = self.data_desc[-1]
		current_dict[data['name']] = {}
		current_dict[0].append (data['name'])
		self.data_desc.append (current_dict[data['name']])

	def decl_end(self):
		"""
		Handles the closing "decl" tag.

		Restores the current dictionary by popping (deleting) the one
		that the starting tag had added.
		"""

		self.data_desc.pop()

	def array_start (self, data):
		"""
		Handles the opening "array" tag.

		Adds three key:value pairs to the current dictionary. The first
		is 'type':'array', the second is 'size':the given size (taken
		from the 'size' element of the data argument, or zero if there
		is no size specified in the xml). The third is 'value':an empty
		dictionary. This empty dictionary is then made the current
		dictionary by adding it to the data_desc list.
		"""

		current_dict = self.data_desc[-1]
		current_dict['type'] = 'array'
		if data.has_key ('size'):
			current_dict['size'] = data['size']
		else:
			current_dict['size'] = '0'
		current_dict['value'] = {}
		self.data_desc.append (current_dict['value'])

	def array_end (self):
		"""
		Handles the closing "array" tag.

		Restores the current dictionary by removing the dictionary that
		had been adding by the opening tag.
		"""

		self.data_desc.pop ()

	def struct_start (self, data):
		"""
		Handles the opening "struct" tag.

		Adds two key:value pairs to the current dictionary. The first is
		'type':'struct'. The second is 'value':empty dictionary. This
		empty dictionary is then made current by adding it to the
		data_desc list. The initial element of the current dictionary is
		explained above, in decl_start.
		"""

		current_dict = self.data_desc[-1]
		current_dict['type'] = 'struct'
		current_dict['value'] = {0:[]}
		self.data_desc.append (current_dict['value'])

	def struct_end (self):
		"""
		Handles the closing "struct" tag.

		Restores the current dictionary by removing the one added by the
		opening tag.
		"""

		self.data_desc.pop ()

	def handle_data (self, data):
		"""
		Handles the data in the given xml.

		This function overrides the handle_data method in the superclass.
		The data is contained in the "data" argument.
		It avoids newlines or empty strings by stripping the data first.
		It then adds a key:value pair to the current dictionary,
		consisting of 'type':the name of the data.

		CAVEAT: as opposed to the "C" version, it does not handle well
		names that have spaces in them, such as "i N t". It only handles
		mixed case.
		"""

		data = string.strip (data)
		if data == "":
			return
		data = string.lower (data)
		current_dict = self.data_desc[-1]
		current_dict['type'] = data

	def unknown_starttag (self, tag, attrs):
		"""
		Raises an exception if the xml contains unknown tags.

		The xml should only contain the tags we know how to parse.
		"""
		raise "Unknown start tag ", tag

	def unknown_endtag (self, tag, attrs):
		"""
		Raises an exception if the xml contains unknown tags.

		The xml should only contain the tags we know how to parse.
		"""
		raise "Unknown start tag %s", tag


	def __init__ (self):
		"""The constructor of the TDTDataParser.

		Besides calling the constructor of the superclass, __init__
		initializes the handlers for the tags. It also sets up the list
		of dictionaries (initially empty) that will be built into the
		final data_desc.
		"""

		apply (XMLParser.__init__, (self,), {'map_case':1})
		self.elements['decl']=(self.decl_start, self.decl_end)
		self.elements['data_desc']=(self.data_desc_start, self.data_desc_end)
		self.elements['array']=(self.array_start, self.array_end)
		self.elements['struct']=(self.struct_start, self.struct_end)
		self.data_desc = []


class TDTConfigParser (XMLParser):
	"""
	Class to parse TDT configuration files.

	Based on Cezar's TDTDataParser class in tdt.py

	Usage:

	ionescu@cipc:~/tdtcvs/python$ python
	Python 2.1.3 (#1, Apr 20 2002, 10:14:34) 
	[GCC 2.95.4 20011002 (Debian prerelease)] on linux2
	Type "copyright", "credits" or "license" for more information.
	>>> from tdt import *
	>>> tdtc = TDTConfigParser()
	>>> fp = open ("clntconf.xml")
	>>> tdtc.feed (fp.read ())
	>>> print tdtc.config
	>>> 
	"""

	def program_start (self, data):
		"""
		Handles the "program" start tag.

		"""
		self.progname = data['name']
		self.config['in']  = []
		self.config['out'] = []

	def program_end (self):
		"""
		Handles the closing "program" tag.

		"""

	def channel_start (self, data):
		"""
		Handles the opening "channel" tag.

		"""
		chname = data['name']

		# Get the attributes for this channel, add to dictionary
      
		self.config[chname] = {}
		for elem in data:
			self.config[chname][elem] = data[elem]
      
		# Check if we're doing an in- or output channel
		if data["mode"] == "in":
			self.config['in'].append(chname)
		elif data["mode"] == "out":
			self.config['out'].append(chname)

	def channel_end (self):
		"""
		Handles the closing "array" tag.

		Restores the current dictionary by removing the dictionary that
		had been adding by the opening tag.
		"""

	def handle_data (self, data):
		"""
		Handles the data in the given xml.

		This function overrides the handle_data method in the superclass.
		The data is contained in the "data" argument.
		It avoids newlines or empty strings by stripping the data first.
		It then adds a key:value pair to the current dictionary,
		consisting of 'type':the name of the data.

		CAVEAT: as opposed to the "C" version, it does not handle well
		names that have spaces in them, such as "i N t". It only handles
		mixed case.
		"""

	def unknown_starttag (self, tag, attrs):
		"""
		Raises an exception if the xml contains unknown tags.

		The xml should only contain the tags we know how to parse.
		"""
		raise "Unknown start tag ", tag

	def unknown_endtag (self, tag, attrs):
		"""
		Raises an exception if the xml contains unknown tags.

		The xml should only contain the tags we know how to parse.
		"""
		raise "Unknown end tag %s", tag


	def __init__ (self):
		"""The constructor of the TDTConfigParser.

		Besides calling the constructor of the superclass, __init__
		initializes the handlers for the tags. It also sets up the list
		of dictionaries (initially empty) that will be built into the
		final config.
		"""

		XMLParser.__init__(*(self,), **{'map_case' : 1})

		self.elements['program']=(self.program_start, self.program_end)
		self.elements['channel']=(self.channel_start, self.channel_end)

		self.config = {}

if __name__ == "__main__":
	import sys

	fname =  sys.argv[1]

	fp = open (fname)
	xmlstring = fp.read ()
	fp.close ()
	#tdtp = TDTDataParser ()
	tdtp = TDTConfigParser ()
	tdtp.feed (xmlstring)
	tdtp.close ()
	#print tdtp.data_desc
	print tdtp.config
