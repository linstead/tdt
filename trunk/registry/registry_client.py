import sys
import xmlrpclib
import socket

print "creating a server instance"

try:
    svr = xmlrpclib.ServerProxy("http://localhost:8000")
    print "Got a server"
except:
    sys.exit("no registry found")

print "registering channel 1"
try:
    svr.register(socket.getfqdn(), "channel1","1234")
except:
    sys.exit("no registry found")

print "registering channel 2"
svr.register(socket.getfqdn(), "channel2","5678")

print "requesting channel 1"
svr.request(socket.getfqdn(), "channel1")

print "requesting channel 2"
svr.request(socket.getfqdn(), "channel2")

print "deregistering channel1"
svr.deregister(socket.getfqdn(), "channel1")

print "deregistering channel2"
svr.deregister(socket.getfqdn(), "channel2")

print "Now requesting channel 1 again"
svr.request(socket.getfqdn(), "channel1")
