class tdtServer {
    public static void main(String[] args) {

        int[][] values = new int[6][2];
        double[][][][] dvalues = new double[10][10][10][10];
	char[] cvalues=new char[10];
	char ch;
	double d;
        

        int tc;
        int ts;
        jTDT tdt = new jTDT();
        tc = tdt.config("serv.xml");
        ts = tdt.open(tc, "clnt_to_serv");
	tdt.tdt_size_multiarray (ts ,"dataarray_d", 2, 10);


	tdt.read (ts, values, "dataarray");
	tdt.read (ts, dvalues, "dataarray_d");
	tdt.read (ts, cvalues, "chararray");
	ch = tdt.read_char (ts);
	d = tdt.read_double(ts);

        System.out.println("java values: " + values[1][0]);
        
        System.out.println("java dvalues: " + dvalues[7][2][4][9]);
        System.out.println("java cvalues[9]: " + cvalues[9]);
	System.out.println("ch:" + ch);
	System.out.println("d:" + d);

        tdt.close(ts);
        tdt.end(tc);
    }
}
