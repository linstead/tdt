public class jTDT {

    public int tdtconf;
    public int tdtstate;

    public native int  config (String confname);
    public native int  open   (int tdtconf, String cname);

  
    public native void read   (int tdtstate, Object values, String name);
    public native void write  (int tdtstate, Object values, String name);

  
    public native void close  (int tdtstate);
    public native void end    (int tdtconfig);

    public native char read_char   (int tdtstate);
    public native int read_int   (int tdtstate);
    public native float read_float   (int tdtstate);
    public native double read_double   (int tdtstate);

    public native void write_char  (int tdtstate, char val);
    public native void write_int  (int tdtstate, int val);
    public native void write_float  (int tdtstate, float val);
    public native void write_double  (int tdtstate, double val);

//    public native void read_string   (int tdtstate, Object values, String name);
//    public native void write_string  (int tdtstate, Object values, String name);
	    
    public native void tdt_size_multiarray (int tdtstate, String name, int rank, int size);
    static {
        System.loadLibrary("jtdt");
    }

}
