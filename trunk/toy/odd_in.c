/* Buffer for the eval program. */
#include <stdio.h>

#include "tdt.h"
#include "tdt_extra.h"

int
main (int argc, char **argv) {

	int n;

	TDTState ts_in; /* input channel */
	FILE *fp;
	char *ctrlhost = "pc08";
	int ctrlport = 2226;

	ts_in = tdt_init ("inodd.xml");

	ts_in = tdt_open_channel (ts_in, ctrlhost, ctrlport, READ);
	if (ts_in == NULL) {
		tdt_error ("tdt_open_channel returned NULL");
		return 1;
	}

	tdt_read (ts_in, &n, "inodd");
	tdt_end (ts_in);

	fp =  fopen ("inodd", "w");
	fprintf (fp, "%d", n);
	fclose (fp);

	return 0;
}
