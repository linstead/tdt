The programs in this directory communicate via files in a way similar
to the data flow in PIAM.

The example implemented is the Collatz conjecture. Take an integer
n. If n is even, divide it by two. If n is odd, multiply it by three
and add one. Repeat until you get 1. Collatz conjectured that this
process always terminates.

The programs are:

EVAL:
	input: n, a file containing an integer
	output: res, a file containing an integer which is 0 if n
		contains 1, 1 if n contains an odd number and 2 if n
		contains an even number
		
		inodd, a file containing the same number as n if n is
		odd

		ineven, a file containing the same number as n if n is
		even
	remarks: EVAL will output res and one of inodd and ineven.

EVEN:
	input: ineven, a file containing an even number
	output: n, a file containing the result of dividing the number
		in ineven by two

ODD:
	input: inodd, a file containing an odd number
	output: n, a file containing the result of multiplying the
		number in inodd by three and adding one to it

The programs have to be run in the following manner:

initial setup: put the number to be processed in file n
do
	run EVAL
	read res
	if res contains 0:
	   stop
	else:
	   if res contains 1:
	      run ODD
	   else:
	      run EVEN	