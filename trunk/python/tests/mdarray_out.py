from tdt import TDT

tdt = TDT ()

tdt.config ("mdarray_out.xml")

tdt.open ("conn1")

# firstthing is a 2x5 Fortran array, ie 2 rows of 5 columns
firstthing = [[11, 12, 13, 14, 15], [21, 22, 23, 24, 25]]
print "Sending matrix: ", firstthing

tdt.write (firstthing, "a")

tdt.end ()
