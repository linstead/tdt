from tdt import TDT

tdt = TDT ()

tdt.config ("servconf.xml")
tdt.open ("input1")

d = tdt.read ("new_double")
astruct = tdt.read ("astruct")
i = tdt.read ("new_int")

# now use new_int as the size of the dynamic array:
tdt.size_array ("dyn", i)

dyn = tdt.read ("dyn")

print 'd = ', str (d)
print 'astruct = ', repr (astruct)
print 'i = ', str (i)
print dyn

tdt.end ()
