                SUBROUTINE tdt_fconfigure (tdtconf, confname)
                INTEGER tdtconf
                CHARACTER *(*) confname

                CALL tdt_fconfigure_internal (tdtconf, confname,
     *                                          LEN_TRIM (confname))

                END


                SUBROUTINE tdt_finit (tdtstate, tdtconf, conn)
                INTEGER tdtstate
                INTEGER tdtconf
                CHARACTER *(*) conn

                CALL tdt_finit_internal (tdtstate, tdtconf, conn, 
     *                                                 LEN_TRIM (conn))

                END

                SUBROUTINE tdt_fopen (tdtstate, tdtconf, conn)
                INTEGER tdtstate
                INTEGER tdtconf
                CHARACTER *(*) conn

                CALL tdt_fopen_internal (tdtstate, tdtconf, conn,
     *                                  LEN_TRIM (conn))

                END

                SUBROUTINE tdt_fread (tdtstate, name, val) 
                INTEGER tdtstate
                INTEGER val
                CHARACTER *(*) name

                CALL tdt_fread_internal (tdtstate, val, 
     *                                  name, LEN_TRIM (name))

                END

                SUBROUTINE tdt_fwrite (tdtstate, name, val) 
                INTEGER tdtstate
                INTEGER val
                CHARACTER *(*) name

                CALL tdt_fwrite_internal (tdtstate, val, 
     *                                  name, LEN_TRIM (name))

                END

                SUBROUTINE tdt_fclose (tdtstate)
                INTEGER tdtstate

                CALL tdt_fclose_internal (tdtstate)

                END

                SUBROUTINE tdt_fend (tdtconf)
                INTEGER tdtconf

                CALL tdt_fend_internal (tdtconf)

                END

                SUBROUTINE tdt_fsize_multiarray (tdtstate, name, 
     *                                          rank, size)
                INTEGER tdtstate
                CHARACTER *(*) name
                INTEGER rank
                INTEGER size

                CALL tdt_fsize_multiarray_internal (tdtstate, name, 
     *                         LEN_TRIM (name),rank, size)

                END

