                PROGRAM fserv
                INTEGER:: firstthing(2,5)
                 REAL*8:: secondthing(100,15,100,100)
                INTEGER tdtstate1
C                INTEGER tdtstate2
                INTEGER tdtconf

                CALL tdt_fconfigure (tdtconf, 'mdarray_in.xml')

                CALL tdt_fopen (tdtstate1, tdtconf, 'conn1')

                CALL tdt_fread (tdtstate1, 'a', firstthing)

                CALL tdt_fclose (tdtstate1)

C               Print the matrix:
                PRINT *,'firsthing(2,5)='
                do i = 1, 2
C               '(5I5)' means 5 integers of width 5
                   write (*,'(5I5)') (firstthing(i,j), j=1,5)
                end do

C                PRINT *, 'I read:'
C                DO k = 1, 2
C                        DO l = 1,5
C                        PRINT *, firstthing (k,l)
C                        END DO
C                END DO

                CALL tdt_fend (tdtconf)

                END
