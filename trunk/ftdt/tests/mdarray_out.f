                PROGRAM fclnt
                INTEGER:: firstthing(2,5)
                INTEGER tdtstate1
                INTEGER tdtconf

                DO k = 1,2
                   DO l = 1,5
                      firstthing(k,l) = k*10 + l
                   END DO
                END DO

C               Print the matrix:
                PRINT *,'firsthing(2,5)='
                do i = 1, 2
C               '(5I5)' means 5 integers of width 5
                   write (*,'(5I5)') (firstthing(i,j), j=1,5)
                end do

                CALL tdt_fconfigure (tdtconf, 'mdarray_out.xml')

                CALL tdt_fopen (tdtstate1, tdtconf, 'conn1')
                CALL tdt_fwrite (tdtstate1, 'a', firstthing)

                CALL tdt_fclose (tdtstate1)
                CALL tdt_fend (tdtconf)


                END
                
