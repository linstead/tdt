<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Author: Ciaron Linstead, 22 June 2004 
     This stylesheet is intended for formatting TDT configuration files for
     output in a browser. Include the declaration: 
     <?xml-stylesheet type="text/xsl" href="configure.xsl"?> 
     underneath the main XML declaration in a TDT configuration file and open
     the XML document in a browser which supports the official W3C XSLT
     Recommendation. 
-->

<!-- The main template, matches the whole document -->

<xsl:template match="/">
  <html>
  <body>
    <h1>TDT Configuration</h1> 
    <xsl:apply-templates/> 
  </body>
  </html>
</xsl:template>

<!-- This template matches the document level tag. We get the
     name of the program from its attribute "name"  
-->

<xsl:template match="program">
    <p><h2>Program Name: <xsl:value-of select="@name" /></h2></p>
    <p><h3>Channels:</h3></p>
    <xsl:choose>
        <xsl:when test="child::channel">
            <table border="1">
            <xsl:call-template name="printheader"/>
            <xsl:for-each select="channel">
                <xsl:call-template name="printrow"/>
            </xsl:for-each>
            </table>
        </xsl:when>

        <xsl:otherwise>No channels.</xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!-- This is a "call" template. It doesn't match anything, but we
     will be calling it later. It prints a HTML table header row. 
-->

<xsl:template name="printheader" >
  <tr bgcolor="#9acd32">
    <th align="left">Channel Name</th> 
    <th align="left">Channel Mode</th> 
    <th align="left">Channel Type</th> 
    <th align="left">Channel Datadesc</th> 
    <th align="left">Channel Host</th> 
    <th align="left">Channel Port</th> 
    <th align="left">Channel Filename</th> 
  </tr>
</xsl:template>

<!-- This is another call template. It doesn't match anything, but we
     will be calling it later. Notice the use of "param". We can pass 
     in whether this is an input channel or an output channel.
     We also do conditional stuff, like putting an "NA" for hostname
     if the channel type is a file, etc.
-->

<xsl:template name="printrow" >
  <tr>
    <td><xsl:value-of select="@name" /></td>
    <td><xsl:value-of select="@mode" /></td>
    <td><xsl:value-of select="@type" /></td>
    <td><xsl:value-of select="@datadesc" /></td>
    <xsl:if test="@type='socket'">
      <td><xsl:value-of select="@host" /></td>
      <td><xsl:value-of select="@port" /></td>
      <td>NA</td>
    </xsl:if>
    <xsl:if test="@type='file'">
      <td>NA</td>
      <td>NA</td>
      <td><xsl:value-of select="@filename" /></td>
    </xsl:if>
    </tr>
</xsl:template>
</xsl:stylesheet>
