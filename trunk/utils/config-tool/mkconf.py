import tdtconftools as tct
import sys

"""
Generate a TDT configuration file from command line arguments

Usage example:

python mkconf.py --create "testing" -I name="test",host=pc115,port=2000
"""

def main():

    args = tct.getargs()

    # Do the actions
    if args['action'] == 'create':
        config = tct.create_config (args['name'], args['channels'])

    elif args['action'] == 'add':
        if args['inputfile'] == None:
            raise tct.actionError
            sys.exit()
        else: 
            config = tct.parse_config (args['inputfile'])
            config = tct.add_channels (config, args['channels'])

    elif args['action'] == 'delete':
        if args['inputfile'] == None:
            raise actionError
            sys.exit()
        else: 
            config = tct.parse_config (args['inputfile'])
            config = tct.delete_channel (config, args['channel'])

    elif args['action'] == 'rename':
        if args['inputfile'] == 'None':
            raise actionError
            sys.exit()
        else:
            config = tct.parse_config (args['inputfile'])
            config = tct.rename_config(config, args['name'])

    else:
        raise tct.actionError

    tct.write_config(config, args['outputfile'])

if __name__ == "__main__":

    main()

