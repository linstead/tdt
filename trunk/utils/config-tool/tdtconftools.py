import getopt
import sys
import fileinput

from tdt import TDTConfigParser

class actionError(Exception):
    def __init__(self):
        sys.stderr.write("You must specify one (only) of --create, --add, --rename  or --delete")
        usage()
        sys.exit()

def usage():
    """ Print a usage message """

    mesg = """
Usage: python mkconf [-c --create "program name"  |
                      -a --add    |
                      -d --delete "channelname" |
                      -r --rename  "program name"]
       [-I --inputchannel  channel_spec ...]
       [-O --outputchannel channel_spec ...]
       [-i --infile  "infile"]
       [-o --outfile "outfile"]
       [-h --help]\n\n"""

    sys.stderr.write(mesg)

def create_config (name, channels):
    """ Create a config dictionary. This will create an empty (i.e. no
        "channel"s) config if the inchannels and outchannels are empty, otherwise it
        will call add_channel() the appropriate number of times.
       
        Input arguments
            name : (string) Value for the "name=" attribute of the "program" tag
            channels : (dictionary) Two lists of channel arguments
                e.g. {'out': [], 'in': ['name=test,type=socket,port=2222,host=localhost']}

        Returns
            A completed config dictionary
    """

    config = {name:{'in':[], 'out':[]}}

    if channels['in']==[] and channels['out']==[]:
        pass 
    else:
        add_channels (config, channels)

    return config

def insert(config, channels):
    """ Inserts a list of channels into a config dictionary

        Input arguments
            config : (dictionary) The configuration into which channel data will be inserted.
        Returns
            config updated
    """

    allowed = ['name', 'type','host','port','datadesc','filename']

    # we only expect one program per config, this is how we get its name:
    program = config.keys()[0]

    # add the channels to the config dictionary...
    for mode in channels.keys():

        if channels[mode] == []:
            pass
        else:
            for channel in channels[mode]:

                # initialise the dictionary for this channel
                config[program][channel['name']] = {}

                # Give it a 'name' element
                config[program][mode].append(channel['name'])
                
                # add all the channel attributes that have been given
                for elem in channel.keys():
                    # add item to dictionary, if it's not "name" 
                    # (that's already part of the config data)
                    if elem != 'name':
                        if elem in allowed: 
                            config[program][channel['name']][elem] = channel[elem]
                        else:
                            sys.stderr.write("No such attribute: " + elem + ". Please check and try again.\n")
                            sys.exit()

    return config

def add_channels (config, channels):
    """ Takes lists of input and/or output channels as they appear on the command line
         and adds them to the config dictionary
    """

    if channels['in']==[] and channels['out']==[]:
        sys.stderr.write("no chnl data pls fx ok thx")
        raise actionError
        sys.exit()

    # create a dictionary for the new channels
    # channels_data will hold a list of dictionaries of attribute/value pairs
    # for each channel to be created

    # first get extract the channel data from the command line

    channel_data = {}
    channel_data['in']=[]
    channel_data['out']=[]

    for mode in channels.keys():
        for i in channels[mode]:
            args = []
            args = i.split(",")
            args_dict = {}
            for j in args:
                tmp = j.split("=")
                args_dict[tmp[0]] = tmp[1]
    
            channel_data[mode].append(args_dict)
    
    config = insert(config, channel_data)
    
    return config

def delete_channel (config, channelname):

    # we only expect one program per config, this is how we get its name:
    program = config.keys()[0]

    # See if the channel exists in the config
    if config[program].has_key(channelname):
        pass 
    else:
        print "Channel " + channelname + " not found."
        sys.exit()

    # remove from the channels index list in the config 
    if channelname in config[program]['in']:
        config[program]['in'].remove(channelname)
    elif channelname in config[program]['out']:
        config[program]['out'].remove(channelname)

    # and remove from the config itself
    del config[program][channelname] 

    return config

def rename_config (config, newname):
    
    name = config.keys()[0]

    config[newname] = config[name]
    del config[name]

    return config

def parse_config (inputfile):
    """
    Parse an existing configuration file, e.g. when adding a new
    channel
    """

    fname = inputfile 
    fp = open (fname)
    xmlstring = fp.read ()
    fp.close ()
    tdtc = TDTConfigParser ()
    tdtc.feed (xmlstring)
    tdtc.close ()

    config={tdtc.progname: tdtc.config}
    return config

def write_config(config, outfile):

    if outfile == None:
        # we'll be outputting on standard output then...
        f = sys.stdout
    else:
        # we'll use the output file we've been given
        f = open(outfile, 'w')

    # Write the XML header
    f.write("""<?xml version="1.0" encoding="ISO-8859-1"?>\n""")
    f.write("""<?xml-stylesheet type="text/xsl" href="configure.xsl"?>\n""")
    f.write("\n")
    f.write("""<!--\n""")
    f.write("""This file was automatically generated by the TDT mkconf utility\n""")
    f.write("\n")
    f.write("""See README.mkconf for more information and help\n""")
    f.write("""See README.config for more information about the format of this file\n""")
    f.write("""-->\n""")
    f.write("\n")

    # only expecting one program per config file
    name = config.keys()[0]

    # opening tag
    f.write("<program name=\""+name+"\">\n")
    
    for mode in ['in','out']:

        for channel in config[name][mode]:

            # write the channel attributes. Note that not all attributes have to be present
            f.write("    <channel name=\"" + channel + "\"")
            f.write("\n             mode=\"" + mode + "\"")

            # the order the attributes appear in the output is indeterminate
            for attr in config[name][channel].keys():
                if attr != 'name':
                    f.write("\n             "+ attr + "=\"" + config[name][channel][attr] + "\"")

            f.write(">\n")
            f.write("    </channel>\n")

    # close the document tag
    f.write("</program>\n")
    f.close()

def getargs():

    try:
        opts, args = getopt.getopt(sys.argv[1:], "c:ad:n:r:I:O:i:o:h", 
                    ["create=", "add", "delete=",
                     "rename=", "inchannel=","outchannel=",
                     "input=","output=","help"])

    except getopt.GetoptError:
        # print help information and exit:
        usage()
        sys.exit(2)

    inputfile = None
    outputfile = None
    action = None
    name = None
    channel = None

    channels={}
    channels['in'] = []
    channels['out'] = []

    for o, a in opts:

        # TESTING: print out the options/arguments
        #print "option:", o, "argument:", a

        # ACTIONS
        if o in ("-c", "--create"):
            if action==None:
                action = "create"
                name = a
            else:
                raise actionError

        if o in ("-a", "--add"):
            if action==None:
                action = "add"
            else:
                raise actionError

        if o in ("-d", "--delete"):
            # delete requires channelname and infile

            if action==None:
                action = "delete"
                channel = a
            else:
                raise actionError

        if o in ("-r", "--rename"):
            # delete requires infile

            if action==None:
                action = "rename"
                name = a
            else:
                raise actionError

        if action==None:
            raise actionError

        # CHANNELS
        if o in ("-I", "--inchannel"):
            channels['in'].append(a)
        if o in ("-O", "--outchannel"):
            channels['out'].append(a)


        # INPUT AND OUTPUT
        if o in ("-i", "--input"):
            inputfile = a
        if o in ("-o", "--output"):
            outputfile = a

        # HELP
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    argd = {}
    argd['action'] = action
    argd['channels'] = channels
    argd['inputfile'] = inputfile
    argd['outputfile'] = outputfile
    argd['name'] = name
    argd['channel'] = channel

    return argd

def main():

    args = getargs()

    # Do the actions
    if args['action'] == 'create':
        config = create_config (args['name'], args['channels'])

    elif args['action'] == 'add':
        if args['inputfile'] == None:
            raise actionError
            sys.exit()
        else: 
            config = parse_config (args['inputfile'])
            config = add_channels (config, args['channels'])

    elif args['action'] == 'delete':
        if args['inputfile'] == None:
            raise actionError
            sys.exit()
        else: 
            config = parse_config (args['inputfile'])
            config = delete_channel (config, args['channel'])

    elif args['action'] == 'rename':
        if args['inputfile'] == 'None':
            raise actionError
            sys.exit()
        else:
            config = parse_config (args['inputfile'])
            config = rename_config(config, args['name'])

    else:
        raise actionError

    write_config(config, args['outputfile'])

if __name__ == "__main__":

    main()

