#ifndef lint
/*static char yysccsid[] = "from: @(#)yaccpar	1.9 (Berkeley) 02/21/93";*/
static char yyrcsid[] = "$Id$";
#endif
#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define yyclearin (yychar=(-1))
#define yyerrok (yyerrflag=0)
#define YYRECOVERING (yyerrflag!=0)
#define YYPREFIX "yy"
#line 2 "cgram.y"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "typeops.h"
#include "xmlhandlers.h"
#include "defs.h"
#include "model2xml.h"
DataDesc   md;

typedef struct decls1 {
	Decl* decls;
	int nd;
}* Decls;

typedef struct comp1 {
	TypeDesc td;
	char *name;
}* Comp;

typedef struct multicomp1 {
	Comp *comps;
	int ncomps;
}* Multicomp;

extern char num;
extern int level;
extern char *name;
extern int line;
int array_size;
int tabs=0;
char *aux;

char *cat(), *ds();
TypeDesc join(TypeDesc, TypeDesc);
#line 41 "cgram.y"
typedef union {
	TypeDesc typedesc;
	ArrayDesc ar;
	AddrDesc ad;
	StructDesc structdesc;
	Decl decl;
	Decls decls;
	DataDesc md;
	Multicomp multicomp;
	Comp comp;
	int num;
} YYSTYPE;
#line 62 "y.tab.c"
#define STRUCT 257
#define CHAR 258
#define INT 259
#define NAME 260
#define DOUBLE 261
#define FLOAT 262
#define UNKNOWN 263
#define NUMBER 264
#define YYERRCODE 256
short yylhs[] = {                                        -1,
    0,    1,    1,    1,    1,    2,    7,    3,    3,    3,
    3,    3,   10,    6,    6,    8,    8,    4,    4,    5,
    9,    9,
};
short yylen[] = {                                         2,
    1,    1,    1,    2,    2,    3,    3,    1,    1,    1,
    1,    1,    4,    3,    3,    1,    2,    1,    2,    3,
    1,    2,
};
short yydefred[] = {                                      0,
    0,    9,   10,   12,   11,    0,    1,    0,    0,    0,
    8,    0,    5,   21,    0,    0,    0,    0,    0,    4,
    0,    0,   19,   17,    6,    7,    0,   22,   13,    0,
   15,    0,   20,
};
short yydgoto[] = {                                       6,
    7,    8,    9,   16,   23,   17,   10,   18,   19,   11,
};
short yysindex[] = {                                   -250,
 -113,    0,    0,    0,    0,    0,    0, -250,  -42, -250,
    0, -250,    0,    0,  -78,  -42,  -44,  -40,  -78,    0,
 -104, -242,    0,    0,    0,    0,  -42,    0,    0,  -70,
    0,  -20,    0,
};
short yyrindex[] = {                                      0,
    0,    0,    0,    0,    0,    0,    0,    2,    0,    3,
    0,    0,    0,    0,  -41,    0,    0,    0,  -39,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  -34,    0,
};
short yygindex[] = {                                      0,
    6,    0,    0,    0,    7,    1,    0,  -10,    0,    0,
};
#define YYTABLESIZE 219
short yytable[] = {                                      15,
   18,    3,    2,   27,   16,   24,    1,    2,    3,   12,
    4,    5,   22,   13,   25,   20,   32,   21,   26,   16,
   29,   30,   33,   27,   14,   28,    0,   31,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    3,    2,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   14,   18,
};
short yycheck[] = {                                      42,
   42,    0,    0,   44,   44,   16,  257,  258,  259,  123,
  261,  262,   91,    8,   59,   10,   27,   12,   59,   59,
  125,  264,   93,   44,   59,   19,   -1,   27,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  125,  125,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  260,  260,
};
#define YYFINAL 6
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 264
#if YYDEBUG
char *yyname[] = {
"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,"'*'",0,"','",0,0,0,0,0,0,0,0,0,0,0,0,0,0,"';'",0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'['",0,"']'",0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'{'",0,"'}'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"STRUCT","CHAR","INT","NAME","DOUBLE","FLOAT","UNKNOWN","NUMBER",
};
char *yyrule[] = {
"$accept : model",
"model : decls",
"decls : decl",
"decls : multidecl",
"decls : decl decls",
"decls : multidecl decls",
"multidecl : type multicomp ';'",
"decl : type comp ';'",
"type : struct",
"type : CHAR",
"type : INT",
"type : FLOAT",
"type : DOUBLE",
"struct : STRUCT '{' decls '}'",
"multicomp : comp ',' comp",
"multicomp : comp ',' multicomp",
"comp : comp1",
"comp : point comp",
"point : '*'",
"point : '*' brack",
"brack : '[' NUMBER ']'",
"comp1 : NAME",
"comp1 : comp1 brack",
};
#endif
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 500
#define YYMAXDEPTH 500
#endif
#endif
int yydebug;
int yynerrs;
int yyerrflag;
int yychar;
short *yyssp;
YYSTYPE *yyvsp;
YYSTYPE yyval;
YYSTYPE yylval;
short yyss[YYSTACKSIZE];
YYSTYPE yyvs[YYSTACKSIZE];
#define yystacksize YYSTACKSIZE
#line 316 "cgram.y"

TypeDesc join(TypeDesc td1, TypeDesc td2)
{
	TypeDesc tmp;
	ArrayDesc ar;
	AddrDesc ad;

	if (td2 == NULL) return (td1);
	if (td1 == NULL) return (td2);
	tmp = td1;
	while (1) {
		if (tmp->name != TDT_ARRAY && tmp->name != TDT_ADDR){
			printf("Could not join\n");
			return (NULL);
		}
		if (tmp->name == TDT_ARRAY) {
			ar = (ArrayDesc) tmp->value;
			if (ar->type == NULL) {
				ar->type = td2;
				return (td1);
			}
			else 
				tmp = ar->type;
		}
		else if (tmp->name == TDT_ADDR) {
			ad = (AddrDesc) tmp->value;
			if (ad->type == NULL) {
				ad->type = td2;
				return (td1);
			}
			else
				tmp = ad->type;
		}
	}
}





extern FILE *yyin;
main(int argc,char  **argv)
{
	int i;
	char *nume;
	
	if (argc > 1)
		yyin = fopen( argv[1], "r" );
	else {
		printf ("Usage: c2xml file\nThe output with the xml description is : file.xml\n");
		exit(0);
	}
	
	do {
		yyparse();
	}
	while(!feof(yyin));
	nume = (char*) malloc (strlen(argv[1]) + 5);
	strcpy (nume, argv[1]);
	strcat (nume, ".xml"); 
	
	model2xml (md, nume);

	free(nume);
}

yyerror(char *s)
{
	printf("%s at line %d\n",s , line);
	exit(0);
}
#line 291 "y.tab.c"
#define YYABORT goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR goto yyerrlab
int
#if defined(__STDC__)
yyparse(void)
#else
yyparse()
#endif
{
    register int yym, yyn, yystate;
#if YYDEBUG
    register char *yys;
    extern char *getenv();

    if (yys = getenv("YYDEBUG"))
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = (-1);

    yyssp = yyss;
    yyvsp = yyvs;
    *yyssp = yystate = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yyssp >= yyss + yystacksize - 1)
        {
            goto yyoverflow;
        }
        *++yyssp = yystate = yytable[yyn];
        *++yyvsp = yylval;
        yychar = (-1);
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;
    yyerror("syntax error");
#ifdef lint
    goto yyerrlab;
#endif
yyerrlab:
    ++yynerrs;
yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yyssp]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yyssp, yytable[yyn]);
#endif
                if (yyssp >= yyss + yystacksize - 1)
                {
                    goto yyoverflow;
                }
                *++yyssp = yystate = yytable[yyn];
                *++yyvsp = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yyssp);
#endif
                if (yyssp <= yyss) goto yyabort;
                --yyssp;
                --yyvsp;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = (-1);
        goto yyloop;
    }
yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    yyval = yyvsp[1-yym];
    switch (yyn)
    {
case 1:
#line 66 "cgram.y"
{
     	
     	int i;
     	/*printf("correct list of declarations\n");*/
	md = (DataDesc) malloc (sizeof (struct data_desc));
	if (md == NULL) {
		printf("Memory allocation failed for md in model\n");
		exit(1);
	}
	md->ad = (Decl *) malloc (yyvsp[0].decls->nd * sizeof (Decl));
	if (md->ad == NULL) {
		printf("Memory allocation failed for md->ad in model\n");
		exit(1);
	}
	md->nd = yyvsp[0].decls->nd;
	for (i=0;i<md->nd;i++) {
		md->ad[i] = yyvsp[0].decls->decls[i];
	}
}
break;
case 2:
#line 87 "cgram.y"
{

	/*printf("decl\n");*/
	yyval.decls = (Decls ) malloc (sizeof (Decls));
	yyval.decls->nd = 1;
	yyval.decls->decls = (Decl *) malloc ( sizeof(void*) );
	yyval.decls->decls[0] = yyvsp[0].decl;
	/*printf("end decl\n");*/
}
break;
case 3:
#line 97 "cgram.y"
{
     
	yyval.decls = yyvsp[0].decls;
}
break;
case 4:
#line 101 "cgram.y"
{
	
	int i;

	/*printf("decl decls\n");*/
	yyval.decls = (Decls ) malloc (sizeof (Decls));
	yyval.decls->nd = 1 + yyvsp[0].decls->nd;
	yyval.decls->decls = (Decl *) malloc ( (1 + yyvsp[0].decls->nd) * sizeof(void*));
	yyval.decls->decls[0] = yyvsp[-1].decl;
	for(i=0; i<yyvsp[0].decls->nd; i++)
		yyval.decls->decls[i+1] = yyvsp[0].decls->decls[i];
	free(yyvsp[0].decls->decls);
	free(yyvsp[0].decls);
	/*printf("end decl decls\n");*/
	
}
break;
case 5:
#line 117 "cgram.y"
{

	int i;
	/*printf("multidecl decls\n");*/
	
	yyval.decls = (Decls ) malloc (sizeof (Decls));
	yyval.decls->nd = yyvsp[-1].decls->nd + yyvsp[0].decls->nd;
	yyval.decls->decls = (Decl *) malloc ( (yyvsp[-1].decls->nd + yyvsp[0].decls->nd) * sizeof(void*));
	for(i=0; i<yyvsp[-1].decls->nd; i++)
		yyval.decls->decls[i] = yyvsp[-1].decls->decls[i];
	for(i=0; i<yyvsp[0].decls->nd; i++)
		yyval.decls->decls[i + yyvsp[-1].decls->nd] = yyvsp[0].decls->decls[i];

	free(yyvsp[-1].decls);
	free(yyvsp[0].decls);
	/*printf("end multidecl decls\n");*/
	
}
break;
case 6:
#line 136 "cgram.y"
{
	
	int i;
	Decl decl;
	yyval.decls = (Decls ) malloc (sizeof (Decls));
	yyval.decls->nd = yyvsp[-1].multicomp->ncomps;
	yyval.decls->decls = (Decl *) malloc (yyvsp[-1].multicomp->ncomps);

	for (i=0; i<yyvsp[-1].multicomp->ncomps; i++) {
		yyval.decls->decls[i] = (Decl) malloc (sizeof(Decl));
		decl = yyval.decls->decls[i];
		decl->name = yyvsp[-1].multicomp->comps[i]->name;
		decl->type = yyvsp[-1].multicomp->comps[i]->td;
		decl->type = join (decl->type, yyvsp[-2].typedesc);
	}	
}
break;
case 7:
#line 154 "cgram.y"
{
	
	/*printf("type comp\n");*/
	yyval.decl = (Decl) malloc (sizeof (Decl));
	yyval.decl->name = yyvsp[-1].comp->name;
	yyval.decl->type = yyvsp[-1].comp->td;
	yyval.decl->type = join (yyval.decl->type, yyvsp[-2].typedesc);
	/*printf("type comp end\n");*/
	
}
break;
case 8:
#line 165 "cgram.y"
{
	
	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_STRUCT;
	yyval.typedesc->value = yyvsp[0].structdesc;
}
break;
case 9:
#line 171 "cgram.y"
{

	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_CHAR;
	yyval.typedesc->value = NULL;
}
break;
case 10:
#line 177 "cgram.y"
{

	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_INT;
	yyval.typedesc->value = NULL;
}
break;
case 11:
#line 183 "cgram.y"
{

	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_FLOAT;
	yyval.typedesc->value = NULL;
}
break;
case 12:
#line 189 "cgram.y"
{

	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_DOUBLE;
	yyval.typedesc->value = NULL;
}
break;
case 13:
#line 197 "cgram.y"
{

	/*printf("struct\n");*/
	yyval.structdesc = (StructDesc) malloc (sizeof (StructDesc));
	yyval.structdesc->num_decls = yyvsp[-1].decls->nd;
	yyval.structdesc->decls = yyvsp[-1].decls->decls;
	free(yyvsp[-1].decls);
}
break;
case 14:
#line 207 "cgram.y"
{

	yyval.multicomp = (Multicomp) malloc (sizeof (Multicomp));
	yyval.multicomp->comps = (Comp*) malloc (2  *  sizeof(void*));
	yyval.multicomp->comps[0] = yyvsp[-2].comp;
	yyval.multicomp->comps[1] = yyvsp[0].comp;
	yyval.multicomp->ncomps = 2;
	 
}
break;
case 15:
#line 216 "cgram.y"
{

	int i;
	yyval.multicomp = (Multicomp) malloc (sizeof (Multicomp));
	yyval.multicomp->comps = (Comp*) malloc( 1 + yyvsp[0].multicomp->ncomps);
	yyval.multicomp->comps[0] = yyvsp[-2].comp;
	for(i=0; i<yyvsp[0].multicomp->ncomps; i++)
		yyval.multicomp->comps[1+i] = yyvsp[0].multicomp->comps[i];
	free(yyvsp[0].multicomp->comps);
	free(yyvsp[0].multicomp);
}
break;
case 16:
#line 230 "cgram.y"
{
    
	yyval.comp = yyvsp[0].comp;
}
break;
case 17:
#line 234 "cgram.y"
{

	TypeDesc tmp;
	ArrayDesc ar;
	AddrDesc ad;
	
	/*printf("point comp\n");*/
	tmp = yyvsp[0].comp->td;
	yyval.comp = yyvsp[0].comp;
	yyval.comp->td = join (tmp, yyvsp[-1].typedesc);
}
break;
case 18:
#line 247 "cgram.y"
{
	
	ArrayDesc ar;
	AddrDesc ad;
	TypeDesc td;

	/*printf("*\n");*/
	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_ADDR;
	ad = (AddrDesc) malloc (sizeof (AddrDesc));
	yyval.typedesc->value = ad;

	td = (TypeDesc) malloc (sizeof (TypeDesc));
	ad->type = td;
	ar = (ArrayDesc) malloc (sizeof (ArrayDesc));
	td->name = TDT_ARRAY;
	td->value = ar;
	ar->size = 0;
	ar->type = NULL;
	
}
break;
case 19:
#line 268 "cgram.y"
{

	AddrDesc ad;
	
	/*printf("* brack\n");*/
	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_ADDR;
	ad = (AddrDesc) malloc (sizeof (AddrDesc));

	yyval.typedesc->value = ad;
	ad->type = yyvsp[0].typedesc;
}
break;
case 20:
#line 282 "cgram.y"
{

	ArrayDesc ar;

	/*printf("brack\n");*/
	yyval.typedesc = (TypeDesc) malloc (sizeof (TypeDesc));
	yyval.typedesc->name = TDT_ARRAY;
	ar = (ArrayDesc) malloc (sizeof (ArrayDesc));
	yyval.typedesc->value = ar;
	ar->size = atoi (name);
	/*printf("NUMBER = %d\n", ar->size);*/
	ar->type = NULL;
}
break;
case 21:
#line 297 "cgram.y"
{
	
	/*printf("NAME = %s\n",name);*/
	yyval.comp = (Comp) malloc (sizeof(Comp));
	yyval.comp->name = (char*) strdup(name);
	yyval.comp->td = NULL;
}
break;
case 22:
#line 304 "cgram.y"
{
	
	/*printf("comp1 brack\n");*/
	yyval.comp = yyvsp[-1].comp;
	if (yyval.comp->td == NULL)
		yyval.comp->td = yyvsp[0].typedesc;
	else
		yyval.comp->td = join (yyval.comp->td, yyvsp[0].typedesc);
}
break;
#line 725 "y.tab.c"
    }
    yyssp -= yym;
    yystate = *yyssp;
    yyvsp -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yyssp = YYFINAL;
        *++yyvsp = yyval;
        if (yychar < 0)
        {
            if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yyssp, yystate);
#endif
    if (yyssp >= yyss + yystacksize - 1)
    {
        goto yyoverflow;
    }
    *++yyssp = yystate;
    *++yyvsp = yyval;
    goto yyloop;
yyoverflow:
    yyerror("yacc stack overflow");
yyabort:
    return (1);
yyaccept:
    return (0);
}
