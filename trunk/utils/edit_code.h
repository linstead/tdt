#include "xml2code.h"

void
add_fortran_write_code(coding_data *, char *);

void
add_fortran_read_code(coding_data *cd, char *name);

void
add_c_write_code(coding_data *cd, char *name);

void
add_c_read_code(coding_data *cd, char *name);

void
init_fortran_write_code(coding_data *cd);

void
init_fortran_read_code(coding_data *cd);

void
init_c_write_code(coding_data *);

void
init_c_read_code(coding_data *cd);

void
end_fortran_write_code(coding_data *cd);

void
end_fortran_read_code(coding_data *cd);

void
end_c_write_code(coding_data *cd);

void
end_c_read_code(coding_data *cd);


