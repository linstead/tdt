/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Windows changes 29-09-2002, Dan
 * Only added TDT_ in front of the defined types
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <expat.h>
#include "tdterror.h"
#include "typeops.h"
#include "typeops_cons.h"
#include "xmlhandlers.h"
int Call=0;	/* How often char data handler has been called */

/* Returns NULL on errors */
String
get_decl_name (const char **attr) {

	if (attr == NULL) {
		tdt_error ("NULL parameter to get_decl_name");
		return NULL;
	}
	if (strcasecmp (attr[0], "name") == 0) {
		return (String) attr[1];
	}
	return NULL;
}

/* Returns -1 on error */
int
get_array_size (const char **attr) {

	if (attr == NULL) {
		tdt_error ("NULL parameter to get_array_size");
		return -1;
	}

	if (strcasecmp (attr[0], "size") == 0) {
		return atoi (attr[1]);
	}
	return -1;
}

/* Returns NOTYPE on errors */
TypeName
string_to_typename (String string) {

	if (strcasecmp (string, "INT") == 0) {
		return TDT_INT;
	} else if (strcasecmp (string, "CHAR") == 0) {
		return TDT_CHAR;
	} else if (strcasecmp (string, "FLOAT") == 0) {
		return TDT_FLOAT;
	} else if (strcasecmp (string, "DOUBLE") == 0) {
		return TDT_DOUBLE;
	} 

	return TDT_NOTYPE;
}

TDTConfig
init_config () {

    TDTConfig tc;

    tc = (TDTConfig) malloc (sizeof (struct tdt_config));

    if (tc == NULL) {
        tdt_error ("Cannot allocate memory for TDTConfig in tdt_configure()");
        return NULL;
    }

    tc->num_channels = 0;

    return tc;
}

/* Returns NULL on errors */
DataDesc
init_datadesc () {

	DataDesc   md;

	md = (DataDesc) malloc (sizeof (struct data_desc));
	if (md == NULL) {
		tdt_error ("Memory allocation failed for md in init_datadesc");
		return NULL;
	}

	md->ad = (Decl *) malloc (NUM_TO_ALLOC * sizeof (Decl));
	if (md->ad == NULL) {
		tdt_error ("Memory allocation failed for md->ad in init_datadesc");
		return NULL;
	}
	md->nd = NUM_TO_ALLOC;

	md->cd = -1;
	md->td = NULL;
	md->as = NULL;
	md->ns = 0;
	md->cs = -1;

	return md;
}

/* Crashes on errors -- exit (1) */
void
free_datadesc (DataDesc md) {

	int         i;
	if (md == NULL) {
		tdt_error ("NULL argument to free_datadesc");
		exit (1);
	}

	for (i = 0; i <= md->cd; i++) {
		destroy_decl (md->ad[i]);
	}

	free (md->ad);
	free (md);
}

/* function to return the type of a particular decl within a data desc 
 * e.g. at "astruct/innerstruct/innerthing" */
/* Returns NULL on error */
TypeDesc
find_type_in_datadesc (DataDesc md, String name) {

	int         nd;
	String      realname;
	String      usedname;
	StructDesc  sd;
	TypeDesc    td;
	Decl       *ad;

	if (md == NULL) {
		tdt_error ("NULL md passed to find_type_in_datadesc");
		return NULL;
	}
	if (name == NULL) {
		tdt_error ("NULL name passed to find_type_in_datadesc");
		return NULL;
	}

	usedname = strdup (name);

	if (usedname == NULL) {
		tdt_error ("strdup failed in find_type_in_datadesc");
		return NULL;
	}

	realname = strtok (usedname, "/");

	if (realname == NULL) {
        tdt_error ("realname is NULL in find_type_in_datadesc");
        free(usedname);
		return NULL;
	}

	ad = md->ad;
	nd = md->cd + 1;

	while (TRUE) {
		td = find_type_in_decls (ad, nd, realname);
		if (td == NULL) {
            tdt_error ("find_type_in_decls returned NULL in find..in datadesc");
            tdt_error ( realname );
            tdt_error ( name );
			return NULL;
		}

		realname = strtok (NULL, "/");

		if (realname == NULL) {
            free(usedname);
			return td;
		} else {
			sd = (StructDesc) type_value (td);
			ad = sd->decls;
			nd = sd->num_decls;
		}
	}


}

/* Main XML Parsing function */
/* Returns NULL on errors */
#define BUFFSIZE 32000
void *
parse_xml (char *fn,
           void *md,
           void (*start) (void *, const char *, const char **),
           void (*end1) (void *, const char *),
           void (*chardata) (void *, const char *, int)){

	FILE       *xfp;
	int         done;
	int         len;
	char        buff[BUFFSIZE];

	XML_Parser  p = XML_ParserCreate (NULL);

	if (!p) {
		tdt_error ("XML_ParserCreate in parse_xml");
		return NULL;
	}

	if (fn == NULL) {
		tdt_error ("NULL file name passed to parse_xml");
		return NULL;
	}

	if ((xfp = fopen (fn, "r")) == NULL) {
		tdt_error ("failed to open file in parse_xml for file:");
		tdt_error (fn);
		return NULL;
	}

	XML_SetUserData (p, md);

	/* start() and end1() are XML start and end tag handlers
	 * chardata() is the character data handler */

	XML_SetElementHandler (p, *start, *end1);
	XML_SetCharacterDataHandler (p, *chardata);

	/* The XML Parsing loop
	 */
	for (;;) {
		len = fread (buff, 1, BUFFSIZE, xfp);

		if (ferror (xfp)) {
			tdt_error ("error in fread in parse_xml");
			return NULL;
		}

		done = feof (xfp);

		if (!XML_Parse (p, buff, len, done)) {
			tdt_error ("XML_Parse in parse_xml");
			return NULL;
		}

		if (done) {
			fclose (xfp);
			return md;
		}
	}
}

void
datadesc_start_handler (void *data, const char *el, const char **attr) {

}

/* Crashes on errors -- exit (1) */
void
decl_start_handler (void *data, const char *el, const char **attr) {

	Decl        decl;
	String      name;
	DataDesc   md;

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("NULL md in decl_start_handler");
		exit (1);
	}

	name = get_decl_name (attr);
	if (name == NULL) {
		tdt_error ("Name of decl is NULL in decl_start_handler");
		exit (1);
	}
	md->td = make_typedesc (TDT_NOTYPE, NULL);
	if (md->td == NULL) {
		tdt_error ("make_typedesc in decl_start_handler");
		exit (1);
	}

	decl = make_decl (name, md->td);
	if (decl == NULL) {
		tdt_error ("make_decl in decl_start_handler");
		exit (1);
	}

	if (md->cs < 0) {
		md->cd++;
		if (md->cd >= md->nd) {
			md->ad = (Decl *) realloc (md->ad, 
					(md->nd + NUM_TO_ALLOC) * sizeof (Decl));
			if (md->ad == NULL) {
				tdt_error ("md->ad realloc failed in decl_start_handler");
				exit (1);
			}
			md->nd += NUM_TO_ALLOC;
		}
		md->ad[md->cd] = decl;
	} else {
		add_decl (md->as[md->cs], decl);
	}
}

void
typedef_start_handler (void *data, const char *el, const char **attr) {

}

/* Crashes on errors -- exit (1) */
void
struct_start_handler (void *data, const char *el, const char **attr) {

	DataDesc   md;
	StructDesc  sd;

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("md is NULL in struct_start_handler");
		exit (1);
	}

	sd = make_empty_structdesc ();
	if (sd == NULL) {
		tdt_error ("make_empty_structdesc in struct_start_handler");
		exit (1);
	}
	set_typename (md->td, TDT_STRUCT);
	set_typevalue (md->td, sd);

	md->cs++;
	if (md->cs >= md->ns) {
		md->as = (StructDesc *) realloc (md->as,
					(md->ns + NUM_TO_ALLOC) * sizeof (StructDesc));

		if (md->as == NULL) {
			tdt_error ("Realloc of md->as in struct_start_handler");
			exit (1);
		}

		md->ns += NUM_TO_ALLOC;
	}
	md->as[md->cs] = sd;
}

/* Crashes on errors -- exit (1) */
void
array_start_handler (void *data, const char *el, const char **attr) {

	DataDesc   md;
	ArrayDesc   ad;
	TypeDesc    td;
	int         size;

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("NULL md passed to array_start_handler");
		exit (1);
	}
	if (attr == NULL) {
		tdt_error ("NULL attr passed to array_start_handler");
		exit (1);
	}

	td = make_typedesc (TDT_NOTYPE, NULL);
	if (td == NULL) {
		tdt_error ("make_typedesc failed in array_start_handler");
		exit (1);
	}

	size = get_array_size (attr);
	if (size == -1) {
		tdt_error ("get_array_size failed in array_start_handler");
		exit (1);
	}

	ad = make_arraydesc (size, td);
	if (ad == NULL) {
		tdt_error ("make_arraydesc failed in array_start_handler");
		exit (1);
	}
	set_typename (md->td, TDT_ARRAY);
	set_typevalue (md->td, ad);

	md->td = td;
}

/* Crashes on errors -- exit (1) */
void
addr_start_handler (void *data, const char *el, const char **attr) {

	DataDesc   md;
	AddrDesc    ad;
	TypeDesc    td;

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("NULL md passed to addr_start_handler");
		exit (1);
	}

	td = make_typedesc (TDT_NOTYPE, NULL);
	if (td == NULL) {
		tdt_error ("make_typedesc failed in addr_start_handler");
		exit (1);
	}

	ad = make_addrdesc (td);
	if (ad == NULL) {
		tdt_error ("make_addrdesc failed in addr_start_handler");
		exit (1);
	}

	set_typename (md->td, TDT_ADDR);
	set_typevalue (md->td, ad);

	md->td = td;
}

/* Crashes on errors -- exit (1) */
void
start (void *data, const char *el, const char **attr) {


	if (0 == strcasecmp (el, "data_desc")) {
		datadesc_start_handler (data, el, attr);
	}

	else if (0 == strcasecmp (el, "decl")) {
		decl_start_handler (data, el, attr);
	}

	else if (0 == strcasecmp (el, "typedef")) {
		typedef_start_handler (data, el, attr);
	}

	else if (0 == strcasecmp (el, "struct")) {
		struct_start_handler (data, el, attr);
	}

	else if (0 == strcasecmp (el, "array")) {
		array_start_handler (data, el, attr);

	}

	else if (0 == strcasecmp (el, "addr")) {
		addr_start_handler (data, el, attr);
	}

	else {	/* Unknown tag */
		tdt_error ("unknown tag in start");
		exit (1);
	}

}	/* End of start tag handler */

/* Crashes on errors -- exit (1) */
void
end1 (void *data, const char *el) {

	DataDesc   md;

	Call = 0;

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("NULL md passed to end1");
		exit (1);
	}

	if (strcasecmp (el, "STRUCT") == 0) {
		md->cs--;
	}

}	/* End of end tag handler */

/* Crashes on errors -- exit (1) */
void
chardata (void *data, const char *el, int len) {

	int         i, j;
	TypeName    tn;
	DataDesc   md;

	char       *temp;

	if (Call == 1) /* chardata is called with the same el as before */
		return;

	temp = (char *) malloc ((len + 1) * sizeof (char));	/* +1 for EOL */

	if (temp == NULL) {
		tdt_error ("Memory allocation of temp in chardata");
		exit (1);
	}

	md = (DataDesc) data;
	if (md == NULL) {
		tdt_error ("NULL md in chardata");
		exit (1);
	}
	if (el == NULL) {
		tdt_error ("NULL el passed to chardata");
		exit (1);
	}

	/* Find out if we've extracted text */
	j = 0;
	for (i = 0; i < len; i++) {
		if (isalnum (el[i])) {
			temp[j] = el[i];
			j++;
		} else {
			/* do nothing */
		}
	}
	/* chardata gets called even on newlines
	 * or whitespace usw. In these cases, we
	 * should skip the rest, and make a 
	 * strategic retreat.
	 */
	if (j == 0) { /* el was empty, clean up & exit */
		free (temp);
		return;
	}
	/* Append an EOL character */
	temp[j] = '\0';

	/* Another quirk of how chardata gets called:
	 * it gets called once for every character in 
	 * the element. Therefore, we do the handling
	 * the first time (when Call == 0), and never
	 * after.
	 */
	Call = 1;

	/* Text elements will only refer to primitive types */
	tn = string_to_typename (temp);
	if (tn == TDT_NOTYPE) {
		tdt_error ("string_to_typename in chardata");
		exit (1);
	}
	set_typename (md->td, tn);
	set_typevalue (md->td, NULL);

	free (temp);
}

/* Start of config file parsing support functions */

Channel
make_channel (String name,
              String mode,
              String host,
              String type,
              String filename,
              String datadesc,
              int port) {

    Channel c;

    c = (Channel) malloc (sizeof (struct channel));

    if (c == NULL) {
        tdt_error ("Failed to malloc Channel in make_channel");
        exit (1);
    }

    c->name     = (char *) strdup(name);
    c->mode     = (char *) strdup(mode);
    c->host     = (char *) strdup(host);
    c->type     = (char *) strdup(type);
    c->filename = (char *) strdup(filename);
    c->datadesc = (char *) strdup(datadesc);
    c->port     = port;

    return c;

}
void
add_channel (TDTConfig tc, Channel c) {

    if (tc->num_channels == 0) {
        tc->channels = (Channel *) malloc (sizeof(struct channel));
    } else {
        tc->channels = (Channel *) realloc (tc->channels,
                                           (tc->num_channels+1) * sizeof(struct channel));
    }

    tc->channels[tc->num_channels] = c;

    tc->num_channels++;

}

/* Start of config file XML tag handlers */

/* "channel" start tag handler */
void
channel_start_handler (void *data, const char *el, const char **attr) {

    String name="";
    String mode="";
    String host="";
    int    port=0;
    String type="";
    String filename="";
    String datadesc="";

    TDTConfig tc;
    Channel c;

    int i;

    tc = (TDTConfig) data;

    /* Extract the attributes */
    if (attr[0] == NULL) {
        tdt_error ("No attributes found in channel_start_handler");
        exit (1);
    }

    /* TODO: Could use some more logic in here: e.g. if type is "socket" then
       we have to have "port" and "host" attributes */

    i = 0;
    while (1) {

        /* process the attribute */
        if (strcasecmp (attr[i], "name") == 0) {
            name = (String) attr[i+1];
        } else if (strcasecmp (attr[i], "mode") == 0) {
            mode = (String) attr[i+1];
        } else if (strcasecmp (attr[i], "host") == 0) {
            host = (String) attr[i+1];
        } else if (strcasecmp (attr[i], "port") == 0) {
            port = atoi (attr[i+1]);
        } else if (strcasecmp (attr[i], "type") == 0) {
            type = (String) attr[i+1];
        } else if (strcasecmp (attr[i], "filename") == 0) {
            filename = (String) attr[i+1];
        } else if (strcasecmp (attr[i], "datadesc") == 0) {
            datadesc = (String) attr[i+1];
        }

        /* was that the last attribute? */
        if (attr[i+2] == NULL) {
            break;
        }
        /* else go to the next attribute */
        else {
            i+=2;
        }
    }

    /* Now prepare a Channel structure and add it to the TDTConfig */
    c  = make_channel (name, mode, host, type, filename, datadesc, port);
    add_channel (tc, c);
}

/* "channel" end tag handler */
void
channel_end_handler (void *data, const char *el) {
}

/* Crashes on errors -- exit (1) */
void
start_config (void *data, const char *el, const char **attr) {

	if (0 == strcasecmp (el, "program")) {
	}

	else if (0 == strcasecmp (el, "channel")) {
        channel_start_handler (data, el, attr);
	}

	else {	/* Unknown tag */
		tdt_error ("unknown tag in config start handler");
		exit (1);
	}

}

/* Crashes on errors -- exit (1) */
void
end_config (void *data, const char *el) {

	if (0 == strcasecmp (el, "program")) {
	}

	else if (0 == strcasecmp (el, "channel")) {
        channel_end_handler (data, el);
	}

	else {	/* Unknown tag */
		tdt_error ("unknown tag in config start handler");
		exit (1);
	}

}

/* Crashes on errors -- exit (1) */
void
chardata_config (void *data, const char *el, int len) {
}

/* Main entry point for parsing configuration files */
TDTConfig
read_config (char *fn) {

    TDTConfig tc;

    tc = init_config();

    if (tc == NULL) {
        tdt_error ("init_config in read_config");
        return NULL;
    }

    tc = (TDTConfig) parse_xml (fn, (void *) tc, start_config, end_config, chardata_config);

    if (tc == NULL) {
        tdt_error ("parse_xml in read_config");
        return NULL;
    }

    return tc;

}

/* Main entry point for parsing data description files */
DataDesc
read_datadesc (char *fn) {

    DataDesc md;

	md = init_datadesc ();

	if (md == NULL) {
		tdt_error ("init_datadesc in read_datadesc");
		return NULL;
	}

    md = (DataDesc) parse_xml (fn, (void *) md, start, end1, chardata);

    if (md == NULL) {
        tdt_error ("parse_xml in read_datadesc");
        return NULL;
    }

    return md;
}
