/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __TYPEOPS_CONS_H__
#define __TYPEOPS_CONS_H__
/* Constructors and Destructors for typeops types */

/* Constructor functions */

TypeDesc
make_typedesc (TypeName tn, void * val);

Decl
make_decl (String name, TypeDesc td);

StructDesc
make_structdesc (int num_decls, Decl *decls);

StructDesc
make_empty_structdesc();

ArrayDesc
make_arraydesc (int size, TypeDesc td);

AddrDesc
make_addrdesc (TypeDesc td);

TypedefDesc
make_typedefdesc (String name, TypeDesc td);

/* Destructor functions */
void
destroy_char (void *v);

void
destroy_int (void *v);

void
destroy_float (void *v);

void
destroy_double (void *v);

void 
destroy_addrdesc (AddrDesc ad);

void 
destroy_addr (void *v);

void
destroy_arraydesc (ArrayDesc ar);

void 
destroy_array (void *v);

void
destroy_structdesc (StructDesc sd);

void
destroy_struct (void *v);

void
destroy_typedesc (TypeDesc td);

void
destroy_decl (Decl d);

void
destroy_typedefdesc (TypedefDesc tdd);

#endif
