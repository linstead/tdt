#include <string.h>
#include "swapbytes.h"

#define max_size 64
void 
swap (void* value, int size) {
     
    int  i;
    char swapped[max_size];
    
    for (i=0; i<size; i++) {
        swapped[i] = *( (char *) value + (size-i-1));
    }

    memmove (value, swapped, size);
}
