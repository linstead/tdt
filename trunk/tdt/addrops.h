/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File addrops.h
 * Declarations required for implementing address operations
 *
 * 23/10/2001
 * Ciaron: basic structures for address operations
 * 20/11/2001
 * Cezar: got rid of the AddressStub data structure
 * 		modified the other typedefed structures to refer to pointers
 * 		added a mode parameter to FileAddr and SockAddr 
 */

#ifndef __ADDROPS_H__
#define __ADDROPS_H__

/* socklen_t is not defined in Cygwin libraries, so add
 * here if required */
/* Windows changes 29-09-2002, Dan
 * Added SOCKET variable to tdtaddr structure
 */
#ifdef WIN32
#include <winsock.h>
typedef int socklen_t;
#endif

#ifdef _MPI
#include "mtdt.h"
#else
#include "tdt.h"
#endif

/*typedef enum mode {READ, WRITE} Mode;*/
#ifdef _MPI
typedef enum protocol {FILEP, SOCKETP, MPI /* etc. */} Protocol;
#else
typedef enum protocol {FILEP, SOCKETP /* etc. */} Protocol;
#endif


typedef struct tdtaddr {
	Protocol prot;  /* see enum above */
	void *addr;		/* specific to connection type */
	FILE *fp;		/* the file pointer associated to the connection */	
	int requires_swap;
	Array_Order array_ord;
	Array_Order array_ord_host;
	Boolean transfer_blocks;
	#ifdef WIN32 
	SOCKET sock;
	#endif
}* TDTAddr;

/* Address of a file */
typedef struct faddr {
	Mode mode;
	String fname;
}* FileAddr;

/* Address of a socket */
typedef struct saddr {
	Mode mode;
	String hostname; 
	int port;
}* SocketAddr;

#ifdef _MPI
/* Address of a mpi destination */
typedef struct mpiaddr {
	Mode mode;
	int rank;
}* MPIAddr;
#endif

/*
 * User level functions for constructing a TDTAddress object.
 * The user is not supposed to touch this object, except for passing it
 * around to tdt_open_addr, tdt_write/read and tdt_close_addr.
 */
TDTAddr
mkfileaddr (String fname, Mode mode); 

TDTAddr
mksocketaddr (String hostname, int port, Mode mode);

#ifdef _MPI
TDTAddr
mkmpiaddr (int i);
#endif

/*
 * tdt_open calls the apropriate open function, according to the
 * protocol parameter in the TDTAddr structure.
 * Returns: a TDTAddr completed with a FILE *
 */
TDTAddr
tdt_open_addr (TDTAddr ad);

/*
 * file_open opens the file given by ad -> addr -> fname, with the
 * mode given by ad -> addr -> mode.
 */
TDTAddr
file_open (TDTAddr ad);

/*
 * socket_open opens a socket to write to or read from, according to
 * ad->addr->mode.
 * if mode == READ, then the FILE * used is that of the socket
 * listening for connections. 
 * if mode == WRITE, the FILE * used is that returned by the
 * socket (2) function. 
 */
TDTAddr
socket_open (TDTAddr ad);

/* 
 * Takes care that the resources associated with the given address are
 * cleaned.
 */
void 
tdt_close_addr (TDTAddr ad);

void
close_file (TDTAddr ad);

void
close_socket (TDTAddr ad);

#ifdef _MPI
void
close_mpi (TDTAddr ad);
#endif


/* 
 * The read and write functions, taking the parameters specified by the
 * pseudocode.
 * Return 0 on succes.
 */
int
tdt_recv (void *var, TypeDesc td, TDTAddr ad);

int
tdt_send (void *var, TypeDesc td, TDTAddr ad);

/* set and get the status of the byte swap flag,
 * i.e. whether or not byte swapping is needed for this connection
 */

void
set_swap (TDTAddr ta, int requires_swap);

int
get_swap (TDTAddr ta);

#endif
