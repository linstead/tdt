/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* SERVER (READER) EXAMPLE */

#include <stdio.h>
#include <stdlib.h>
#include "tdt.h"

int
main (int argc, char **argv) {

    /* a loop counter */
    int i;

    /* variables we want to read */
    double vardouble;

    /* C-to-C only */
    /* double * ai[10]; */
    /* End C-to-C only */

    struct {
        int    elem1[2];
        double elem2;
    } astruct;

    int varint;
    double *dynarray;

    /* variables required by TDT */
    TDTConfig tc;
    TDTState  ts;

    /* Get the configuration */
    tc = tdt_configure ("servconf.xml");

    /* Establish the connection */
    ts = tdt_open (tc, "conn1");

    /* start reading the data */
    tdt_read (ts, &vardouble, "vardouble");
    tdt_read (ts, &astruct, "astruct");
    tdt_read (ts, &varint, "varint");

    /* reading the dynamic array is done here ... */
    dynarray = (double *) malloc (varint * sizeof (double));
    
    /* tdt_size_array (ts, "dyn", varint); */
    /* ... is equivalent to ... */    
    tdt_size_multiarray (ts, "dyn", 1, varint);

    /* now we can read the dynamically sized array */
    tdt_read (ts, dynarray, "dyn");

    /* C-to-C only */
    /* for (i = 0; i < 10; i++)
	    ai[i]=(double *) malloc (varint * sizeof (double));
    tdt_size_multiarray (ts, "adyn", 2, varint);
    tdt_read (ts, ai, "adyn");
    */
    /* End C-to-C only */
    

    /* Print what we've read */
    printf ("Read this:\n");
    printf ("    %d %d\n     %f\n", astruct.elem1[0],
            astruct.elem1[1], astruct.elem2);
    printf ("varint is %d\n", varint);
    printf ("vardouble is %f\n", vardouble);

    for (i = 0; i < varint; i++) {
        printf ("dyn[%d]=%f\n", i, dynarray[i]);
    }

    /* C-to-C only */
    /* printf ("ai[3][7]=%f\n",ai[3][7]); */
    /* End C-to-C only */
    
    /* Now tidy up, close connections etc. */
    tdt_close (ts);
    tdt_end (tc);

    return 0;
}
