/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* CLIENT (WRITER) EXAMPLE */

#include <stdio.h>
#include <stdlib.h>
#include "tdt.h"

int
main (int argc, char **argv) {

    /* a loop counter */
    int i;

    /* declare the variables we're going to write */
    double vardouble;
    
    struct {
        int    elem1[2];
        double elem2;
    } astruct;

    int varint;
    double *dynarray;
    
    /* C-to-C only */
    /* double * ai[10]; */
    /* End C-to-C only */

    /* declare some variables required by TDT */
    TDTConfig tc;
    TDTState  ts;

    /* Now make up some data to write... */
    astruct.elem1[0] = 10;
    astruct.elem1[1] = 20;
    astruct.elem2 = 199.99;

    vardouble = 123.45;
    varint = 12;

    /* define and populate a dynamic array */
    dynarray = (double *) malloc (varint * sizeof (double));
    for (i = 0; i < varint; i++) {
        dynarray[i] = i * 0.33;
    }

    /* Get the configuration */
    tc = tdt_configure ("clntconf.xml");
    
    /* Establish the connection */
    ts = tdt_open (tc, "conn1");
    
    /* now write the data */
    tdt_write (ts, &vardouble, "vardouble");
    tdt_write (ts, &astruct, "astruct");
    tdt_write (ts, &varint, "varint");

    /* we haven't yet told TDT how big dynarray will be, so do that now ... */
    tdt_size_multiarray (ts, "dyn", 1, varint);
    /* the following line is an alternative to the above, for 1-dimensional
     * arrays only: */
    /* tdt_size_array (ts, "dyn", varint); */

    /* ... and write it */
    tdt_write (ts, dynarray, "dyn");

    /* C-to-C only */
    /* allocate array of dynamical arrays*/
    /*for (i = 0; i < 10; i++) 
	    ai[i]=(double *) malloc (varint * sizeof (double));
    ai[3][7] = 11.11; */
    
    /* how big the arrays ai[i][] is */
    /*tdt_size_multiarray (ts, "adyn", 2, varint);
    tdt_write (ts, ai, "adyn"); */
    /* C-to-C only */
    
    /* tidy up, close connections etc. */
    tdt_close (ts);
    tdt_end (tc);

    return 0;
}
