#include <stdio.h>
#include <stdlib.h>
#include "parse.h"
#define SIZE 1 
struct parse_state{
	FILE *file;
	int pos;
	char s[SIZE+1];
	char ret1[2];
	char *ret2;
	char *last;
	int line;
} parse_state;


char *simple="{}*[];()";

void get_new_line(parser p)
{
	int n;
	n=sizeof(parse_state);
	n=fread(p->s,1,SIZE,p->file);
	p->s[n]=0;
	p->pos=0;
}
	

parser parse_init(char *name)
{
	parser p;
	p=(parser) calloc(1,sizeof(parse_state));
	p->file=fopen(name,"r");
	if(!p->file) {
		printf("error : file %s not find\n",name);
		return(NULL);
	}
	p->ret1[1]=0;
	p->ret2=NULL;
	p->s[SIZE+1]=0;
	get_new_line(p);
	p->line=1;
	return(p);
}

void parse_stop(parser p)
{
	fclose(p->file);
	free(p->ret2);
	free(p);
}

int is_blank(char c)
{
	if(c==' ' || c=='\t' ||  c=='\n' || c=='\r') 
		return(1);
	else
		return(0);
}

int is_simple(char c)
{
	int k;
	k=0;
	while (simple[k]!=0) {
		if(simple[k]==c)
			return(1);
		k++;
	}
	return(0);
}
		
		
char* parse_get_next(parser p)
{
	int i,k;
	if(p->s[p->pos]==0)
		get_new_line(p);
	while ( is_blank(p->s[p->pos]) &&  p->s[p->pos]!=0 ) {
		if(p->s[p->pos]=='\n')
			p->line++;
		p->pos++;
		if(p->s[p->pos]==0) {
			get_new_line(p);
			if(p->s[p->pos]==0)
				return(NULL);
			}
	}
	if ( is_simple(p->s[p->pos]) ) {
		p->ret1[0]=p->s[p->pos];
		p->pos++;
		p->last=p->ret1;
		return(p->ret1);
	}
	i=p->pos;
	if(p->ret2!=NULL)
		free(p->ret2);
	p->ret2=NULL;
	while ( !is_blank(p->s[i]) &&  !is_simple(p->s[i]) && p->s[i]!=0 ) {
		if(p->s[p->pos]=='\n')
			p->line++;
		i++;
		if(p->s[i]==0) {
			if(p->ret2==NULL) {
				p->ret2=(char*) calloc(1,i-p->pos);
				p->ret2[0]=0;
			}
			else {
				k=strlen(p->ret2);
				p->ret2=(char*) realloc(p->ret2,i-p->pos + k+1);
			}
			strncat(p->ret2,p->s+p->pos,i);
			get_new_line(p);
			if(p->s[p->pos]==0)
				return(p->ret2);
			i=0;
		}
	}
	if(p->s[p->pos]=='\n')
		p->line++;
	if(p->ret2==NULL) {
		p->ret2=(char*) calloc(1,i-p->pos);
		p->ret2[0]=0;
	}
	else
		p->ret2=(char*) realloc(p->ret2,i-p->pos + strlen(p->ret2));
	strncat(p->ret2,p->s+p->pos,i-p->pos);
	p->pos=i;
	p->last=p->ret2;
	return(p->ret2);
}

char* parser_get_last(parser p)
{
	return(p->last);
}

int parse_get_line(parser p)
{
	return(p->line);
}

