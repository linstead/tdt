#ifndef DECL_H
#define DECL_H

typedef enum  Type1 {zchar, zint, zfloat, zdouble, zstruct, notype1} type1;
typedef enum  Type2 {zaddr, zarray, simple} type2;

typedef struct {
	type1 type;
	void* type_desc;
} mytype1;

typedef struct {
	char *name;
	type2 type;
	void *type_desc;
} mytype2;

typedef struct {
	mytype1 *t1;
	mytype2 *t2;
} mydecl;

typedef struct  {
	int num_decls;
	mydecl **decls;
} struct_desc;

typedef struct {
	int dim;
	mytype2 *t;
} addr_desc;

typedef struct {
	int dim;
	mytype2 *t;
} array_desc;

type1 get_type1(char*);

typedef struct {
	int num_decls;
	mydecl **decls;
} model;
#endif
