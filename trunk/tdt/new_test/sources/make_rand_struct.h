#include "decl.h"
struct OPTS {
	int num_declarations;
	int struct_percentage;
	int max_elem_struct;
	int array_percentage;
	int addr_percentage;
	int addr_array_percentage;
	int max_array_dimensions;
	int max_array_sizes;
	int max_level;
} opt;


model* add_rand_model(char*);

