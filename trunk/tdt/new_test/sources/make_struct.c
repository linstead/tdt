#include "parse.h"
#include "make_struct.h"
#include <stdio.h>
#include <string.h>

#define ALLOC 20
parser p;

struct_desc* add_struct();

int noerror_struct, noerror_model;
mytype1* add_type1()
{
	char *str;
	type1 t1;
	mytype1 *type1;
	
	str=parse_get_next(p);
	if(str==NULL) {
		noerror_model=1;  /*if no new declartion (end model)*/
		return(NULL);
	}
	if(strcmp(str,"}")==0) /*if no new declartion (end stucture)*/
		noerror_struct=1;
	t1=get_type1(str);
	if(t1==notype1)
		return(NULL);

	type1 = (mytype1*) calloc(1,sizeof(mytype1));
	type1->type=t1;

	if(t1!=zstruct)
		type1->type_desc=NULL;
	else {
		type1->type_desc=add_struct();
		if (type1->type_desc == NULL)
			return(NULL);
	}

	return(type1);
}
		
int is_positive_integer(char *s)
{
	int i,n;
	n=strlen(s);
	
	for(i=0;i<n;i++)
		if(s[i]<'0' || s[i]>'9')
			return(-1);
	sscanf(s,"%d",&n);
	return(n);
}
	
int is_good_name(char *s)
{
	int i,n;
	n=strlen(s);
	i=0;
	if(!( (s[i]>='a' &&  s[i]<='z') || (s[i]>='A' &&  s[i]<='Z') || s[i]=='_'))
		return(0);
	for(i=1;i<n;i++) {
		
		if(!( (s[i]>='a' &&  s[i]<='z') || (s[i]>='A' &&  s[i]<='Z') || (s[i]>='0' &&  s[i]<='9') || s[i]=='_' ))
			return(0);
	}
	return(1);
}

	
mytype2* add_type2()
{
	int i;
	char *str;
	mytype2 *t2,*aux,*aux1,*aux2;
	addr_desc *ad_d;
	array_desc *ar_d;
	char *name;

	
	aux=t2=NULL;
	str=parse_get_next(p);
	if(str==NULL)
		return(NULL);
	while ( strcmp(str,"*") == 0) {
		aux = (mytype2*) calloc(1,sizeof(type2));
		aux->type = zaddr;
		ad_d = (addr_desc*) calloc(1,sizeof(addr_desc));
		aux->type_desc = (addr_desc*) ad_d;
		str=parse_get_next(p);
		if(str==NULL) return(NULL);
		if(strcmp(str,"[")==0) {
			str=parse_get_next(p);
			if(str==NULL) return(NULL);
			i=is_positive_integer(str);
			if(i<=0)
				return(NULL);
			else 
				ad_d->dim=i;
			str=parse_get_next(p);
			if(str==NULL) return(NULL);
			if(strcmp(str,"]")!=0) 
				return(NULL);
			 str=parse_get_next(p);
			 if(str==NULL) return(NULL);
		}
	
		else
//			ad_d->dim=0;
			return(NULL);
		
		ad_d->t=t2;
		t2=aux;
	}
	
	if(is_good_name(str)) {
		name=strdup(str);
		str=parse_get_next(p);
		if(str==NULL) return(NULL);
	}
	else
		return(NULL);

	aux1=NULL;
	while ( strcmp(str,"[") == 0) {
		aux = (mytype2*) calloc(1,sizeof(type2));
		aux->type = zarray;
		ar_d = (array_desc*) calloc(1,sizeof(addr_desc));
		aux->type_desc =(array_desc*) ar_d;
		str=parse_get_next(p);
		if(str==NULL) return(NULL);
		i=is_positive_integer(str);
		if(i<=0)
			return(NULL);
		else 
			ar_d->dim=i;
		str=parse_get_next(p);
		if(str==NULL) return(NULL);
		if(strcmp(str,"]")!=0)
			return(NULL);
		if (aux1==NULL)
			aux1=aux;
		else {
			aux2=aux1;
			while(((array_desc*)aux2->type_desc)->t!=NULL)
				aux2=((array_desc*)aux2->type_desc)->t;
			((array_desc*)(aux2->type_desc))->t=aux;
		
		}
		str=parse_get_next(p);
		if(str==NULL)
			return(NULL);
	}	
	if(t2==NULL && aux1==NULL) {
		t2=(mytype2*) calloc(1,sizeof(type2));
		t2->type=simple;
		t2->type_desc=NULL;
	}
	else if(aux1==NULL)
		t2=t2;
	else if(t2==NULL)
		t2=aux1;
	else
	{
		aux2=aux1;
		while(((array_desc*)aux2->type_desc)->t!=NULL)
			 aux2=((array_desc*)aux2->type_desc)->t;
		((array_desc*)aux2->type_desc)->t=t2;
		t2=aux1;
	}
	if(strcmp(str,";")!=0)
		return(NULL);
	t2->name=name;
//	printf("%s\n",name);
	return(t2);
}

mydecl* add_decl()
{
	mydecl *md;
	char *str;
	
	md = (mydecl*) calloc(1,sizeof(mydecl));

	md->t1=add_type1();
	if(md->t1==NULL)
		return(NULL);

	md->t2=add_type2();
	if(md->t2==NULL)
		return(NULL);

	return(md);
}
		


struct_desc* add_struct()
{
	struct_desc *sd;
	int num_decl;
	mydecl *dec;
	char *str;
	noerror_struct=0;
	
	str=parse_get_next(p);
	if(str==NULL) return(NULL);
	if(strcmp(str,"{")!=0)
			return (NULL);
	sd = (struct_desc*) calloc(1,sizeof(struct_desc));
	sd->num_decls=0;
	sd->decls=NULL;

	num_decl=0;
	do {
		dec=add_decl();
		if ( dec == NULL ) {
			if(noerror_struct==0)
				return (NULL);
			else
			{
				noerror_struct=0;
				return(sd);
			}
		
		}
		if( num_decl <= 0 ) {
			sd->decls=(mydecl**) realloc(sd->decls, ((sd->num_decls+1)*ALLOC )*sizeof(mydecl*) );
			num_decl=ALLOC;
		}
		sd->decls[sd->num_decls]=dec;
		sd->num_decls++;
		num_decl--;
	}
	while( dec!=NULL);
}


model* add_model(char* name)
{
	char *str;
	mydecl *dec;
	int num_decl;
	model *mod;
	p=parse_init(name);

	mod=(model*) calloc(1,sizeof(model));
	mod->num_decls=0;
	mod->decls=NULL;

	num_decl=0;
	noerror_model=0;
	
	do {
		dec=add_decl();
		if ( dec == NULL ) {
			parse_stop(p);
			if(noerror_model==0) {
				printf("error in file %s at line %d\n",name,parse_get_line(p));
				return(NULL);
			}
			else {
				noerror_model=0;
				return(mod);
			}
		}
		if( num_decl <= 0 ) {
			mod->decls=(mydecl**) realloc(mod->decls, ((mod->num_decls+1)*ALLOC )*sizeof(mydecl*) );
			num_decl=ALLOC;
		}
		mod->decls[mod->num_decls]=dec;
		mod->num_decls++;
		num_decl--;
	}
	while( dec!=NULL);
}
