#include "decl.h"
#include <stdio.h>
#include <time.h>
FILE *var_h, *td_h, *var_c, *s_c, *c_c,*xml;

tabs(FILE *f,int l) 
{
	int i;
	for(i=0;i<l;i++)
		fprintf(f,"\t");
}

char* tip(type1 t)
{
	char *c[]={"char","int","float","double","struct"};
	return (c[(int)t]);
}

w_var_h(mydecl* myd, int level) 
{
	int i;
	struct_desc *sd;
	addr_desc *add;
	array_desc *ard;
	mytype2 *mt2;
	int num_addr;
	
	tabs(var_h,level);
	fprintf(var_h,"%s ",tip(myd->t1->type));
//	printf("%s ",tip(myd->t1->type));
	if(myd->t1->type==zstruct) {
		sd=(struct_desc*) myd->t1->type_desc;
	fprintf(var_h,"{\n");
		for(i=0;i<sd->num_decls;i++)
			w_var_h(sd->decls[i],level+1);
		tabs(var_h,level);
		fprintf(var_h,"} ");
	}
	
	mt2=myd->t2;
	num_addr=0;
	do
	{
		if(mt2->type==zaddr)
			num_addr++;
		if(mt2->type==zaddr)
			mt2=((addr_desc*)mt2->type_desc)->t;
		else if(mt2->type==zarray)
			mt2=((array_desc*)mt2->type_desc)->t;
		else
			mt2=NULL;
	}
	while (mt2!=NULL);
	for (i=0;i<num_addr;i++)
		fprintf(var_h,"*");
	mt2=myd->t2;
	fprintf(var_h,"%s",mt2->name);
//	printf("%s\n",mt2->name);
	while(mt2->type==zarray) {
		fprintf(var_h,"[%d]",((array_desc*)mt2->type_desc)->dim);
		mt2=((array_desc*)mt2->type_desc)->t;
		if(mt2==NULL)
			break;
	}
	fprintf(var_h,";\n");
}
	
write_var_h(model* md)  /*and typedefs.h*/
{
	int i;
	mydecl *myd;
	var_h=fopen("var.h","w");
	for(i=0;i<md->num_decls;i++)
		w_var_h(md->decls[i],0);
	fclose(var_h);
}
		
write_function(mydecl *myd)
{
	int i,j,k,lev;
	mytype1 *mt1;
	mytype2 *mt2;
	struct_desc *sd;
	int nad,nar;
	int *sizes_addr, *sizes_array;
	
	nad=0;
	nar=0;
	lev=1;
	mt1=myd->t1;
	mt2=myd->t2;
	while (mt2->type==zarray || mt2->type==zaddr) {
		if(mt2->type==zaddr) 
			nad++;
		else
			nar++;
		if (((array_desc*) mt2->type_desc)->t==NULL)
			break;
		else
			mt2=((array_desc*) mt2->type_desc)->t;
	}
	sizes_addr=(int*) calloc(1,nad*sizeof(int));
	sizes_array=(int*) calloc(1,nar*sizeof(int));
	mt2=myd->t2;
	i=0;j=0;
	while (mt2->type==zarray || mt2->type==zaddr) {
		if(mt2->type==zarray) {
			sizes_array[i]=((array_desc*) mt2->type_desc)->dim;
			i++;
		}
		else {
			sizes_addr[j]=((addr_desc*) mt2->type_desc)->dim;
			j++;
		}
		if (((array_desc*) mt2->type_desc)->t==NULL)
			break;
		else
			mt2=((array_desc*) mt2->type_desc)->t;
	}

	mt2=myd->t2;
	fprintf(var_c,"void init_%s(void *x,option opt)\n{\n",mt2->name);
	switch(mt1->type) {
		case zchar:
			fprintf(var_c,"\tchar *y;\n\ty=(char*)x;\n\tif(opt==_alloc)\n\t\t*y=97+myrand(27);\n");
			break;
		case zint:
			fprintf(var_c,"\tint *y;\n\ty=(int*)x;\n\tif(opt==_alloc)\n\t\t*y=myrand(100);\n");
			break;
		case zfloat:
			fprintf(var_c,"\tfloat *y;\n\ty=(float*)x;\n\tif(opt==_alloc)\n\t\t*y=myrand(100);\n");
			break;
		case zdouble:
			fprintf(var_c,"\tdouble *y;\n\ty=(double*)x;\n\tif(opt==_alloc)\n\t\t*y=myrand(100);\n");
			break;
		case zstruct:
			fprintf(var_c,"\tstruct_%s *y;\n\ty=(struct_%s*)x;\n",mt2->name,mt2->name);
			sd=(struct_desc*) mt1->type_desc;
			for(i=0;i<sd->num_decls;i++)
				fprintf(var_c,"\talloc_%s(&y->%s,opt);\n",sd->decls[i]->t2->name,sd->decls[i]->t2->name);
			break;
	}
	if(mt1->type!=zstruct)
		fprintf(var_c,"\tif(opt==_checksum)\n\t\tchecksum=checksum ^ (unsigned int) *y;\n");
	fprintf(var_c,"}\n\n");
/*	for(k=0;k<nad;k++)
		fprintf(var_h,"*");
	mt2=myd->t2;
	fprintf(var_c,"x");
	for(k=0;k<nar;k++){
		if(k==0)
			fprintf(var_h,"[]");
		else
			fprintf(var_h,"[%d]",sizes_array[k]);
	}
*/
	fprintf(var_c,"void alloc_%s(void *x",mt2->name);
	fprintf(var_c,",option opt)\n{\n");
	if(nad+nar>0) {
		if(mt1->type==zstruct)
			fprintf(var_c,"\tstruct_%s ",mt2->name);
		else
			fprintf(var_c,"\t%s ",tip(mt1->type));
		if(nar>0)
			fprintf(var_c,"*");
		for(k=0;k<nad;k++)
			fprintf(var_c,"*");
//		fprintf(var_c,"*");
		fprintf(var_c,"y;\n");
	}
	if(nar==0 && nad!=0){
		if(mt1->type==zstruct)
			fprintf(var_c,"\tstruct_%s ",mt2->name);
		else
			fprintf(var_c,"\t%s ",tip(mt1->type));
		for(i=0;i<nad+1;i++)
			fprintf(var_c,"*");
		fprintf(var_c,"z;\n");
	}
	for(k=0;k<nar;k++)
		fprintf(var_c,"\tint i%d, n%d;\n",k,k);
	for(k=0;k<nad;k++){
		if(sizes_addr[k]==0)
			break;
		fprintf(var_c,"\tint j%d, m%d;\n",k,k);
	}
	if(nar>0) {
		fprintf(var_c,"\tint d=0;\n");
		fprintf(var_c,"\ty=x;\n");
	}
	else if (nad!=0 && nar==0)
		fprintf(var_c,"\tz=x;\n");
	for(k=0;k<nar;k++)
		fprintf(var_c,"\tn%d=%d;\n",k,sizes_array[k]);
	for(k=0;k<nad;k++) {
		if(sizes_addr[k]==0)
			break;
		else
			fprintf(var_c,"\tm%d=%d;\n",k,sizes_addr[k]);
	}

	for(k=0;k<nar;k++) {
		tabs(var_c,lev);
		fprintf(var_c,"for (i%d=0;i%d<n%d;i%d++) {\n",k,k,k,k);
		lev++;
	}
	for(k=0;k<nad;k++) {
		if(sizes_addr[k]==0)
			break;
		tabs(var_c,lev);
		fprintf(var_c,"if(opt==_alloc)\n");
		tabs(var_c,lev+1);
		if(k==0 && nar==0)
			fprintf(var_c,"*z");
		else
			fprintf(var_c,"y");
//		for(i=0;i<nar;i++)
		if(nar>0)
			fprintf(var_c,"[d]",i);
		for(j=0;j<k;j++)
			fprintf(var_c,"[j%d]",j);
		if(k!=nad-1)
			fprintf(var_c,"=calloc(1,m%d*sizeof(void*));\n",k);
		else {
			fprintf(var_c,"=calloc(1,m%d*sizeof(",k);
			if(mt1->type==zstruct)
				fprintf(var_c,"struct_%s));\n",mt2->name);
			else
				fprintf(var_c,"%s));\n",tip(mt1->type));
		}
		if(k==0 && nar==0) {
			tabs(var_c,lev);
			fprintf(var_c,"y=*z;\n");
		}
		tabs(var_c,lev);
		fprintf(var_c,"for (j%d=0;j%d<m%d;j%d++) {\n",k,k,k,k);
			
			
	lev++;
	}
	tabs(var_c,lev);
	if(nad+nar>0) {
		fprintf(var_c,"init_%s(&y",mt2->name);
//		for(k=0;k<nar;k++) 
		if(nar>0)
			fprintf(var_c,"[d]",k);
		for(k=0;k<nad;k++) {
			if(sizes_addr[k]==0)
				break;
			fprintf(var_c,"[j%d]",k);
		}
	}
	else
		fprintf(var_c,"init_%s(x",mt2->name);
	fprintf(var_c,",opt);\n");

	k=nad-1;
	while(lev>0){
		if(lev==nar+1 && nar>0) {
			tabs(var_c,lev);
			fprintf(var_c,"d++;\n");
		}
		lev--;
		tabs(var_c,lev);
		fprintf(var_c,"}\n");
		if(lev>nar) {
			tabs(var_c,lev);
			fprintf(var_c,"if(opt==_free)\n");
			tabs(var_c,lev+1);
			fprintf(var_c,"free(y");
			if(nar>0)
				fprintf(var_c,"[d]");
			for(j=0;j<k;j++)
				fprintf(var_c,"[j%d]",j);
			k--;
			fprintf(var_c,");\n");
				
		}
	}
}		

w_var_c(mydecl *myd)
{
    int i;
    struct_desc *sd;
    addr_desc *add;
    array_desc *ard;
	mytype1 *mt1;
	mytype2 *mt2;

	mt1=myd->t1;
	mt2=myd->t2;
	if(mt1==NULL || mt2==NULL)
		exit(3);
	if(mt1->type==zstruct) {
		sd=(struct_desc*) mt1->type_desc;
		for(i=0;i<sd->num_decls;i++)
			w_var_c(sd->decls[i]);
	}
	write_function(myd);
}

write_var_c(model *mod)
{
	int i;
	var_c=fopen("var.c","w");
	fprintf(var_c,"#include <stdio.h>\n#include <stdlib.h>\n#include \"typedefs.h\"\n#include \"var.h\"\n\nunsigned int checksum;\n\n");
	fprintf(var_c,"int myrand(int r)\n{\n\tint p;\n\tp=rand();\n\treturn(p-r*(p/r));\n}\n");
	for(i=0;i<mod->num_decls;i++)
		w_var_c(mod->decls[i]);
	fclose(var_c);
}


write_server_client_c(model *mod)
{
	int i;

	s_c=fopen("server.c","w");
	c_c=fopen("client.c","w");
#ifndef WIN32

	fprintf(s_c,"#include <unistd.h>\n\
#include <sys/types.h>\n\
#include <sys/socket.h>\n\
#include <netinet/in.h>\n\
#include <netdb.h>\n");
	fprintf(c_c,"#include <unistd.h>\n\
#include <sys/types.h>\n\
#include <sys/socket.h>\n\
#include <netinet/in.h>\n\
#include <netdb.h>\n");

#endif

	fprintf(s_c,"#include <stdio.h>\n\
#include <time.h>\n\
#include <stdlib.h>\n\
#include \"typedefs.h\"\n\
#include \"var.h\"\n\
#include \"../tdt.h\"\n\n\
main()\n{\n");
    fprintf(c_c,"#include <stdio.h>\n\
#include <time.h>\n\
#include <stdlib.h>\n\
#include \"typedefs.h\"\n\
#include \"var.h\"\n\
#include \"../tdt.h\"\n\n\
main()\n{\n");
	fprintf(s_c,"\n\tFILE *ck;\n\tTDTState ts;\n\textern unsigned int checksum;\n\tsrand((int) time(NULL));\n\n");
	fprintf(c_c,"\n\tFILE *ck;\n\tTDTState ts;\n\textern unsigned int checksum;\n\tsrand((int) time(NULL));\n\n");
	for(i=0;i<mod->num_decls;i++) {
		fprintf(s_c,"\talloc_%s(&%s,_alloc);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
		fprintf(c_c,"\talloc_%s(&%s,_alloc);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	}
	fprintf(s_c,"\n");
	fprintf(c_c,"\n");
    fprintf(s_c,"\tchecksum=0;\n");
	for(i=0;i<mod->num_decls;i++)
		fprintf(s_c,"\talloc_%s(&%s,_checksum);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	fprintf(s_c,"\tprintf(\"checksum server before tdt = %%u\\n\",checksum);\n\n");
	
	fprintf(c_c,"\tchecksum=0;\n");
	for(i=0;i<mod->num_decls;i++)
		 fprintf(c_c,"\talloc_%s(&%s,_checksum);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	fprintf(c_c,"\n");
	fprintf(c_c,"\tts = tdt_init (\"example.xml\");\n");
	fprintf(s_c,"\tts = tdt_init (\"example.xml\");\n");
	srand((int)time(NULL));
	i=rand()+1;
	i=2000+i-20000*(i/20000);
	fprintf(c_c,"\tts = tdt_open_socket (ts, \"localhost\", %d, WRITE);\n",i);
	fprintf(s_c,"\tts = tdt_open_socket (ts, \"localhost\", %d, READ);\n",i);
	for(i=0;i<mod->num_decls;i++) {
		fprintf(s_c,"\ttdt_read (ts, &%s, \"%s\");\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
		fprintf(c_c,"\ttdt_write(ts, &%s, \"%s\");\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	}
    
	fprintf(s_c,"\n");
	fprintf(c_c,"\n");
	fprintf(s_c,"\ttdt_end(ts);\n");
	fprintf(c_c,"\ttdt_end(ts);\n");
	
	fprintf(s_c,"\tchecksum=0;\n");
	for(i=0;i<mod->num_decls;i++) 
		fprintf(s_c,"\talloc_%s(&%s,_checksum);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	fprintf(s_c,"\n");
	fprintf(s_c,"\tprintf(\"checksum server after tdt= %%u\\n\",checksum);\n");
	fprintf(s_c,"\tck=fopen(\"server.check\",\"w\");\n\tfprintf(ck,\"%%u\",checksum);\n\tfclose(ck);\n");
	fprintf(c_c,"\tprintf(\"checksum client = %%u\\n\",checksum);\n");
	fprintf(c_c,"\tck=fopen(\"client.check\",\"w\");\n\tfprintf(ck,\"%%u\",checksum);\n\tfclose(ck);\n");
	for(i=0;i<mod->num_decls;i++) {
		fprintf(s_c,"\talloc_%s(&%s,_free);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
		fprintf(c_c,"\talloc_%s(&%s,_free);\n",mod->decls[i]->t2->name,mod->decls[i]->t2->name);
	}

	fprintf(s_c,"}\n");
	fprintf(c_c,"}\n");
}

w_td_h(mydecl *myd)
{
    int i,j,num_addr;
    struct_desc *sd;
    addr_desc *add;
    array_desc *ard;
    mytype2 *mt2;
    mytype1 *mt1;
    if(myd->t1->type==zstruct) {
        sd=(struct_desc*) myd->t1->type_desc;
        for(i=0;i<sd->num_decls;i++)
            w_td_h(sd->decls[i]);

        fprintf(td_h,"typedef struct {\n");
        for(i=0;i<sd->num_decls;i++) {
            mt1=sd->decls[i]->t1;
            mt2=sd->decls[i]->t2;
            if(mt1->type!=zstruct)
                fprintf(td_h,"\t%s ",tip(mt1->type));
			else
				fprintf(td_h,"\tstruct_%s ",mt2->name);
		    num_addr=0;
		    do
	    	{	
	        	if(mt2->type==zaddr)
		            num_addr++;
        		if(mt2->type==zaddr)
	        	    mt2=((addr_desc*)mt2->type_desc)->t;
	        	else if(mt2->type==zarray)
		            mt2=((array_desc*)mt2->type_desc)->t;
        		else
        		    mt2=NULL;
		    }
		    while (mt2!=NULL);
	    	for (j=0;j<num_addr;j++)
	        	fprintf(td_h,"*");
		    mt2=sd->decls[i]->t2;
	    	fprintf(td_h,"%s",mt2->name);
//		    printf("%s\n",mt2->name);
		    while(mt2->type==zarray) {
	    	    fprintf(td_h,"[%d]",((array_desc*)mt2->type_desc)->dim);
	        	mt2=((array_desc*)mt2->type_desc)->t;
	        	if(mt2==NULL)
    	    	    break;
	    	}
			fprintf(td_h,";\n",myd->t2->name);
        }
		fprintf(td_h,"} struct_%s;\n",myd->t2->name);
    }


}


write_td_h(model *mod)
{
    int i;
    mydecl *myd;
    td_h=fopen("typedefs.h","w");
	fprintf(td_h,"typedef enum  {_alloc, _free ,_checksum} option;\n\n");
    for(i=0;i<mod->num_decls;i++)
        w_td_h(mod->decls[i]);
    fclose(td_h);
}



w_xml(mydecl *myd,int level)
{
	int i;
	struct_desc *sd;
	mytype2 *mt2;
	int num_addr,num_array;
	num_addr=0;
	num_array=0;
	tabs(xml,level);
	mt2=myd->t2;
	fprintf(xml,"<decl name=\"%s\">\n",mt2->name);
	level++;
	if(mt2->type!=simple)
    do
	{
		if(mt2->type==zaddr) {
			num_addr++;
			tabs(xml,level);
			fprintf(xml,"<addr>\n");
			level++;
			tabs(xml,level);
			level++;
			fprintf(xml,"<array size=\"%d\">\n",((addr_desc*)mt2->type_desc)->dim);
			mt2=((addr_desc*)mt2->type_desc)->t;
		}
		else if(mt2->type==zarray){
			num_array++;
			tabs(xml,level);
			fprintf(xml,"<array size=\"%d\">\n",((array_desc*)mt2->type_desc)->dim);
			level++;
			mt2=((array_desc*)mt2->type_desc)->t;
		}
		else
			mt2=NULL;
	}
	while (mt2!=NULL);

	
	if(myd->t1->type==zstruct) {
		tabs(xml,level);
		fprintf(xml,"<struct>\n");
		sd=(struct_desc*) myd->t1->type_desc;
		for(i=0;i<sd->num_decls;i++)
			w_xml(sd->decls[i],level+1);
		tabs(xml,level);
		fprintf(xml,"</struct>");
	}
	else {
		tabs(xml,level);
		fprintf(xml,"%s\n",tip(myd->t1->type));
		level++;
		tabs(xml,level);
	}
		
		
	for(i=0;i<num_addr;i++) {
		level=level-2;
		fprintf(xml,"</array></addr>");
	}
	for(i=0;i<num_array;i++) {
		level--;
		fprintf(xml,"</array>");
	}

	fprintf(xml,"</decl>\n");
	level--;
}	
write_xml(model *mod)
{
	int i;
	mydecl *myd;
	xml=fopen("example.xml","w");
	fprintf(xml,"<data_desc>\n");
	for(i=0;i<mod->num_decls;i++)
		w_xml(mod->decls[i],1);
	fprintf(xml,"</data_desc>");
	fclose(xml);
}
