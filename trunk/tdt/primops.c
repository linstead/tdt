/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


/*
 * Implements the default primitive type transfer operations declared in
 * primops.h
 * Originated by Cezar.
 */

/* Windows changes 29-09-2002, Dan
 * Rewritten complete the code for sending and receving with sockets
 */
#include <stdio.h>
#include <stdlib.h>
#include "tdterror.h"

#ifdef WIN32

#include <winsock.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#endif

#include "swapbytes.h"
#include "typeops.h"
#include "addrops.h"
#include "primops.h"


#ifdef  _MPI
#include <mpi.h>
#endif

#define MAX_BUFFER 1000000 
#ifndef WIN32
/* Exits with status 1 on error */
void
read_char (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_char is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_char is NULL");
        exit (1);
    }
    
	if (fread (val, sizeof (char), 1, ad->fp) < 1) {
        tdt_error ("fread in read_char failed");
        exit (1);
    }
}

/* Exits with status 1 on error */
void
read_int (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_int is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_int is NULL");
        exit (1);
    }
    
	if (fread (val, sizeof (int), 1, ad->fp) < 1) {
        tdt_error ("fread in read_int failed");
        exit (1);
    }

    if (ad->requires_swap == 1) {
        swap (val, sizeof(int));
    }
}

/* Exits with status 1 on error */
void
read_float (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_float is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_float is NULL");
        exit (1);
    }
    
	if (fread (val, sizeof (float), 1, ad->fp) < 1) {
        tdt_error ("fread in read_float failed");
        exit (1);
    }

    if (ad->requires_swap == 1) {
        swap (val, sizeof(float));
    }
}

/* Exits with status 1 on error */
void
read_double (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_double is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_double is NULL");
        exit (1);
    }
    
	if (fread (val, sizeof (double), 1, ad->fp) < 1) {
        tdt_error ("fread in read_double failed");
        exit (1);
    }

    if (ad->requires_swap == 1) {
        swap (val, sizeof(double));
    }
}

void
read_block (void *val, int size, void *args) {

    TDTAddr     ad;
    ad = args;
    
    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_block is NULL");
        exit (1);
    }
    
	if (fread (val, size, 1, ad->fp) < 1) {
        tdt_error ("fread in read_block failed");
        exit (1);
    }

}



/* Exits with status 1 on error */
void
write_char (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_char is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in write_char is NULL");
        exit (1);
    }
    
	if (fwrite (val, sizeof (char), 1, ad->fp) < 1) {
        tdt_error ("fwrite in write_char failed");
        exit (1);
    }
}

/* Exits with status 1 on error */
void
write_int (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_int is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in write_int is NULL");
        exit (1);
    }
    
	if (fwrite (val, sizeof (int), 1, ad->fp) < 1) {
        tdt_error ("fwrite in write_int failed");
        exit (1);
    }

    if (fflush (ad->fp) != 0) {
        tdt_error ("fflush failed in write_int");
        exit (1);
    }
}

/* Exits with status 1 on error */
void
write_float (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_float is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in write_float is NULL");
        exit (1);
    }
    
	if (fwrite (val, sizeof (float), 1, ad->fp) < 1) {
        tdt_error ("fwrite in write_float failed");
        exit (1);
    }
}

/* Exits with status 1 on error */
void
write_double (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_double is NULL");
        exit (1);
    }

    ad = args;

    if (ad->fp == NULL) {
        tdt_error ("ad->fp in write_double is NULL");
        exit (1);
    }
    
	if (fwrite (val, sizeof (double), 1, ad->fp) < 1) {
        tdt_error ("fwrite in write_double failed");
        exit (1);
    }
}


void
write_block (void *val, int size, void *args) {
    
    TDTAddr     ad;
    ad = args;
    
    if (ad->fp == NULL) {
        tdt_error ("ad->fp in read_block is NULL");
        exit (1);
    }
	if (fwrite (val, size, 1, ad->fp) < 1) {
        tdt_error ("fread in write_block failed");
        exit (1);
    }

}
#else


int recv_data (SOCKET s, char *buf, int n, int flags)
{ 
	int bcount; /* counts bytes read */
	int br; /* bytes read this pass */
	bcount = 0;
	br = 0;
	while (bcount < n) { /* loop until full buffer */
		if ((br = recv(s, buf, n - bcount,flags)) > 0) {
			bcount += br; /* increment byte counter */
			buf += br; /* move buffer ptr for next read */
		}
		else if (br < 0) /* signal an error to the caller */
			return -1;
	}
	return bcount;
}





/* Exits with status 1 on error */
void
read_char (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_char is NULL");
        exit (1);
    }

    ad = args;
	if (ad->prot == FILEP) {
		if (ad->fp == NULL) {
	        tdt_error ("ad->fp in read_char is NULL");
		    exit (1);
		}
    
		if (fread (val, sizeof (char), 1, ad->fp) < 1) {
			tdt_error ("fread in read_char failed");
			exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if (recv_data (ad->sock, val, sizeof (char),0) < 1) {
			tdt_error ("recv in read_char failed");
			exit (1);
		}
	}
 
}

/* Exits with status 1 on error */
void
read_int (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;
int i,err;
    if (args == NULL) {
        tdt_error ("args parameter passed to read_int is NULL");
        exit (1);
    }

    ad = args;
	
	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in read_int is NULL");
			exit (1);
		}
    
		if (fread (val, sizeof (int), 1, ad->fp) < 1) {
		    tdt_error ("fread in read_int failed");
			exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if ((i=recv_data (ad->sock, val, sizeof (int),0) )< 1) {
			tdt_error ("recv in read_int failed");
			exit (1);
		}
		err=WSAGetLastError();
	}
    
    if (ad->requires_swap == 1) {
        swap (val, sizeof(int));
    }
}

/* Exits with status 1 on error */
void
read_float (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to read_float is NULL");
        exit (1);
    }

    ad = args;

	if (ad->prot == FILEP) {
		if (ad->fp == NULL) {
			tdt_error ("ad->fp in read_float is NULL");
	        exit (1);
		}
    
		if (fread (val, sizeof (float), 1, ad->fp) < 1) {
			tdt_error ("fread in read_float failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if (recv_data (ad->sock, val, sizeof (float),0 ) < 1) {
			tdt_error ("recv in read_float failed");
			exit (1);
		}
	}

    if (ad->requires_swap == 1) {
        swap (val, sizeof(float));
    }
}

/* Exits with status 1 on error */
void
read_double (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;
int i,err;
    if (args == NULL) {
        tdt_error ("args parameter passed to read_double is NULL");
        exit (1);
    }

    ad = args;

	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in read_double is NULL");
	        exit (1);
		}
    
		if (fread (val, sizeof (double), 1, ad->fp) < 1) {
			tdt_error ("fread in read_double failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if ((i=recv_data (ad->sock, val, sizeof (double),0 )) < 1) {
			err=WSAGetLastError();
			tdt_error ("recv in read_double failed");
			exit (1);
		}
		i++;
	}

    if (ad->requires_swap == 1) {
        swap (val, sizeof(double));
    }
}

/* Exits with status 1 on error */
void
read_block (void *val, int size, void *args) {
	
	TDTAddr ad;
	ad = args;
	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in read_double is NULL");
	        exit (1);
		}
    
		if (fread (val, size, 1, ad->fp) < 1) {
			tdt_error ("fread in read_double failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}
		do {
			if (size <= MAX_BUFFER) { 
				if (recv_data (ad->sock, val, size, 0) < 1) {
					tdt_error ("recv in read_block failed");
					exit (1);
				}
				size = size - MAX_BUFFER; 
			}
			else {
					if (recv_data (ad->sock, val, MAX_BUFFER, 0 ) < 1) { 
						tdt_error ("recv in read_block failed2");
						exit (1);
					} 
					size = size - MAX_BUFFER; 
					val = (char*) val + MAX_BUFFER;
			}
		}
		while ( size > 0); 
	}

}
 

/* Exits with status 1 on error */
void
write_char (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_char is NULL");
        exit (1);
    }

    ad = args;

	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in write_char is NULL");
	        exit (1);
		}
    
		if (fwrite (val, sizeof (char), 1, ad->fp) < 1) {
	        tdt_error ("fwrite in write_char failed");
		    exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if (send (ad->sock, val, sizeof (char),0) < 1) {
			tdt_error ("send in write_char failed");
			exit (1);
		}
	}
}

/* Exits with status 1 on error */
void
write_int (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;
int i,err;
    if (args == NULL) {
        tdt_error ("args parameter passed to write_int is NULL");
        exit (1);
    }

    ad = args;

	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in write_int is NULL");
			exit (1);
		}
    
		if (fwrite (val, sizeof (int), 1, ad->fp) < 1) {
			tdt_error ("fwrite in write_int failed");
			exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}

		if ((i=send (ad->sock, val, sizeof (int),0)) < 1) {
			err = WSAGetLastError();
			tdt_error ("send in write_int failed");
			exit (1);
		}
	}
}

/* Exits with status 1 on error */
int cont=0;
void
write_float (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr     ad;
int i,err;

    if (args == NULL) {
        tdt_error ("args parameter passed to write_float is NULL");
        exit (1);
    }

    ad = args;
	cont++;
	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in write_float is NULL");
			exit (1);
		}
    
		if (fwrite (val, sizeof (float), 1, ad->fp) < 1) {
			tdt_error ("fwrite in write_float failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}
		if ((i=send (ad->sock, val, sizeof (float),0)) < 1) {
			err=WSAGetLastError();
			tdt_error ("send in write_float failed");
			exit (1);
		}
	}
}

/* Exits with status 1 on error */
void
write_double (void *val, void *type_val, void *args, void *fctns) {
int i,err;
	TDTAddr     ad;
	if (args == NULL) {
        tdt_error ("args parameter passed to write_double is NULL");
        exit (1);
    }

    ad = args;


	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in write_double is NULL");
			exit (1);
		}
    
		if (fwrite (val, sizeof (double), 1, ad->fp) < 1) {
			tdt_error ("fwrite in write_double failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}
		
		if ((i=send (ad->sock, val, sizeof (double),0)) < 1) {
			err=WSAGetLastError();
			tdt_error ("send in write_double failed");
			exit (1);
		}
	}
}

/* Exits with status 1 on error */
void
write_block (void *val, int size, void *args) {

	TDTAddr ad;
	ad = args;

	if (ad->prot == FILEP) {
	    if (ad->fp == NULL) {
		    tdt_error ("ad->fp in read_double is NULL");
	        exit (1);
		}
    
		if (fwrite (val, size, 1, ad->fp) < 1) {
			tdt_error ("fread in read_double failed");
	        exit (1);
		}
	}
	else if (ad->prot == SOCKETP) {
		if (ad->sock == 0) {
	        tdt_error ("ad->sock in read_char is NULL");
		    exit (1);
		}
		do {
			if (size <= MAX_BUFFER) {
				if (send (ad->sock, val, size, 0 ) < 1) {
					tdt_error ("send in write_double failed1");
					exit (1);
				}
				size = size - MAX_BUFFER; 
			}
			else {
				if (send (ad->sock, val, MAX_BUFFER, 0 ) < 1) {
					tdt_error ("send in write_double failed2");
					exit (1);
				}
				size = size - MAX_BUFFER;
				val = (char*) val + MAX_BUFFER;
			}
		}
		while ( size > 0);


	}

} 
#endif


#ifdef _MPI


void
read_char_mpi (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr ad;
	MPIAddr ma;
	MPI_Status status;
	
	ad = args;
	ma = (MPIAddr) ad->addr;
	MPI_Recv (val, 1, MPI_CHAR, ma->rank, 42, MPI_COMM_WORLD, &status);
}

void
read_int_mpi (void *val, void *type_val, void *args, void *fctns) {
	
	TDTAddr ad;
	MPIAddr ma;
	MPI_Status status;
	
	ad = args;
	ma = (MPIAddr) ad->addr;
	MPI_Recv (val, 1, MPI_INT, ma->rank, 42, MPI_COMM_WORLD, &status);

}

void
read_float_mpi (void *val, void *type_val, void *args, void *fctns) {
	
	TDTAddr ad;
	MPIAddr ma;
	MPI_Status status;
	
	ad = args;
	ma = (MPIAddr) ad->addr;
	
	MPI_Recv (val, 1, MPI_FLOAT, ma->rank, 42, MPI_COMM_WORLD, &status);
}

void
read_double_mpi (void *val, void *type_val, void *args, void *fctns) {
	
	TDTAddr ad;
	MPIAddr ma;
	MPI_Status status;
	
	ad = args;
	ma = (MPIAddr) ad->addr;

	MPI_Recv (val, 1, MPI_DOUBLE, ma->rank, 42, MPI_COMM_WORLD, &status);
}

void
read_block_mpi (void *val, int size, void *args) {
    
    TDTAddr ad;
    MPIAddr ma;
    MPI_Status status;

    ad = args;
    ma = (MPIAddr) ad->addr;
    MPI_Recv (val, size, MPI_BYTE, ma->rank, 42, MPI_COMM_WORLD, &status);
}


void
write_char_mpi (void *val, void *type_val, void *args, void *fctns) {
	
	TDTAddr ad;
	MPIAddr ma;
	
	ad = args;
	ma = (MPIAddr) ad->addr;
	
	MPI_Send (val, 1, MPI_CHAR, ma->rank, 42, MPI_COMM_WORLD);
}

void
write_int_mpi (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr ad;
	MPIAddr ma;
	
	ad = args;
	ma = (MPIAddr) ad->addr;

	MPI_Send (val, 1, MPI_INT, ma->rank, 42, MPI_COMM_WORLD);

}

void
write_float_mpi (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr ad;
	MPIAddr ma;
	
	ad = args;
	ma = (MPIAddr) ad->addr;
	
	MPI_Send (val, 1, MPI_FLOAT, ma->rank, 42, MPI_COMM_WORLD);
}

void
write_double_mpi (void *val, void *type_val, void *args, void *fctns) {

	TDTAddr ad;
	MPIAddr ma;
	
	ad = args;
	ma = (MPIAddr) ad->addr;

	MPI_Send (val, 1, MPI_DOUBLE, ma->rank, 42, MPI_COMM_WORLD);
}

void
write_block_mpi (void *val, int size, void *args) {
    
    TDTAddr ad;
    MPIAddr ma;

    ad = args;
    ma = (MPIAddr) ad->addr;

    MPI_Send (val, size, MPI_BYTE, ma->rank, 42, MPI_COMM_WORLD);
}

#endif
