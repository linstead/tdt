/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tdterror.h"
#include "typeops.h"
#include "typeops_cons.h"

/* Constructor functions */

/* Constructor for TypeDesc */
/* Returns NULL on error */
TypeDesc
make_typedesc (TypeName tn, void *val) {

	TypeDesc    td;
	td = (TypeDesc) malloc (sizeof (struct typedesc));

	if (td == NULL) {
		tdt_error ("Memory allocation of td in make_typedesc.");
		return NULL;
	}

	td->name = tn;
	td->value = val;

	return td;
}

/* Constructor for Decl */
/* Returns NULL on error */
Decl
make_decl (String name, TypeDesc td) {

	Decl        d;
/*	size_t		namesz;
	char 		*newname;*/

	d = (Decl) malloc (sizeof (struct decl_desc));

	if (d == NULL) {
		tdt_error ("Memory allocation of d in make_decl.");
		return NULL;
	}

    d->name = strdup (name);
    
	if (d->name == NULL) {
		tdt_error ("Memory allocation of d->name in make_decl");
		return NULL;
	}

	d->type = td;
	return d;
}

/* Constructor for Struct */
/* Return NULL on error */
StructDesc
make_structdesc (int num_decls, Decl *decls) {

	StructDesc  sd;
	sd = (StructDesc) malloc (sizeof (struct struct_desc));

	if (sd == NULL) {
		tdt_error ("Memory allocation for sd in make_structdesc.");
		return NULL;
	}
	sd->num_decls = num_decls;
	sd->_current = -1;
	sd->decls = decls;

	return sd;
}

/* Returns NULL on error */ 
StructDesc
make_empty_structdesc () {

	StructDesc  sd;

	sd = make_structdesc (0, NULL);

	if (sd == NULL) {
		tdt_error ("make_structdesc error in make_empty_structdesc.");
		return NULL;
	}

	return sd;
}

/* Constructor for array */
/* Returns NULL on error */
ArrayDesc
make_arraydesc (int size, TypeDesc td) {

	ArrayDesc   a;
	a = (ArrayDesc) malloc (sizeof (struct array_desc));

	if (a == NULL) {
		tdt_error ("Memory allocation for a in make_arraydesc.");
		return NULL;
	}
	a->size = size;
	a->type = td;

	return a;
}

/* Constructor for addr */
/* Returns NULL on error */
AddrDesc
make_addrdesc (TypeDesc td) {

	AddrDesc    ad;
	ad = (AddrDesc) malloc (sizeof (struct addr_desc));

	if (ad == NULL) {
		tdt_error ("Memory allocation for ad in make_addrdesc.");
		return NULL;
	}

	ad->type = td;

	return ad;
}

/* Constructor for typedef */
/* Returns NULL on error */
TypedefDesc
make_typedefdesc (String name, TypeDesc td) {

	TypedefDesc tdd;
	tdd = (TypedefDesc) malloc (sizeof (struct typedef_desc));

	if (tdd == NULL) {
		tdt_error ("Could not allocate memory for TypedefDesc struct");
		return NULL;
	}
	tdd->typedefname = name;
	tdd->basetype = td;

	return tdd;
}

/* Destructor functions */

/* Primitive types have nothing to destroy */
void
destroy_char (void *v) {

	return;
}

void
destroy_int (void *v) {

	return;
}

void
destroy_float (void *v) {

	return;
}

void
destroy_double (void *v) {

	return;
}

/* addr and array have to destroy the descriptors */
void
destroy_addrdesc (AddrDesc ad) {

	TypeDesc    td;

	if (ad == NULL) {
		tdt_error ("Null argument to destroy_addrdesc.");
		exit (1); /* no return value can be interpreted as error */
	}

	td = ad->type;

	destroy_typedesc (td); 
	free (ad);
}

void
destroy_addr (void *v) {

	AddrDesc    ad = v;
	if (ad == NULL) {
		tdt_error ("Null argument to destroy_addr.");
		exit (1); /* no return value can be interpreted as error */
	}

	destroy_addrdesc (ad);
}

void
destroy_arraydesc (ArrayDesc ar) {

	TypeDesc    td;

	if (ar == NULL) {
		tdt_error ("Null argument to destroy_arraydesc.");
		exit (1); /* no return value can be interpreted as error */
	}

	td = ar->type;

	destroy_typedesc (td);
	free (ar);
}

void
destroy_array (void *v) {

	ArrayDesc   ar = v;
	if (ar == NULL) {
		tdt_error ("Null argument to destroy_array.");
		exit (1); /* no return value can be interpreted as error */
	}

	destroy_arraydesc (ar);
}

/* struct has to destroy the decls */
void
destroy_structdesc (StructDesc sd) {

	Decl        d;

	if (sd == NULL) {
		tdt_error ("NULL argument to destroy_structdesc.");
		exit (1); /* no return value can be interpreted as error */
	}

	reset_decls (sd);
	while (has_more_decls (sd) == TRUE) {
		d = next_decl (sd);
		if (d == NULL) {
			tdt_error ("sd contains NULL decl in destroy_structdesc.");
			exit (1); /* no return value can be interpreted as error */
		}
		destroy_decl (d);
	}
	free (sd->decls);
	free (sd);
}

void
destroy_struct (void *v) {

	StructDesc  sd = v;

	if (sd == NULL) {
		tdt_error ("NULL argument to destroy_struct.");
		exit (1); /* no return value can be interpreted as error */
	}
	destroy_structdesc (sd);
}

void
    (*destroy_type[]) (void *v) = {
	destroy_char,
	destroy_int,
	destroy_float,
	destroy_double, 
	destroy_addr, 
	destroy_array, 
	destroy_struct};

void
destroy_typedesc (TypeDesc td) {

	if (td == NULL) {
		tdt_error ("NULL argument to destroy_typedesc.");
		exit (1); /* no return value can be interpreted as error */
	}
	destroy_type[td->name] (td->value);
	free (td);
}

void
destroy_decl (Decl d) {

	String dname;
	TypeDesc td;
	if (d == NULL) {
		tdt_error ("NULL argument to destroy_decl.");
		exit (1); /* no return value can be interpreted as error */
	}
	dname = decl_name (d);
	if (dname == NULL) {
		tdt_error ("NULL-named decl in destroy_decl.");
		exit (1); /* no return value can be interpreted as error */
	}
	free (dname);
	td = decl_type (d);
	if (td == NULL) {
		tdt_error ("NULL-decl type in destroy_decl.");
		exit (1); /* no return value can be interpreted as error */
	}
	destroy_typedesc (td);
	free (d);
}

void
destroy_typedefdesc (TypedefDesc tdd) {

	if (tdd == NULL) {
		tdt_error ("NULL argument to destroy_typedefdesc.");
		exit (1); /* no return value can be interpreted as error */
	}
	free (tdd->typedefname);
	destroy_typedesc (tdd->basetype);
	free (tdd);
}
