/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut für Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __TDT_H__
#define __TDT_H__

typedef enum mode {READ, WRITE} Mode; 
typedef enum array_order {ON_COLUMNS, ON_ROWS} Array_Order;

typedef char* String;

typedef struct channel {
	String name;
	String mode;
	String host;
	int    port;
	String type;
	String filename;
	String datadesc;
}* Channel;

typedef struct tdt_config {
	String progname;
	Channel *channels;
	int num_channels;
	Array_Order array_ord;
} *TDTConfig;

typedef struct  tdt_state * TDTState;

TDTState
tdt_parse_datadesc (String xmlname);

TDTState
tdt_init(TDTConfig tc, String cname);

TDTState
tdt_open_file(TDTState ts, String fname, Mode mode);

TDTState
tdt_open_socket(TDTState ts, String hostname, int port, Mode mode);

TDTState
tdt_open (TDTConfig tc, String cname);

void
tdt_write(TDTState ts, void *value, String name);

void
tdt_read(TDTState ts, void *value, String name);

void
tdt_close(TDTState ts);

void
tdt_size_array (TDTState ts, String name, int size);

void
tdt_size_multiarray (TDTState ts, String name, int rank, int size);

void
tdt_set_array_order_oncolumns (TDTConfig tc);
void
tdt_set_array_order_onrows  (TDTConfig tc);

/* from tdt_config.h*/

void
print_config (TDTConfig tc);

TDTConfig
tdt_configure (String config_filename);

void
tdt_end (TDTConfig tc);

#endif
