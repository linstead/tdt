/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* xmlhandlers.h */
#ifndef __XMLHANDLERS_H__
#define __XMLHANDLERS_H__

#ifdef _MPI
#include "mtdt.h"
#else
#include "tdt.h"
#endif

#define NUM_TO_ALLOC 10

/* XML data parsed into this structure */
typedef struct data_desc {
	Decl *ad;
	int nd;
	int cd;
	TypeDesc td;
	StructDesc *as;
	int ns;
	int cs;
}* DataDesc;

TDTConfig
init_config ();

DataDesc 
init_datadesc();

void
free_datadesc(DataDesc md);

TypeDesc 
find_type_in_datadesc(DataDesc md, String name);

DataDesc
read_datadesc (char *fn);

TDTConfig
read_config (char *fn);

/*void
start(void *data, const char *el, const char **attr);

void
end1(void *data, const char *el);

void
chardata(void *data, const char *el, int len);
*/
#endif
