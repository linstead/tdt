/*
 * Typed Data Transfer (TDT) Library 
 *
 * Copyright (C) 2001-2002, Cezar Ionescu (ionescu@pik-potsdam.de)
 *                          Ciaron Linstead (linstead@pik-potsdam.de)
 *
 *                          Potsdam Institut f�r Klimafolgenforschung
 *                          (PIK)
 *                          Telegrafenberg A31
 *                          14473 Potsdam
 *                          Germany
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This is progo, and will be reading from progc on port 2226 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include "tdt.h"

int
main (int argc, char **argv) { 


    /* progo reads the following from progc */
    double sbcocn_co[72][38][30];
    
    /* progo writes the following to progc */
    double dtts_oc;
    int init_oc;
    double runlen_oc;
    int eqyear_oc;
    int eqmon_oc;
    int year0_oc;
    int month0_oc;
    int day0_oc;
    double dttsit_oc;
    double dttsid_oc;
    int imt_oc;
    int jmt_oc;
    double dzt_oc[13];
    double dxtdeg_oc[72];
    double dytdeg_oc[38];
    double xt_oc[72];
    double yt_oc[38];
    double dxt_oc[72];
    double dyt_oc[38];
    double cst_oc[38];
    double map_oc[72][38];
    double sbcocn_oc[72][38][10];
    /* double sbcocn_co[72][38][20]; */
            
    TDTState ts_co;
    TDTState ts_oc;

    /* some loop counters */
    int i,j,k,l;

    ts_co = tdt_init ("exdat_co.xml");
    ts_oc = tdt_init ("exdat_oc.xml");

    ts_co = tdt_open_socket (ts_co, "localhost", 2226, READ);
    ts_oc = tdt_open_socket (ts_oc, "localhost", 2228, WRITE);

    /* read from progc */
    tdt_read (&sbcocn_co, "sbcocn_co", ts_co);
    
    /* initialise vars being written to progc*/
    dtts_oc = 1.1;
    init_oc = 2;
    runlen_oc = 3.3;
    eqyear_oc = 4;
    eqmon_oc = 5 ;
    year0_oc = 6;
    month0_oc = 7;
    day0_oc = 8;
    dttsit_oc = 9.9;
    dttsid_oc = 10.1;
    imt_oc = 11;
    jmt_oc = 12;

    for (i=0; i<13; i++) {
       dzt_oc[i] = i*13.0;
    }
    
    for (i=0; i<72; i++) {
        dxtdeg_oc[i] = i*14.0;
    }
    for (i=0; i<38; i++) {
        dytdeg_oc[i] = i*15.0;
    }
    for (i=0; i<72; i++) {
        xt_oc[i] = i*16.0;
    }
    for (i=0; i<38; i++) {
        yt_oc[i] = i*17.0;
    }
    for (i=0; i<72; i++) {
        dxt_oc[i] = i*18.0;
    }
    for (i=0; i<38; i++) {
        dyt_oc[i]= i*19.0;
    }
    for (i=0; i<38; i++) {
        cst_oc[i] = i*20.0;
    }
    for (i=0; i<72; i++) {
        for (j=0; j<38; j++) {
            map_oc[i][j] = i*j*21.0;
        }
    }
    for (i=0; i<72; i++) {
        for (j=0; j<38; j++) {
            for (k=0; k<10; k++) {
                sbcocn_oc[i][j][k] = i*j*k*22.0;
            }
        }
    }

    /* Writes to progc */
    tdt_write (&dtts_oc, "dtts_oc", ts_oc);
    tdt_write (&init_oc, "init_oc", ts_oc);
    tdt_write (&runlen_oc, "runlen_oc", ts_oc);
    tdt_write (&eqyear_oc, "eqyear_oc", ts_oc);
    tdt_write (&eqmon_oc, "eqmon_oc", ts_oc);
    tdt_write (&year0_oc, "year0_oc", ts_oc);
    tdt_write (&month0_oc, "month0_oc", ts_oc);
    tdt_write (&day0_oc, "day0_oc", ts_oc);
    tdt_write (&dttsit_oc, "dttsit_oc", ts_oc);
    tdt_write (&dttsid_oc, "dttsid_oc", ts_oc);
    tdt_write (&imt_oc, "imt_oc", ts_oc);
    tdt_write (&jmt_oc, "jmt_oc", ts_oc);
    tdt_write (&dzt_oc, "dzt_oc", ts_oc);
    tdt_write (&dxtdeg_oc, "dxtdeg_oc", ts_oc);
    tdt_write (&dytdeg_oc, "dytdeg_oc", ts_oc);
    tdt_write (&xt_oc, "xt_oc", ts_oc);
    tdt_write (&yt_oc, "yt_oc", ts_oc);
    tdt_write (&dxt_oc, "dxt_oc", ts_oc);
    tdt_write (&dyt_oc, "dyt_oc", ts_oc);
    tdt_write (&cst_oc, "cst_oc", ts_oc);
    tdt_write (&map_oc, "map_oc", ts_oc);
    tdt_write (&sbcocn_oc, "sbcocn_oc", ts_oc);
    //tdt_write (&sbcocn_co, "sbcocn_co", ts_co); // !!!!!!!!!!!!!
    
    /* check what's been read */
    /*for (i=0; i<72; i++) {
        for (j=0; j<38; j++) {
            for (k=0; k<30; k++) {
                printf ("%f ",sbcocn_co[i][j][k]);
            }
            printf ("\n");
        }
    }
      */  
    tdt_end (ts_co);
    tdt_end (ts_oc);

    return 0;

}
